﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LixilRebate.Constants;
using LixilRebate.Models;
using LixilRebate.ViewModels;

namespace LixilRebate.Controllers
{
    public class AdminController : Controller
    {
        // GET: Admin
        private DBContext _db = new DBContext();
        public ActionResult Index()
        {
            var rebateRewards = new List<RebateReward>();
            var rebateSelects = new List<RebateSelect>();

            if (_db.RebateRewards.Any(x => x.IsActive == true))
            {
                rebateRewards = _db.RebateRewards.Where(x => x.IsActive == true).ToList();
            }

            if (_db.RebateSelects.Any(x => x.IsActive == true))
            {
                rebateSelects = _db.RebateSelects.Where(x => x.IsActive == true).ToList();
            }

            var rewardsViewModels = new List<RebateRewardsViewModel>();
            var selectViewModels = new List<RebateSelectViewModel>();

            RebateInformationViewModel rebateInformation = new RebateInformationViewModel();

            if (rebateRewards.Count > 0)
            {
                foreach (var reward in rebateRewards)
                {
                    RebateRewardsViewModel rebate = new RebateRewardsViewModel()
                    {
                        OrderNumber = reward.RecordNumber,
                        DistributorInvoiceNumber = reward.DistributorInvoiceNumber,
                        DistributorInvoiceDate = reward.DistributorInvoiceDate,
                        CreatedDate = reward.CreatedDate,
                        DollarEstimated = reward.DollarEstimated
                    };

                    var rebateDetails = _db.RebateDetails.Where(x => x.Id == reward.RebateId).FirstOrDefault();
                    if (rebateDetails.status == 3 && rebateDetails.ModifiedBy == 1)
                    {
                        rebate.Status = "Approved";
                    }
                    else if (rebateDetails.status == 4 && rebateDetails.ModifiedBy == 1)
                    {
                        rebate.Status = "Denied";
                    }
                    else
                    {
                        rebate.Status = "Pending";
                    }
                    rebate.ApprovedOrDeniedDate = rebateDetails.ApprovedDeniedDate;
                    rebate.DollarApproved = (rebateDetails.ApprovedAmount != null) ? rebateDetails.ApprovedAmount : "$0.00";
                    rebate.ReasonIfDenied = (rebateDetails.ReasonIfDenied != null) ? rebateDetails.ReasonIfDenied : "-";

                    var builder = _db.Users.Where(x => x.Id == reward.BuilderId).FirstOrDefault();
                    rebate.BuilderName = builder.FirstName + " " + builder.LastName;
                    rebate.BuilderEmail = builder.Email;
                    rebate.BuilderPhNo = builder.PhoneNumber;
                    rebate.Type = "Rewards";
                    rewardsViewModels.Add(rebate);

                }
            }
            
            if(rebateSelects.Count > 0)
            {
                foreach (var select in rebateSelects)
                {
                    RebateSelectViewModel selectViewModel = new RebateSelectViewModel()
                    {
                        OrderNumber = select.RecordNumber,
                        SubDivisionName = select.SubDivisionName,
                        Lot_ = select.Lot_,
                        PerHomeRebate = select.PerHomeRebate,
                        ClosingStatementDate = select.ClosingStatementDate,
                        CreatedDate = select.CreatedDate
                    };
                    var rebateDetails = _db.RebateDetails.Where(x => x.Id == select.RebateId).FirstOrDefault();
                    if (rebateDetails.status == 3 && rebateDetails.ModifiedBy == 1)
                    {
                        selectViewModel.Status = "Approved";
                    }
                    else if (rebateDetails.status == 4 && rebateDetails.ModifiedBy == 1)
                    {
                        selectViewModel.Status = "Denied";
                    }
                    else
                    {
                        selectViewModel.Status = "Pending";
                    }
                    selectViewModel.ApprovedorDeniedDate = rebateDetails.ApprovedDeniedDate;
                    selectViewModel.ApprovedAmount = (rebateDetails.ApprovedAmount != null) ? rebateDetails.ApprovedAmount : "$0.00";
                    selectViewModel.ReasonIfDenied = (rebateDetails.ReasonIfDenied != null) ? rebateDetails.ReasonIfDenied : "-";

                    var builder = _db.Users.Where(x => x.Id == select.BuilderId).FirstOrDefault();
                    selectViewModel.BuilderName = builder.FirstName + " " + builder.LastName;
                    selectViewModel.BuilderEmail = builder.Email;
                    selectViewModel.BuilderPhNo = builder.PhoneNumber;
                    selectViewModel.Type = "Select";
                    selectViewModels.Add(selectViewModel);
                }
            }
           
            rebateInformation.RebateRewards = rewardsViewModels;
            rebateInformation.RebateSelects = selectViewModels;
            //return Json(new { data = rebateInformation });
            return View(rebateInformation);
        }

        //public ActionResult Search(string search)
        //{
        //    return null;
        //}

        [HttpGet]
        public ActionResult InvoiceDetailsRewards(string orderNumber)
        {
            var rebateInvoice = _db.RebateRewards.Where(x => x.RecordNumber == orderNumber).FirstOrDefault();

            RebateRewardsViewModel rewardsViewModel = new RebateRewardsViewModel()
            {
                Id = rebateInvoice.Id,
                OrderNumber = rebateInvoice.RecordNumber,
                DistributorInvoiceNumber = rebateInvoice.DistributorInvoiceNumber,
                CreatedDate = rebateInvoice.CreatedDate,
                DollarEstimated = rebateInvoice.DollarEstimated,
                //Comments = rebateInvoice.Comments
            };

            var rebateDetails = _db.RebateDetails.Where(x => x.Id == rebateInvoice.RebateId).FirstOrDefault();
            rewardsViewModel.StatusDate = (rebateDetails.ApprovedDeniedDate != null) ? rebateDetails.ApprovedDeniedDate.Value.ToString("MM/dd/yyyy") : "-";
            rewardsViewModel.StatusId = rebateDetails.status;

            if (rebateDetails.status == 3 && rebateDetails.ModifiedBy == 1)
            {
                rewardsViewModel.Status = "Approved";
            }
            else if (rebateDetails.status == 4 && rebateDetails.ModifiedBy == 1)
            {
                rewardsViewModel.Status = "Denied";
            }
            if (rebateDetails.ApprovedAmount != null)
            {
                rewardsViewModel.DollarApproved = rebateDetails.ApprovedAmount.Substring(rebateDetails.ApprovedAmount.LastIndexOf('$') + 1);
            }
            else
            {
                rewardsViewModel.DollarApproved = rebateDetails.ApprovedAmount;
            }
            rewardsViewModel.ReasonIfDenied = rebateDetails.ReasonIfDenied;

            //if(rebateInvoice.ModifiedBy == 1 && rebateInvoice.Status == 2)
            //{
            //    rewardsViewModel.Status = "Approved";
            //}           

            var builder = _db.Users.Where(x => x.Id == rebateInvoice.BuilderId).First();

            UserViewModel userViewModel = new UserViewModel()
            {
                FirstName = builder.FirstName,
                LastName = builder.LastName,
                PhoneNumber = builder.PhoneNumber,
                Email = builder.Email
            };


            var rebateRedemption = _db.RebateRedemptions.Where(x => x.RebateId == rebateInvoice.RebateId).FirstOrDefault();

            RebateRedemptionViewModel redemptionViewModel = new RebateRedemptionViewModel()
            {
                Name = rebateRedemption.Name,
                Address1 = rebateRedemption.Address1,
                Address2 = rebateRedemption.Address2,
                City = rebateRedemption.City,
                Zip = rebateRedemption.Zip
            };

            redemptionViewModel.StateName = _db.States.Where(x => x.Id == rebateRedemption.State).Select(a => a.Name).FirstOrDefault();

            var invoiceProducts = _db.InvoiceProducts.Where(x => x.InvoiceNumber == rebateInvoice.DistributorInvoiceNumber && x.BuilderId == builder.Id).ToList();

            var products = new List<InvoiceProductViewModel>();
            foreach (var product in invoiceProducts)
            {
                InvoiceProductViewModel invoiceProduct = new InvoiceProductViewModel()
                {
                    ProductNumber = product.ProductNumber,
                    Quantity = product.Quantity
                };
                int manufacturer = (int)product.Manufacturer;
                invoiceProduct.Manufacturer = _db.Manufacturers.Where(x => x.id == manufacturer).Select(x => x.mfg_name).FirstOrDefault();
                products.Add(invoiceProduct);
            }


            var invoicePaths = _db.RebateInvoices.Where(x => x.RebateId == rebateInvoice.RebateId).ToList();

            var pathViewModels = new List<InvoicePathViewModel>();
            foreach (var path in invoicePaths)
            {
                InvoicePathViewModel invoicePath = new InvoicePathViewModel();
                int type = path.InvoiceType;
                invoicePath.InvoicePath = path.InvoicePath;
                invoicePath.InvoiceType = "Rewards";

                pathViewModels.Add(invoicePath);

            }

            InvoiceDetailsViewModel detailsViewModel = new InvoiceDetailsViewModel()
            {
                RebateRewardsViewModel = rewardsViewModel,
                RebateRedemptionViewModel = redemptionViewModel,
                InvoiceProductViewModels = products,
                InvoicePathViewModels = pathViewModels,
                UserViewModel = userViewModel
            };
            return View(detailsViewModel);
        }

        [HttpPost]
        public ActionResult InvoiceDetailsRewards(InvoiceDetailsViewModel detailsViewModel)
        {
            var rewardViewModel = detailsViewModel.RebateRewardsViewModel;
            var rebateReward = _db.RebateRewards.Where(x => x.RecordNumber == rewardViewModel.OrderNumber).FirstOrDefault();
            var rebateDetails = _db.RebateDetails.Where(x => x.Id == rebateReward.RebateId).FirstOrDefault();
            _db.Entry(rebateDetails).State = System.Data.Entity.EntityState.Detached;

            RebateDetail rebateDetail = new RebateDetail();

            if (rewardViewModel.Status == "3")
            {
                rebateDetail.Id = (int)rebateReward.RebateId;
                rebateDetail.status = 3;
                rebateDetail.ModifiedBy = 1;
                rebateDetail.ApprovedDeniedDate = DateTime.Now;
                rebateDetail.ApprovedAmount = "$" + rewardViewModel.DollarApproved;
                rebateDetail.Comments = rewardViewModel.Comments;

                _db.RebateDetails.Attach(rebateDetail);
                _db.Entry(rebateDetail).Property(x => x.status).IsModified = true;
                _db.Entry(rebateDetail).Property(x => x.ModifiedBy).IsModified = true;
                _db.Entry(rebateDetail).Property(x => x.ApprovedDeniedDate).IsModified = true;
                _db.Entry(rebateDetail).Property(x => x.ApprovedAmount).IsModified = true;
                _db.Entry(rebateDetail).Property(x => x.Comments).IsModified = true;
            }
            else if (rewardViewModel.Status == "4")
            {
                rebateDetail.Id = (int)rebateReward.RebateId;
                rebateDetail.ApprovedDeniedDate = DateTime.Now;
                rebateDetail.status = 4;
                rebateDetail.ModifiedBy = 1;
                rebateDetail.ReasonIfDenied = rewardViewModel.ReasonIfDenied;
                rebateDetail.Comments = rewardViewModel.Comments;

                _db.RebateDetails.Attach(rebateDetail);
                _db.Entry(rebateDetail).Property(x => x.status).IsModified = true;
                _db.Entry(rebateDetail).Property(x => x.ModifiedBy).IsModified = true;
                _db.Entry(rebateDetail).Property(x => x.ApprovedDeniedDate).IsModified = true;
                _db.Entry(rebateDetail).Property(x => x.ReasonIfDenied).IsModified = true;
                _db.Entry(rebateDetail).Property(x => x.Comments).IsModified = true;
            }
            _db.Configuration.ValidateOnSaveEnabled = false;
            _db.SaveChanges();

            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult InvoiceDetailsSelect(string orderNumber)
        {
            var rebateSelect = _db.RebateSelects.Where(x => x.RecordNumber == orderNumber).FirstOrDefault();
            var rebateDetails = _db.RebateDetails.Where(x => x.Id == rebateSelect.RebateId).FirstOrDefault();

            RebateSelectViewModel selectViewModel = new RebateSelectViewModel()
            {
                OrderNumber = rebateSelect.RecordNumber,
                SubDivisionName = rebateSelect.SubDivisionName,
                Lot_ = rebateSelect.Lot_,
                PerHomeRebate = rebateSelect.PerHomeRebate,
                ClosingStatementDate = rebateSelect.ClosingStatementDate,
                CreatedDate = rebateSelect.CreatedDate
            };
            selectViewModel.StatusDate = (rebateDetails.ApprovedDeniedDate != null) ? rebateDetails.ApprovedDeniedDate.Value.ToString("MM/dd/yyyy") : "-";
            if (rebateDetails.ApprovedAmount != null)
            {
                selectViewModel.ApprovedAmount = rebateDetails.ApprovedAmount.Substring(rebateDetails.ApprovedAmount.LastIndexOf('$') + 1);
            }
            else
            {
                selectViewModel.ApprovedAmount = rebateDetails.ApprovedAmount;
            }
            selectViewModel.ReasonIfDenied = rebateDetails.ReasonIfDenied;
            selectViewModel.StatusId = rebateDetails.status;

            if (rebateDetails.status == 3 && rebateDetails.ModifiedBy == 1)
            {
                selectViewModel.Status = "Approved";
            }
            else if (rebateDetails.status == 4 && rebateDetails.ModifiedBy == 1)
            {
                selectViewModel.Status = "Denied";
            }
            selectViewModel.Comments = rebateDetails.Comments;

            var builder = _db.Users.Where(x => x.Id == rebateSelect.BuilderId).First();

            UserViewModel userViewModel = new UserViewModel()
            {
                FirstName = builder.FirstName,
                LastName = builder.LastName,
                PhoneNumber = builder.PhoneNumber,
                Email = builder.Email
            };


            var rebateRedemption = _db.RebateRedemptions.Where(x => x.RebateId == rebateSelect.RebateId).FirstOrDefault();

            RebateRedemptionViewModel redemptionViewModel = new RebateRedemptionViewModel()
            {
                Name = rebateRedemption.Name,
                Address1 = rebateRedemption.Address1,
                Address2 = rebateRedemption.Address2,
                City = rebateRedemption.City,
                Zip = rebateRedemption.Zip
            };

            redemptionViewModel.StateName = _db.States.Where(x => x.Id == rebateRedemption.State).Select(a => a.Name).FirstOrDefault();

            var invoicePaths = _db.RebateInvoices.Where(x => x.RebateId == rebateSelect.RebateId).ToList();

            var pathViewModels = new List<InvoicePathViewModel>();
            foreach (var path in invoicePaths)
            {
                InvoicePathViewModel invoicePath = new InvoicePathViewModel();
                int type = path.InvoiceType;
                invoicePath.InvoicePath = path.InvoicePath;
                invoicePath.InvoiceType = "Select";

                pathViewModels.Add(invoicePath);
            }
            InvoiceDetailsViewModel detailsViewModel = new InvoiceDetailsViewModel()
            {
                RebateSelectViewModel = selectViewModel,
                RebateRedemptionViewModel = redemptionViewModel,
                InvoicePathViewModels = pathViewModels,
                UserViewModel = userViewModel
            };

            return View(detailsViewModel);
        }

        [HttpPost]
        public ActionResult InvoiceDetailsSelect(InvoiceDetailsViewModel detailsViewModel)
        {
            var selectViewModel = detailsViewModel.RebateSelectViewModel;
            var rebateSelect = _db.RebateSelects.Where(x => x.RecordNumber == selectViewModel.OrderNumber).FirstOrDefault();
            var rebateDetails = _db.RebateDetails.Where(x => x.Id == rebateSelect.RebateId).FirstOrDefault();
            _db.Entry(rebateDetails).State = System.Data.Entity.EntityState.Detached;

            RebateDetail rebate = new RebateDetail();
            if (selectViewModel.Status == "3")
            {
                rebate.Id = (int)rebateSelect.RebateId;
                rebate.ApprovedDeniedDate = DateTime.Now;
                rebate.ApprovedAmount = "$" + selectViewModel.ApprovedAmount;
                rebate.status = 3;
                rebate.ModifiedBy = 1;
                rebate.Comments = selectViewModel.Comments;

                _db.RebateDetails.Attach(rebate);
                _db.Entry(rebate).Property(x => x.status).IsModified = true;
                _db.Entry(rebate).Property(x => x.ModifiedBy).IsModified = true;
                _db.Entry(rebate).Property(x => x.ApprovedDeniedDate).IsModified = true;
                _db.Entry(rebate).Property(x => x.ApprovedAmount).IsModified = true;
                _db.Entry(rebate).Property(x => x.Comments).IsModified = true;
            }
            else if (selectViewModel.Status == "4")
            {
                rebate.Id = (int)rebateSelect.RebateId;
                rebate.ApprovedDeniedDate = DateTime.Now;
                rebate.status = 4;
                rebate.ModifiedBy = 1;
                rebate.ReasonIfDenied = selectViewModel.ReasonIfDenied;
                rebate.Comments = selectViewModel.Comments;

                _db.RebateDetails.Attach(rebate);
                _db.Entry(rebate).Property(x => x.status).IsModified = true;
                _db.Entry(rebate).Property(x => x.ModifiedBy).IsModified = true;
                _db.Entry(rebate).Property(x => x.ApprovedDeniedDate).IsModified = true;
                _db.Entry(rebate).Property(x => x.ReasonIfDenied).IsModified = true;
                _db.Entry(rebate).Property(x => x.Comments).IsModified = true;
            }
            _db.Configuration.ValidateOnSaveEnabled = false;
            _db.SaveChanges();
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult GetBuildersNotAssignedToRep()
        {
            var builders = _db.Users.Where(x => x.RoleId == 5).ToList();

            var buildersNotAssignedToRep = (from b in builders
                                            where !_db.RepBuilders.Any(x => x.BuilderId == b.Id)
                                            select new UserViewModel()
                                            {
                                                Id = b.Id,
                                                Email = b.Email,
                                                PhoneNumber = b.PhoneNumber,
                                                FirstName = b.FirstName,
                                                LastName = b.LastName
                                            }).ToList();
            return View(buildersNotAssignedToRep);
        }

        [HttpGet]
        public ActionResult AddBuilderToRep(string email)
        {
            var builder = _db.Users.Where(x => x.Email == email).FirstOrDefault();

            UserViewModel userViewModel = new UserViewModel()
            {
                Id = builder.Id,
                FirstName = builder.FirstName,
                LastName = builder.LastName,
                Email = builder.Email,
                PhoneNumber = builder.PhoneNumber,
                StateName = _db.States.Where(x => x.Id == builder.State).Select(x => x.Name).FirstOrDefault(),
                City = builder.City,
                Zip = builder.Zip,
            };

            var reps = _db.Users.Where(x => x.RoleId == 2).ToList();
            var userReps = new List<UserViewModel>();

            foreach (var rep in reps)
            {
                UserViewModel user = new UserViewModel()
                {
                    RoleName = _db.Roles.Where(x => x.Id == rep.RoleId).Select(x => x.Name).FirstOrDefault(),
                    Id = rep.Id
                };

                userReps.Add(user);
            };
            ViewBag.Roles = new SelectList(userReps, "Id", "RoleName");
            return View(userViewModel);
        }
        [HttpPost]
        public ActionResult AddBuilderToRep(UserViewModel user)
        {
            int repId = Convert.ToInt32(user.RoleName);
            RepBuilder repBuilder = new RepBuilder()
            {
                RepId = repId,
                BuilderId = user.Id,
                CreatedDate = DateTime.Now,
                ModifiedDate = DateTime.Now,
                ModifiedBy = 1,
                IsActive = true
            };
            _db.RepBuilders.Add(repBuilder);
            _db.SaveChanges();
            SendMailToRep(user.Id, repId);
            return RedirectToAction("GetBuildersNotAssignedToRep");
        }

        public int SendMailToRep(int userId, int repId)
        {
            var rep = _db.Users.Where(x => x.Id == repId).FirstOrDefault();
            var builder = _db.Users.Where(x => x.Id == userId).FirstOrDefault();
            string subject = "Lixil Builder Rebate - Action Required";
            string body = "Great news " + "<strong>" + rep.FirstName + "!</strong>" + " <strong>" + builder.FirstName + " " + builder.LastName + "</strong>" +
                          " has just submitted a request to earn a Lixil builder rebate." + "<br><br>";
            body += "Please log into <a href='https://localhost:44392/'>Lixil Builder Rebate </a> and approve/deny the request." + "<br><br>";
            body += "If you have any questions, please contact Lixil Builder Rebate headquarters - 877-239-9898." + "<br><br>";
            body += "Happy Selling!" + "<br><br>";
            body += "Lixil Builder Rebate" + ".<br>";
            body += "877-239-9878";
            MailHelper mailHelper = new MailHelper();
            var status = mailHelper.CommonMail(builder.Email, subject, body);
            return 1;
        }

    }
}