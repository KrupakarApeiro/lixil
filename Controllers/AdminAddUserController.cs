﻿using LixilRebate.Models;
using LixilRebate.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LixilRebate.Controllers
{
    public class AdminAddUserController : Controller
    {
        private LixilRebateEntities _db = new LixilRebateEntities();
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult AddUser()
        {
            List<SelectListItem> list = _db.Zipcodes.Where(x => x.IsActive == true).Select(s => new SelectListItem() { Text = s.Zipcodes, Value = s.Id.ToString(), Selected = false }).ToList();
            UserViewModel userVM = new UserViewModel();
            userVM.ZipcodesList = list;
            //ViewBag.zip = new SelectList(zip.ToList(), "Id", "Zipcode");
            var roles = _db.Roles.Where(a => a.Id != 1 && a.Id != 5).Select(s => new RoleViewModel { Id = s.Id, Name = s.Name }).ToList();
            ViewBag.roles = new SelectList(roles.OrderBy(m => m.Name).ToList(), "Id", "Name");
            return View(userVM);
        }

        [HttpPost]
        public ActionResult AddUser(UserViewModel obj)
        {
            var roles = _db.Roles.Where(a => a.Id != 1 && a.Id != 5).Select(s => new RoleViewModel { Id = s.Id, Name = s.Name }).ToList();
            ViewBag.roles = new SelectList(roles.OrderBy(m => m.Name).ToList(), "Id", "Name");
            List<SelectListItem> list = _db.Zipcodes.Where(x => x.IsActive == true).Select(s => new SelectListItem() { Text = s.Zipcodes, Value = s.Id.ToString(), Selected = false }).ToList();

            if (ModelState.IsValid)
            {
                try
                {
                    User objUser = new User();
                    objUser.FirstName = obj.FirstName;
                    objUser.LastName = obj.LastName;
                    objUser.Email = obj.EmailId;
                    //objUser.Zip = obj.ZipCode;
                    objUser.Password = obj.Password;
                    objUser.RoleId = obj.RoleId;

                    _db.Users.Add(objUser);
                    _db.SaveChanges();
                    obj.ZipcodesList = list;
                    List<SelectListItem> selectedItems = obj.ZipcodesList.Where(p => obj.ZipCode.Contains(int.Parse(p.Value))).ToList();

                    foreach (var zc in selectedItems)
                    {
                        RepZipCode repZipCode = new RepZipCode()
                        {
                            RepId = objUser.Id,
                            ZipCode = zc.Text.ToString(),
                            IsActive = true,
                            CreatedDate = DateTime.Now,
                        };

                        _db.RepZipCodes.Add(repZipCode);
                        _db.SaveChanges();
                    }
                    ModelState.Clear();
                    TempData["Success"] = "User Added Successfully!";
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                }
            }

            obj.ZipcodesList = list;
            return View(obj);
        }

        public JsonResult EmailExists(string EmailId)
        {
            return Json(!_db.Users.Any(a => a.Email == EmailId), JsonRequestBehavior.AllowGet);
        }

        public JsonResult ZipCodeExists(int[] ZipCode)
        {
            bool valid = true;
            if (ZipCode.Length > 0)
            {
                foreach (int i in ZipCode)
                {
                    if (!_db.RepZipCodes.Any(a => a.ZipCode == i.ToString())) { valid = false; }
                }
            }
            return Json(valid, JsonRequestBehavior.AllowGet);
        }
    }
}