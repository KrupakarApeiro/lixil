﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using LixilRebate.Models;
using LixilRebate.ViewModels;
using LixilRebate.Services;
using System.Configuration;
using LixilRebate.Constants;
using System.Web;

namespace LixilRebate.Controllers
{
    public class HomeController : Controller
    {
       
        [HttpGet]
        public ActionResult Index(string message)
        {
            
            //Response.Cookies["FullName"].Expires = DateTime.Now.AddDays(-1);
            //Response.Cookies["UserId"].Expires = DateTime.Now.AddDays(-1);
            //Response.Cookies["Role"].Expires = DateTime.Now.AddDays(-1);
            //Response.Cookies["LoginId"].Expires = DateTime.Now.AddDays(-1);

            if(Request.Cookies["Role"] !=null)
            {
                int RoleId = Convert.ToInt32(Request.Cookies["Role"].Value);
                if (RoleId == 2 || RoleId == 3 || RoleId == 4 || RoleId == 6)
                {
                    return RedirectToAction("Index", "Users");
                }
                else if (RoleId == 1)
                {
                    return RedirectToAction("Index", "Admin");
                }
                else if (RoleId == 5)
                {
                    return RedirectToAction("Index", "Builder");
                }
                else
                {
                    return View();
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(message))
                {

                    ViewBag.Message = message;
                }
                return View();
            }
            //if (Request.Cookies["UserName"] != null)
            //{
            //    if (Request.Cookies["Role"]?.Value != null)
            //    {
            //        if (Request.Cookies["Role"].Value.ToString() == "1")
            //            return RedirectToAction("Index");
            //        return RedirectToAction("Index");
            //    }
            //    Response.Cookies.Clear();
            //    Response.Cookies["FullName"].Expires = DateTime.Now.AddDays(-1);
            //    Response.Cookies["UserId"].Expires = DateTime.Now.AddDays(-1);
            //    Response.Cookies["Role"].Expires = DateTime.Now.AddDays(-1);
            //}
            
        }
        [HttpPost]
        public ActionResult Index(LoginViewModel loginVM)
        {
            if(ModelState.IsValid)
            {
                //Response.Cookies["FullName"].Expires = DateTime.Now.AddDays(-1);
                //Response.Cookies["UserId"].Expires = DateTime.Now.AddDays(-1);
                //Response.Cookies["Role"].Expires = DateTime.Now.AddDays(-1);

                UserService userService = new UserService();
                bool userExists = userService.UserExists(loginVM.Email);
                if (!userExists)
                {
                    ModelState.AddModelError("Password", "User does not exists.");
                    return View(loginVM);
                }
                User user = userService.VerifyUser(loginVM);

                if (user != null)
                {
                    if (user.IsActive == false)
                    {
                        ModelState.AddModelError(string.Empty, "Your account is deactivated.");

                    }
                    var fullname = user.FirstName + " " + user.LastName;

                   
                    Response.Cookies["FullName"].Value = fullname;
                    Response.Cookies["FullName"].Expires = DateTime.Now.AddDays(1);

                    Response.Cookies["UserId"].Value = user.Id.ToString();
                    Response.Cookies["UserId"].Expires = DateTime.Now.AddDays(1);

                    Response.Cookies["Role"].Value = user.RoleId.ToString();
                    Response.Cookies["Role"].Expires = DateTime.Now.AddDays(1);

                    Response.Cookies["LoginId"].Value = user.Id.ToString();
                    Response.Cookies["LoginId"].Expires = DateTime.Now.AddDays(1);

                    if (user.RoleId == 2 || user.RoleId == 3 || user.RoleId == 4 || user.RoleId == 6)
                    {
                        return RedirectToAction("Index", "Users");
                    }
                    else if (user.RoleId == 1)
                    {
                        return RedirectToAction("Index","Admin");
                    }
                    else if (user.RoleId == 5)
                    {
                        return RedirectToAction("Index", "Builder");
                    }
                    

                }
                ModelState.AddModelError("Password", "Wrong password. Try again or click Forgot password to reset it.");
                return View(loginVM);
            }
            return View();
        }
        public ActionResult Logout()
        {
            Response.Cookies.Clear();
            Response.Cookies["FullName"].Expires = DateTime.Now.AddDays(-1);
            Response.Cookies["UserId"].Expires = DateTime.Now.AddDays(-1);
            Response.Cookies["Role"].Expires = DateTime.Now.AddDays(-1);
            return RedirectToAction("Index", "Home");
        }
        [HttpGet]
        public ActionResult Register()
        {
            States();
            PlumbingWholeSaleStates();
            UserService userService = new UserService();
            UserViewModel userViewModel = new UserViewModel()
            {
                ProductName = userService.prodCat(),
                PlumbingManufacturingPartners = userService.ManufacturingPartners()
            };
            return View(userViewModel);
        }
        [HttpPost]
        public ActionResult Register(UserViewModel userViewModel)
        {

            UserService userService = new UserService();
            ModelState.Remove("Contact");
            ModelState.Remove("Description");
            ModelState.Remove("RoleName");
            ModelState.Remove("StateName");
            ModelState.Remove("PlumbWholePartState");
            ModelState.Remove("PlumbWholePartCity");
            ModelState.Remove("PlumbWholePartner");
            
            if (ModelState.IsValid)
            {
                if (userViewModel != null)
                {
                    string result = userService.RegisterUser(userViewModel);
                    if (result == "Success")
                    {
                        //redirect to dashboard
                        return RedirectToAction("Index", new { message = "New" });
                    }
                }
            }
            States();
          
            UserViewModel userVM = new UserViewModel()
            {
                ProductName = userService.prodCat()
            };
            return View(userVM);
         }
        [NonAction]
        public void States()
        {
            using(DBContext _db=new DBContext())
            {
                List<StateViewModel> states = _db.States.Where(x => (bool)x.IsActive).Select(s => new StateViewModel { Id = s.Id, Name = s.Name }).ToList();
                ViewBag.States = new SelectList(states.OrderBy(m => m.Name).ToList(), "Id", "Name");

            }
        }

        [NonAction]
        public void PlumbingWholeSaleStates()
        {
            using (DBContext _db = new DBContext())
            {
                List<PlumbWholePartnerStateViewModel> pstates = _db.PlumbingWholesalePartnerStates.Where(x => (bool)x.IsActive).Select(s => new PlumbWholePartnerStateViewModel { Id = s.Id, Name = s.Name }).ToList();
                ViewBag.pstates = new SelectList(pstates.OrderBy(m => m.Name).ToList(), "Id", "Name");

            }
        }

        public JsonResult EmailExists(string Email)
        {
            using (DBContext _db = new DBContext())
            {
                return Json(!_db.Users.Any(a => a.Email == Email && a.IsActive == true));
            }
                
        }

        public JsonResult PhoneNumberExists(string PhoneNumber)
        {
            using (DBContext _db = new DBContext())
            {
                return Json(!_db.Users.Any(a => a.PhoneNumber == PhoneNumber && a.IsActive == true));
            }

        }
        [HttpGet]
        public ActionResult ChangePassword()
        {
            return View();
        }
        [HttpPost]
        public ActionResult ChangePassword(ChangePasswordViewModel changePasswordViewModel)
        {
            var message = "";
            if (ModelState.IsValid)
            {
                UserService userService = new UserService();
                var userId=Request.Cookies["UserId"].Value.ToString();
                changePasswordViewModel.userId = Convert.ToInt32(userId);
                string result = userService.ChangePassword(changePasswordViewModel);
                if(result== "SamePassword")
                {
                    ModelState.AddModelError(string.Empty, "New password should be different from old password.");

                }
                if (result == "Success")
                {
                    ViewBag.Message = "ChangePass";
                    return RedirectToAction("Logout");
                    //return RedirectToAction("Index", "Home", new { message = "Password Changed Succesfully"});
                }
               
                return View();
            }
            return View();
        }
        [HttpGet]
        public ActionResult ForgotPassword()
        {
            return View();
        }
        [HttpPost]

        public ActionResult ForgotPassword(string Email)
        {
            using(DBContext _db=new DBContext())
            {
                UserService userService = new UserService();
                var user = _db.Users.Where(x => x.Email.Equals(Email)).FirstOrDefault();
                if (user != null)
                {
                    var confirmEmail = Url.Action("ResetPassword", "Home", new { id = user.Identifier });
                    string content = userService.ResetPasswordlink(user.Email, confirmEmail);
                    return RedirectToAction("Index", new { message = "ForgotPass" });
                }
                ModelState.AddModelError("Email", "Email Does Not Exist.");
                return View();
            }
        }
        [HttpGet]
        [Route("ResetPassword/{id}")]
        public ActionResult ResetPassword(string id)
        {
            if (!string.IsNullOrEmpty(id))
            {
                TempData["Identifier"] = id;
                return View();
            }
            return View();
        }

        [HttpPost]
        public ActionResult ResetPassword(ResetPasswordViewModel resetPassword)
        {
            if (ModelState.IsValid)
            {
                resetPassword.Identifier = TempData["Identifier"].ToString();
                UserService userService = new UserService();
                string result = userService.ResetPassword(resetPassword);
                if (result == "Success")
                {
                    return RedirectToAction("Index", new { message = "ResetPassword" });

                }
                else if (result == "NotFound")
                {
                    ModelState.AddModelError("ConfirmPassword", "User not found.");
                    return View();
                }
                else
                {
                    ModelState.AddModelError("ConfirmPassword", "Please contact Admin.");
                    return View();
                }
              
            }

            return View(resetPassword);

        }
        [HttpGet]
        public ActionResult ContactUs()
        {
            return View();
        }
        [HttpPost]
        public ActionResult ContactUs(ContactUsViewModel contactUs)
        {
            if (ModelState.IsValid)
            {
                UserService userService = new UserService();
                var userId = Request.Cookies["UserId"] != null ? Request.Cookies["UserId"].Value : "";
                var roleid = Request.Cookies["Role"] != null ? Request.Cookies["Role"].Value : "";
                var loggedId = Request.Cookies["FullName"] != null ? Request.Cookies["FullName"].Value : "";
                contactUs.UserId = userId;
                var result = userService.ContactUs(contactUs);
                if(result=="Success")
                {
                    if(loggedId == null || loggedId == "")
                        return RedirectToAction("Index", "Home");
                    if (roleid == "2" || roleid == "3" || roleid == "4" || roleid == "6")
                        return RedirectToAction("Index", "Users");
                   else if(roleid == "5")
                        return RedirectToAction("Index", "Builder");
                    else return RedirectToAction("Index", "Admin");
                }
            }
            return View(contactUs);
        }

        [HttpGet]
        public ActionResult MyProfile()
        {
            using (DBContext _db = new DBContext())
            {
                States();
                var userId = Request.Cookies["UserId"] != null ? Request.Cookies["UserId"].Value : "";
                UserService userService = new UserService();
               
                if (userId != "")
                {
                    var user = userService.GetUserDetails(userId);
                    if (user != null)
                    {
                        return View(user);
                    }
                    return RedirectToAction("Logout");
                }
            }
              
                return RedirectToAction("Logout");
            
        }
        [HttpPost]
        public ActionResult MyProfile(UserEditViewModel userVM)
        {
            if (ModelState.IsValid)
            {
                var userId = Request.Cookies["UserId"] != null ? Request.Cookies["UserId"].Value : "";
                var roleid = Request.Cookies["Role"] != null ? Request.Cookies["Role"].Value : "";
                var loggedId = Request.Cookies["FullName"] != null ? Request.Cookies["FullName"].Value : "";
                userVM.Id = Convert.ToInt32(userId);
                UserService userService = new UserService();
                var result = userService.PostUserDetails(userVM);
                if (result == "Success")
                {
                    if (roleid == "2" || roleid == "3" || roleid == "4" || roleid == "6")
                        return RedirectToAction("Index", "Users");
                    else if (roleid == "5")
                        return RedirectToAction("Index", "Builder");
                    else if (roleid == "1") 
                        return RedirectToAction("Index", "Admin");
                    
                }
             
            }
            return RedirectToAction("Index");
        }

        public JsonResult GetPlumbingWholesalecities(int id)
        {
            using (DBContext _db = new DBContext())
            {
                var cities = _db.PlumbingWholesalePartnerCities.Where(x => x.StateId == id  && x.IsActive == true).OrderByDescending(o => o.Id).Select(s => new PlumbWholePartnersCityViewModel
                { Id = s.Id, Name = s.Name }).ToList();

                return Json(
             new SelectList(cities.OrderBy(m => m.Name).ToList(), "Id", "Name"), JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetWholesalePartnersByCityId(int id, int stateid)
        {
            using (DBContext _db = new DBContext())
            {
                var cities = _db.PlumbingWholesalePartners.Where(x => x.State == stateid && x.City == id && x.IsActive == true).OrderByDescending(o => o.Id).Select(s => new PlumbWholeSalePartnerViewModel
                { Id = s.Id, WholesalePartnerName = s.WholesalePartnerName }).ToList();

                return Json(
             new SelectList(cities.OrderBy(m => m.WholesalePartnerName).ToList(), "Id", "WholesalePartnerName"), JsonRequestBehavior.AllowGet);
            }
        }

    }
}