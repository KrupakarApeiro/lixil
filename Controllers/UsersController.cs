﻿using HtmlAgilityPack;
using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using LixilRebate.Constants;
using LixilRebate.Models;
using LixilRebate.Services;
using LixilRebate.ViewModels;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace LixilRebate.Controllers
{
    public class UsersController : Controller
    {
        // GET: Users
        private readonly DBContext _db = new DBContext();
        MailService mailService = new MailService();
        public ActionResult Index()
        {
            var loginid = Convert.ToInt32(Request.Cookies["LoginId"].Value);
            var roleid = Convert.ToInt32(Request.Cookies["Role"].Value);
            var today = DateTime.Now;
            var future3mon = DateTime.Now.AddMonths(3);
            var future6mon = DateTime.Now.AddMonths(6);
            var builderids = (List<int?>)null;
            var repbuilderids = (List<int>)null;
            //rep
            if (Request.Cookies["Role"].Value == "2")
            {
                if (_db.RepBuilders.Any(s => s.RepId == loginid))
                {
                    builderids = _db.RepBuilders.Where(s => s.RepId == loginid).Select(p => p.BuilderId).ToList();
                    ViewBag.repcount = _db.Users.Where(t => builderids.Contains(t.Id) && t.ModifiedBy == 1).Count();
                }
                if (_db.Users.Any(s => s.RepId == loginid))
                {
                    repbuilderids = _db.Users.Where(s => s.RepId == loginid).Select(p => p.Id).ToList();
                    if (_db.Users.Any(s => repbuilderids.Contains(s.Id) && s.RoleId == 5 && s.ModifiedBy == 2))
                        ViewBag.mancount = _db.Users.Where(s => repbuilderids.Contains(s.Id) && s.RoleId == 5 && s.ModifiedBy == 2).Count();
                    if (_db.Users.Any(s => repbuilderids.Contains(s.Id) && s.RoleId == 5 && s.ModifiedBy == 3 && s.Type == 2))
                        ViewBag.fincount = _db.Users.Where(s => repbuilderids.Contains(s.Id) && s.RoleId == 5 && s.ModifiedBy == 3 && s.Type == 2).Count();
                    if (_db.ContractDetails.Any(s => repbuilderids.Contains(s.BuilderId) && s.IsActive == true && s.ContractStatus == 1 && s.UploadStatus == "Unsigned"))
                        ViewBag.unsignedcnt = _db.ContractDetails.Where(s => repbuilderids.Contains(s.BuilderId) && s.IsActive == true && s.ContractStatus == 1 && s.UploadStatus == "Unsigned").Count();
                    if (_db.ContractDetails.Any(s => repbuilderids.Contains(s.BuilderId) && s.ContractStatus == 2 && s.ModifiedBy == 4))
                        ViewBag.appcontrcnt = _db.ContractDetails.Where(s => repbuilderids.Contains(s.BuilderId) && s.ContractStatus == 2 && s.ModifiedBy == 4).Count();
                    if (_db.ContractDetails.Any(s => repbuilderids.Contains(s.BuilderId) && s.ContractStatus == 3))
                        ViewBag.dencontrcnt = _db.ContractDetails.Where(s => repbuilderids.Contains(s.BuilderId) && s.ContractStatus == 3).Count();
                    if (_db.ContractDetails.Any(s => repbuilderids.Contains(s.BuilderId) && s.IsActive == true && s.ContractStatus == 2 && s.ExpirationDate >= today && s.ExpirationDate <= future3mon))
                        ViewBag.expiry3cnt = _db.ContractDetails.Where(s => repbuilderids.Contains(s.BuilderId) && s.IsActive == true && s.ContractStatus == 2 && s.ExpirationDate >= today && s.ExpirationDate <= future3mon).Count();
                    if (_db.ContractDetails.Any(s => repbuilderids.Contains(s.BuilderId) && s.IsActive == true && s.ExpirationDate >= today && s.ContractStatus == 2 && s.ExpirationDate <= future6mon))
                        ViewBag.expiry6cnt = _db.ContractDetails.Where(s => repbuilderids.Contains(s.BuilderId) && s.IsActive == true && s.ContractStatus == 2 && s.ExpirationDate >= today && s.ExpirationDate <= future6mon).Count();
                    var countr = 0;
                    if (_db.ContractDetails.Any(s => repbuilderids.Contains(s.BuilderId) && s.UploadStatus == "Signed" && s.ModifiedBy == 4 && s.ContractStatus == 2))
                        countr = _db.ContractDetails.Where(s => repbuilderids.Contains(s.BuilderId) && s.UploadStatus == "Signed" && s.ModifiedBy == 4 && s.ContractStatus == 2).Count();
                    if (_db.Users.Any(p => repbuilderids.Contains(p.Id) && p.Type == 1 && p.ModifiedBy == 3 && p.BuilderStatusId == 2))
                        countr += _db.Users.Where(p => repbuilderids.Contains(p.Id) && p.Type == 1 && p.ModifiedBy == 3 && p.BuilderStatusId == 2).Count();
                    ViewBag.editcnt = countr;
                }


            }
            //endrep
            else
            {
                if (_db.Users.Any(s => s.RoleId == 5 && s.ModifiedBy == 2))
                    ViewBag.mancount = _db.Users.Where(s => s.RoleId == 5 && s.ModifiedBy == 2).Count();
                if (_db.Users.Any(s => s.RoleId == 5 && s.ModifiedBy == 3 && s.Type == 2))
                    ViewBag.fincount = _db.Users.Where(s => s.RoleId == 5 && s.ModifiedBy == 3 && s.Type == 2).Count();
                if (roleid == 3)
                {
                    if (_db.ContractDetails.Any(s => s.IsActive == true & s.ModifiedBy == 2 && s.ContractStatus == 2))
                        ViewBag.pensignedcnt = _db.ContractDetails.Where(s => s.IsActive == true & s.ModifiedBy == 2 && s.ContractStatus == 2).Count();
                }

                else
                {
                    if (_db.ContractDetails.Any(s => s.IsActive == true & s.ModifiedBy == 3 && s.ContractStatus == 2))
                        ViewBag.pensignedcnt = _db.ContractDetails.Where(s => s.IsActive == true & s.ModifiedBy == 3 && s.ContractStatus == 2).Count();
                }

                if (_db.ContractDetails.Any(s => s.ContractStatus == 2 && s.ModifiedBy == 4))
                    ViewBag.appcontrcnt = _db.ContractDetails.Where(s => s.ContractStatus == 2 && s.ModifiedBy == 4).Count();
                if (_db.ContractDetails.Any(s => s.ContractStatus == 3))
                    ViewBag.dencontrcnt = _db.ContractDetails.Where(s => s.ContractStatus == 3).Count();
                if (_db.ContractDetails.Any(s => s.IsActive == true && s.ContractStatus == 1 && s.UploadStatus == "Unsigned"))
                    ViewBag.unsignedcnt = _db.ContractDetails.Where(s => s.IsActive == true && s.ContractStatus == 1 && s.UploadStatus == "Unsigned").Count();
                if (_db.ContractDetails.Any(s => s.IsActive == true && s.ContractStatus == 2 && s.ExpirationDate >= today && s.ExpirationDate <= future3mon))
                    ViewBag.expiry3cnt = _db.ContractDetails.Where(s => s.IsActive == true && s.ContractStatus == 2 && s.ExpirationDate >= today && s.ExpirationDate <= future3mon).Count();
                if (_db.ContractDetails.Any(s => s.IsActive == true && s.ExpirationDate >= today && s.ContractStatus == 2 && s.ExpirationDate <= future6mon))
                    ViewBag.expiry6cnt = _db.ContractDetails.Where(s => s.IsActive == true && s.ContractStatus == 2 && s.ExpirationDate >= today && s.ExpirationDate <= future6mon).Count();
                var count = 0;
                if (_db.ContractDetails.Any(s => s.UploadStatus == "Signed" && s.ModifiedBy == 4 && s.ContractStatus == 2))
                    count = _db.ContractDetails.Where(s => s.UploadStatus == "Signed" && s.ModifiedBy == 4 && s.ContractStatus == 2).Count();
                if (_db.Users.Any(p => p.Type == 1 && p.ModifiedBy == 3 && p.BuilderStatusId == 2))
                    count += _db.Users.Where(p => p.Type == 1 && p.ModifiedBy == 3 && p.BuilderStatusId == 2).Count();
                ViewBag.editcnt = count;
            }
            return View();
        }

        public ActionResult BuildersGrid(string roleid)
        {
            ViewBag.role = roleid;
            var loginid = Convert.ToInt32(Request.Cookies["LoginId"].Value);
            if (roleid == "2")
            {
                if (_db.RepBuilders.Any(s => s.RepId == loginid))
                {
                    var builderids = _db.RepBuilders.Where(s => s.RepId == loginid).Select(p => p.BuilderId).ToList();
                    if (_db.Users.Any(t => builderids.Contains(t.Id) && t.ModifiedBy == 1))
                    {
                        var pendingusers = _db.Users.Where(t => builderids.Contains(t.Id) && t.ModifiedBy == 1).Select(p => new BuilderDetailsViewModel
                        {
                            BuilderId = p.Id,
                            CompanyName = p.CompanyName,
                            City = p.City,
                            State = _db.States.Where(s => s.Id == p.State).Select(t => t.Name).FirstOrDefault(),
                            RegDate = p.CreatedDate,
                            BuilderStatus = p.BuilderStatusId == 1 ? "Pending" : "Denied"
                        }).ToList();
                        return View(pendingusers);
                    }
                }
            }

            else if (roleid == "3")
            {
                if (Request.Cookies["Role"].Value == "2")
                {
                    if (_db.Users.Any(s => s.RepId == loginid))
                    {
                        var builderids = _db.Users.Where(s => s.RepId == loginid).Select(p => p.Id).ToList();
                        if (_db.Users.Any(s => builderids.Contains(s.Id) && s.RoleId == 5 && s.ModifiedBy == 2 && (s.BuilderStatusId == 2 || s.BuilderStatusId == 3)))
                        {
                            var pendingusers = _db.Users.Where(s => builderids.Contains(s.Id) && s.RoleId == 5 && s.ModifiedBy == 2 && (s.BuilderStatusId == 2 || s.BuilderStatusId == 3)).ToList();
                            if (pendingusers != null && pendingusers.Count() > 0)
                            {
                                var penusersVM = pendingusers.Select(p => new BuilderDetailsViewModel
                                {
                                    BuilderId = p.Id,
                                    CompanyName = p.CompanyName,
                                    City = p.City,
                                    State = _db.States.Where(s => s.Id == p.State).Select(t => t.Name).FirstOrDefault(),
                                    LastApproval = "Rep",
                                    ModifiedDate = p.ModifiedDate,
                                    RegDate = p.CreatedDate,
                                    RebateType = p.Type == 1 ? "Rewards" : "Select",
                                    BuilderStatus = p.BuilderStatusId == 2 ? "Pending" : "Denied",
                                }).ToList();
                                return View(penusersVM);
                            }
                        }
                    }
                }
                else
                {
                    if (_db.Users.Any(s => s.RoleId == 5 && s.ModifiedBy == 2 && (s.BuilderStatusId == 2 || s.BuilderStatusId == 3)))
                    {
                        var pendingusers = _db.Users.Where(s => s.RoleId == 5 && s.ModifiedBy == 2 && (s.BuilderStatusId == 2 || s.BuilderStatusId == 3)).ToList();
                        if (pendingusers != null && pendingusers.Count() > 0)
                        {
                            var penusersVM = pendingusers.Select(p => new BuilderDetailsViewModel
                            {
                                BuilderId = p.Id,
                                CompanyName = p.CompanyName,
                                City = p.City,
                                State = _db.States.Where(s => s.Id == p.State).Select(t => t.Name).FirstOrDefault(),
                                LastApproval = "Rep",
                                ModifiedDate = p.ModifiedDate,
                                RegDate = p.CreatedDate,
                                RebateType = p.Type == 1 ? "Rewards" : "Select",
                                BuilderStatus = p.BuilderStatusId == 2 ? "Pending" : "Denied",
                            }).ToList();
                            return View(penusersVM);
                        }
                    }
                }
            }

            else
            {
                if (Request.Cookies["Role"].Value == "2")
                {
                    if (_db.Users.Any(s => s.RepId == loginid))
                    {
                        var builderids = _db.Users.Where(s => s.RepId == loginid).Select(p => p.Id).ToList();
                        if (_db.Users.Any(s => builderids.Contains(s.Id) && s.RoleId == 5 && s.ModifiedBy == 3 && s.Type == 2 && (s.BuilderStatusId == 2 || s.BuilderStatusId == 3)))
                        {
                            var pendingusers = _db.Users.Where(s => builderids.Contains(s.Id) && s.RoleId == 5 && s.Type == 2 && s.ModifiedBy == 3 && (s.BuilderStatusId == 2 || s.BuilderStatusId == 3)).ToList();
                            if (pendingusers != null && pendingusers.Count() > 0)
                            {
                                var penusersVM = pendingusers.Select(p => new BuilderDetailsViewModel
                                {
                                    BuilderId = p.Id,
                                    CompanyName = p.CompanyName,
                                    City = p.City,
                                    State = _db.States.Where(s => s.Id == p.State).Select(t => t.Name).FirstOrDefault(),
                                    LastApproval = "Manager",
                                    ModifiedDate = p.ModifiedDate,
                                    RegDate = p.CreatedDate,
                                    RebateType = p.Type == 1 ? "Rewards" : "Select",
                                    BuilderStatus = p.BuilderStatusId == 2 ? "Pending" : "Denied"
                                }).ToList();
                                return View(penusersVM);
                            }
                        }
                    }
                }
                else
                {
                    if (_db.Users.Any(s => s.RoleId == 5 && s.ModifiedBy == 3 && s.Type == 2 && (s.BuilderStatusId == 2 || s.BuilderStatusId == 3)))
                    {
                        var pendingusers = _db.Users.Where(s => s.RoleId == 5 && s.Type == 2 && s.ModifiedBy == 3 && (s.BuilderStatusId == 2 || s.BuilderStatusId == 3)).ToList();
                        if (pendingusers != null && pendingusers.Count() > 0)
                        {
                            var penusersVM = pendingusers.Select(p => new BuilderDetailsViewModel
                            {
                                BuilderId = p.Id,
                                CompanyName = p.CompanyName,
                                City = p.City,
                                State = _db.States.Where(s => s.Id == p.State).Select(t => t.Name).FirstOrDefault(),
                                LastApproval = "Manager",
                                ModifiedDate = p.ModifiedDate,
                                RegDate = p.CreatedDate,
                                RebateType = p.Type == 1 ? "Rewards" : "Select",
                                BuilderStatus = p.BuilderStatusId == 2 ? "Pending" : "Denied"
                            }).ToList();
                            return View(penusersVM);
                        }
                    }
                }
            }
            return View();
        }

        public ActionResult BuilderDetails(int id, string roleid, int? edit)
        {
            ViewBag.role = roleid;
            var amts = _db.ModelCoOpAmounts.Where(x => x.IsActive == true).Select(s => new AmountViewModel { AmountId = s.Id, AmountName = s.Amount }).ToList();
            var repid = _db.Users.Find(id).RepId;
            if(repid != null)
            {

                //var repid = _db.RepBuilders.Where(s => s.BuilderId == id && s.IsActive == true).Select(p => p.RepId).FirstOrDefault();
                var name = _db.Users.Where(s => s.Id == repid).Select(p => new { p.FirstName, p.LastName }).FirstOrDefault();
                ViewBag.repname = name.FirstName + " " + name.LastName;
            }



            if (_db.Users.Any(x => x.Id == id && x.RoleId == 5)) //1
            {
                var data = _db.Users.Where(x => x.Id == id && x.RoleId == 5).ToList().Select(s => new BuilderDetailsViewModel //1
                {
                    BuilderId = s.Id,
                    CompanyName = s.CompanyName,
                    City = s.City,
                    State = _db.States.Where(a => a.Id == s.State).Select(t => t.Name).FirstOrDefault(),
                    RegDate = s.CreatedDate,
                    DateString = s.CreatedDate.Value.ToString("MM/dd/yyyy"),
                    FirstName = s.FirstName,
                    LastName = s.LastName,
                    PhoneNumber = s.PhoneNumber,
                    AnnualNoOfHomes = s.AnnualNumberOfHomes,
                    AnnualHomes = Convert.ToInt32(s.AnnualNumberOfHomes),
                    AvgPriceUnit = s.AveragePriceUnit,
                    WebsiteURL = s.WebsiteURL,
                    PercentPlumbingUpgrades = s.PercentageOfPlumbingUpgrades,
                    PlumbManufacturePartners = (from p in _db.UserPlumbingManufacturers
                                                join q in _db.PlumbingManufacturingPartners on p.PlumbingManufactureId equals q.Id
                                                where p.UserId == id
                                                select q.Name).ToList(),
                    InstallationPlumb = s.InstallationPlumbers,
                    PlumbWholesalePartners = (from p in _db.UserPlumbingWholesalePartners
                                              join q in _db.PlumbingWholesalePartners on p.PWholesalePartner equals q.Id
                                              where p.UserId == id
                                              select q.WholesalePartnerName).ToList(),
                    ProductCategories = (from p in _db.UserProdCategories
                                         join q in _db.ProductCategories on p.ProductCategoryId equals q.ProductCategoryId
                                         where p.UserId == id
                                         select q.ProductName).ToList(),
                    BuilderStatus = s.BuilderStatusId.ToString(),
                    ModelHomeCoOp = s.ModelHomeCoOp != null ? s.ModelHomeCoOp : "",
                    PerHouseRebate = s.PerHouseRebate != null ? s.PerHouseRebate : "",
                    TermAgreement = s.TermAgreement != null ? s.TermAgreement : "",
                    RebateType = s.Type.ToString(), // s.Type == 2 ? "Select" : "Rewards",
                    ReasonDenied = s.ReasonDenied != null ? s.ReasonDenied : "",
                    OpportunityBoxNotes = s.OpportunityBoxNotes != null ? s.OpportunityBoxNotes : "",
                    Value = s.Value != null && s.Value > 0 ? s.Value.ToString() : "",
                    Amount = s.Value != null && s.Value > 0 ? _db.ModelCoOpAmounts.Find(s.Value).Amount.ToString() : "",
                    OtherAmount = s.OtherAmount != null && s.OtherAmount > 0 ? s.OtherAmount.ToString() : "",
                    Edit = s.Type == 2 ? 0 : edit,
                    ContractLengthYears = s.ContactLengthYears,
                    MasterBathroom = (from p in _db.UserRoomProductSKUs
                                      join q in _db.Products on p.ProductSkuId equals q.Id
                                      join r in _db.Manufacturers on q.Manufacturer equals r.id
                                      where p.UserId == id && p.RoomId == 1
                                      select q.SKU + "_" + r.mfg_name).ToList(),
                    sBathroom1 = (from p in _db.UserRoomProductSKUs
                                  join q in _db.Products on p.ProductSkuId equals q.Id
                                  join r in _db.Manufacturers on q.Manufacturer equals r.id
                                  where p.UserId == id && p.RoomId == 2
                                  select q.SKU + "_" + r.mfg_name).ToList(),
                    sBathroom2 = (from p in _db.UserRoomProductSKUs
                                  join q in _db.Products on p.ProductSkuId equals q.Id
                                  join r in _db.Manufacturers on q.Manufacturer equals r.id
                                  where p.UserId == id && p.RoomId == 3
                                  select q.SKU + "_" + r.mfg_name).ToList(),
                    sBathroom3 = (from p in _db.UserRoomProductSKUs
                                  join q in _db.Products on p.ProductSkuId equals q.Id
                                  join r in _db.Manufacturers on q.Manufacturer equals r.id
                                  where p.UserId == id && p.RoomId == 4
                                  select q.SKU + "_" + r.mfg_name).ToList(),
                    sKitchen1 = (from p in _db.UserRoomProductSKUs
                                 join q in _db.Products on p.ProductSkuId equals q.Id
                                 join r in _db.Manufacturers on q.Manufacturer equals r.id
                                 where p.UserId == id && p.RoomId == 5
                                 select q.SKU + "_" + r.mfg_name).ToList(),
                    sKitchen2 = (from p in _db.UserRoomProductSKUs
                                 join q in _db.Products on p.ProductSkuId equals q.Id
                                 join r in _db.Manufacturers on q.Manufacturer equals r.id
                                 where p.UserId == id && p.RoomId == 6
                                 select q.SKU + "_" + r.mfg_name).ToList(),
                    sLaundryroom = (from p in _db.UserRoomProductSKUs
                                    join q in _db.Products on p.ProductSkuId equals q.Id
                                    join r in _db.Manufacturers on q.Manufacturer equals r.id
                                    where p.UserId == id && p.RoomId == 7
                                    select q.SKU + "_" + r.mfg_name).ToList(),
                    sOther = (from p in _db.UserRoomProductSKUs
                              join q in _db.Products on p.ProductSkuId equals q.Id
                              join r in _db.Manufacturers on q.Manufacturer equals r.id
                              where p.UserId == id && p.RoomId == 8
                              select q.SKU + "_" + r.mfg_name).ToList(),


                }).FirstOrDefault();

                ViewBag.amts = new SelectList(amts.ToList(), "AmountId", "AmountName", data.Value);

                var rooms = _db.Rooms.ToList().Select(p => new RoomViewModel { Id = p.Id, RoomName = p.RoomName });
                ViewBag.rooms = new SelectList(rooms, "Id", "RoomName");

                var products = _db.Products.ToList().Select(p => new ProductViewModel { Id = p.Id, SKU = p.SKU + " _ " + _db.Manufacturers.Where(x => x.id == p.Manufacturer).Select(a => a.mfg_name).FirstOrDefault() });
                ViewBag.Products = new SelectList(products, "Id", "SKU");
                return View(data);
            }
            else
                ViewBag.amts = new SelectList(amts.ToList(), "AmountId", "AmountName");
            return View();
        }

        [HttpPost]
        public ActionResult BuilderDetails(BuilderDetailsViewModel builderDetailsViewModel)
        {
            try
            {
                ModelState.Remove("Room");
                ModelState.Remove("ProductNumber");
                var loginid = Convert.ToInt32(Request.Cookies["LoginId"].Value);
                var rooms = _db.Rooms.ToList().Select(p => new RoomViewModel { Id = p.Id, RoomName = p.RoomName });
                ViewBag.rooms = new SelectList(rooms, "Id", "RoomName");
                var builder = _db.Users.Find(builderDetailsViewModel.BuilderId);
                BuilderStatusChanx obj = new BuilderStatusChanx();
                var note = "";
                var redirect = "";
                var user = _db.Users.Find(Convert.ToInt32(Request.Cookies["LoginId"].Value));
                var uname = user.FirstName + " " + user.LastName;
                var uemail = user.Email;
                if (builderDetailsViewModel.Edit == 1)
                {
                    if (builder.Type.ToString() != builderDetailsViewModel.RebateType && builderDetailsViewModel.BuilderStatus == "2")
                    {
                        builder.ModifiedBy = 2;
                        if (builderDetailsViewModel.RebateType == "1" && _db.ContractDetails.Any(s => s.BuilderId == builder.Id))
                        {
                            var data = _db.ContractDetails.Where(p => p.BuilderId == builder.Id).FirstOrDefault();
                            data.ModifiedBy = 2;
                            data.ExpirationDate = null;
                            data.UploadStatus = "Signed";
                            _db.SaveChanges();
                        }
                        if (builderDetailsViewModel.RebateType == "2")
                        {
                            if (builderDetailsViewModel.RoomProductSKUs != null)
                            {
                                string[] splits = builderDetailsViewModel.RoomProductSKUs.Split('#');
                                foreach (var w in splits)
                                {
                                    var roomproduct = w.Split('|');
                                    if (roomproduct[0] != "")
                                    {
                                        var room = roomproduct[0];
                                        bool exists = _db.UserRoomProductSKUs.Any(x => x.UserId == builderDetailsViewModel.BuilderId && x.RoomId.ToString() == room);
                                        if (!exists)
                                        {
                                            var productskusplits = roomproduct[1].Split(',');
                                            foreach (var product in productskusplits)
                                            {
                                                UserRoomProductSKU userRoomProductSKU = new UserRoomProductSKU()
                                                {
                                                    UserId = builderDetailsViewModel.BuilderId,
                                                    RoomId = Convert.ToInt32(roomproduct[0]),
                                                    ProductSkuId = Convert.ToInt32(product),

                                                };
                                                _db.UserRoomProductSKUs.Add(userRoomProductSKU);
                                                _db.SaveChanges();
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            if (_db.UserRoomProductSKUs.Any(s => s.UserId == builderDetailsViewModel.BuilderId))
                            {
                                _db.UserRoomProductSKUs.RemoveRange(_db.UserRoomProductSKUs.Where(s => s.UserId == builderDetailsViewModel.BuilderId));
                                _db.SaveChanges();
                            }
                        }
                    }

                    builder.BuilderStatusId = builderDetailsViewModel.BuilderStatus == "2" ? 2 : 3;
                    builder.Type = Convert.ToInt32(builderDetailsViewModel.RebateType);
                    builder.ModelHomeCoOp = builder.Type == 2 ? builderDetailsViewModel.ModelHomeCoOp : null;
                    builder.PerHouseRebate = builder.Type == 2 ? builderDetailsViewModel.PerHouseRebate : null;
                    builder.TermAgreement = builder.Type == 2 ? builderDetailsViewModel.TermAgreement : null;
                    builder.ContactLengthYears = builder.Type == 2 ? builderDetailsViewModel.ContractLengthYears : null;
                    builder.Value = builder.Type == 2 ? Convert.ToInt32(builderDetailsViewModel.Value) : 0;
                    builder.OtherAmount = builder.Type == 2 ? Convert.ToDecimal(builderDetailsViewModel.OtherAmount) : 0;
                    builder.OpportunityBoxNotes = builderDetailsViewModel.OpportunityBoxNotes;
                    builder.ReasonDenied = builder.BuilderStatusId == 2 ? "" : builderDetailsViewModel.ReasonDenied;
                    builder.ModifiedDate = DateTime.Now;
                    builder.RepId = loginid;
                    _db.SaveChanges();
                    note = "Edited by Rep ";
                    redirect = "BuildersGrid";
                }

                else if (Request.Cookies["Role"].Value.ToString() == "2")
                {
                    builder.BuilderStatusId = builderDetailsViewModel.BuilderStatus == "2" ? 2 : 3;
                    builder.Type = Convert.ToInt32(builderDetailsViewModel.RebateType);
                    builder.ModelHomeCoOp = builder.Type == 2 ? builderDetailsViewModel.ModelHomeCoOp : "";
                    builder.PerHouseRebate = builder.Type == 2 ?  builderDetailsViewModel.PerHouseRebate : "";
                    builder.TermAgreement = builder.Type == 2 ? builderDetailsViewModel.TermAgreement : null;
                    builder.ContactLengthYears = builder.Type == 2 ? builderDetailsViewModel.ContractLengthYears : null;
                    builder.Value = builder.Type == 2 ? Convert.ToInt32(builderDetailsViewModel.Value) : 0;
                    builder.OtherAmount = builder.Type == 2 ? Convert.ToDecimal(builderDetailsViewModel.OtherAmount) : 0;
                    builder.OpportunityBoxNotes = builderDetailsViewModel.OpportunityBoxNotes;
                    builder.ReasonDenied = builder.BuilderStatusId == 2 ? "" : builderDetailsViewModel.ReasonDenied;
                    builder.ModifiedDate = DateTime.Now;
                    builder.ModifiedBy = builder.BuilderStatusId == 2 ? 2 : 1;
                    builder.RepId = loginid;
                    _db.SaveChanges();

                    note = "Builder Registration by Rep ";
                    redirect = "BuildersGrid";

                    if (builder.Type == 2)
                    {
                        if (builderDetailsViewModel.RoomProductSKUs != null)
                        {
                            string[] splits = builderDetailsViewModel.RoomProductSKUs.Split('#');
                            foreach (var w in splits)
                            {
                                var roomproduct = w.Split('|');
                                if (roomproduct[0] != "")
                                {
                                    var room = roomproduct[0];
                                    bool exists = _db.UserRoomProductSKUs.Any(x => x.UserId == builderDetailsViewModel.BuilderId && x.RoomId.ToString() == room);
                                    if (!exists)
                                    {
                                        var productskusplits = roomproduct[1].Split(',');
                                        foreach (var product in productskusplits)
                                        {
                                            UserRoomProductSKU userRoomProductSKU = new UserRoomProductSKU()
                                            {
                                                UserId = builderDetailsViewModel.BuilderId,
                                                RoomId = Convert.ToInt32(roomproduct[0]),
                                                ProductSkuId = Convert.ToInt32(product),

                                            };
                                            _db.UserRoomProductSKUs.Add(userRoomProductSKU);
                                            _db.SaveChanges();
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        if (_db.UserRoomProductSKUs.Any(s => s.UserId == builderDetailsViewModel.BuilderId))
                        {
                            _db.UserRoomProductSKUs.RemoveRange(_db.UserRoomProductSKUs.Where(s => s.UserId == builderDetailsViewModel.BuilderId));
                            _db.SaveChanges();
                        }
                    }

                    if (builder.BuilderStatusId == 3)
                        mailService.SendBuilderDeniedMail(builder.FirstName + " " + builder.LastName, builder.ReasonDenied, uname, uemail, builder.Email);

                    else if (builder.BuilderStatusId == 2)
                        mailService.SendMailManagerFinance(builder.FirstName + " " + builder.LastName, uname, "manager");

                }
                else if (Request.Cookies["Role"].Value.ToString() == "3")
                {
                    builder.BuilderStatusId = builderDetailsViewModel.BuilderStatus == "2" ? 2 : 3;
                    builder.ModifiedBy = builder.BuilderStatusId == 2 ? 3 : 2;
                    builder.ReasonDenied = builder.BuilderStatusId == 2 ? "" : builderDetailsViewModel.ReasonDenied;
                    builder.ModifiedDate = DateTime.Now;
                    _db.SaveChanges();
                    note = "Builder Registration by Manager ";
                    redirect = "BuildersGrid";
                    var repid = _db.Users.Find(builderDetailsViewModel.BuilderId).RepId;
                    var repname = _db.Users.Where(s => s.Id == repid).Select(p => new { p.FirstName, p.LastName, p.Email }).FirstOrDefault();
                    if (builder.BuilderStatusId == 3)
                    {
                        mailService.SendBuilderDeniedMail(builder.FirstName + " " + builder.LastName, builder.ReasonDenied, repname.FirstName + " " + repname.LastName, repname.Email, builder.Email);
                        mailService.SendDeniedMailRep(repname.FirstName, uname, uemail, builder.FirstName + " " + builder.LastName, builder.ReasonDenied, "manager", repname.Email);
                    }

                    else if (builder.BuilderStatusId == 2 && builder.Type == 2)
                    {
                        mailService.SendMailManagerFinance(builder.FirstName + " " + builder.LastName, uname, "finance");
                        mailService.SendMailRepSelectManager(builder.FirstName + " " + builder.LastName, repname.Email);
                    }

                    else if (builder.BuilderStatusId == 2 && builder.Type == 1)
                    {
                        mailService.SendMailBuilderRewards(builder.FirstName + " " + builder.LastName, builder.Email);
                        mailService.SendMailRepRewards(builder.FirstName + " " + builder.LastName, repname.Email);
                    }

                }

                else if (Request.Cookies["Role"].Value.ToString() == "4")
                {
                    builder.BuilderStatusId = builderDetailsViewModel.BuilderStatus == "2" ? 2 : 3;
                    builder.ModifiedBy = builder.BuilderStatusId == 2 ? 4 : 3;
                    builder.ReasonDenied = builder.BuilderStatusId == 2 ? "" : builderDetailsViewModel.ReasonDenied;
                    builder.ModifiedDate = DateTime.Now;
                    _db.SaveChanges();
                    note = "Builder Registration by Finance ";
                    redirect = "BuildersGrid";
                    var repid = _db.Users.Find(builderDetailsViewModel.BuilderId).RepId;
                    var repname = _db.Users.Where(s => s.Id == repid).Select(p => new { p.FirstName, p.LastName, p.Email }).FirstOrDefault();
                    if (builder.BuilderStatusId == 2)
                    {
                        redirect = "ContractTemplates";
                    }
                    else if (builder.BuilderStatusId == 3)
                    {
                        mailService.SendBuilderDeniedMail(builder.FirstName + " " + builder.LastName, builder.ReasonDenied, repname.FirstName + " " + repname.LastName, repname.Email, builder.Email);
                        mailService.SendDeniedMailRep(repname.FirstName, uname, uemail, builder.FirstName + " " + builder.LastName, builder.ReasonDenied, "finance", repname.Email);
                    }
                }



                obj.BuilderStatusId = builder.BuilderStatusId;
                obj.Note = note + builderDetailsViewModel.BuilderStatus == "2" ? "Approved" : "Denied";
                obj.CreatedDate = DateTime.Now;
                obj.ReasonDenied = builder.ReasonDenied;
                _db.BuilderStatusChanges.Add(obj);
                _db.SaveChanges();

                if (builderDetailsViewModel.Edit == 1)
                    return RedirectToAction("EditContracts");
                else if (Request.Cookies["Role"].Value.ToString() == "4")
                    return RedirectToAction(redirect, new { BuilderId = builder.Id });
                else
                    return RedirectToAction(redirect, new { roleid = Request.Cookies["Role"].Value.ToString() });

            }
            catch (Exception ex)
            {
                //ModelState.AddModelError(string.Empty, ex.Message);
            }
            return View(builderDetailsViewModel);
        }

        public ActionResult DeleteRoomBuilder(int BuilderId, string roomtext)
        {
            var roomid = _db.Rooms.Where(s => s.RoomName == roomtext).Select(p => p.Id).First();
            if (_db.UserRoomProductSKUs.Any(s => s.UserId == BuilderId && s.RoomId == roomid))
            {
                _db.UserRoomProductSKUs.RemoveRange(_db.UserRoomProductSKUs.Where((s => s.UserId == BuilderId && s.RoomId == roomid)));
                _db.SaveChanges();
            }
            return Json(1);
        }

        public ActionResult ContractsGrid()
        {
            var ModifiedBy = 0;
            var contractstatus = 0;
            ViewBag.Status = "Pending";

            if (Request.Cookies["Role"].Value.ToString() == "3")
            {
                ModifiedBy = 2;
                contractstatus = 2;
            }

            else if (Request.Cookies["Role"].Value.ToString() == "4")
            {
                ModifiedBy = 3;
                contractstatus = 2;
            }
            else
            {
                return RedirectToAction("UnsignedContracts");
            }
            if (_db.ContractDetails.Any(s => s.IsActive == true & s.ModifiedBy == ModifiedBy && s.ContractStatus == contractstatus))
            {
                var pendcontracts = _db.ContractDetails.Where(s => s.IsActive == true & s.ModifiedBy == ModifiedBy && s.ContractStatus == contractstatus).ToList().Select(p => new ContractDetailsViewModel
                {
                    ContractId = p.Id,
                    CompanyName = p.User.CompanyName,
                    City = p.User.City,
                    State = _db.States.Where(s => s.Id == p.User.State).Select(t => t.Name).FirstOrDefault(),
                    CreatedDate = p.CreatedDate,
                    BuilderId = p.BuilderId,
                    ContractStatus = p.ContractStatus.ToString(),
                });
                return View(pendcontracts);
            }
            return View();

        }

        public ActionResult ContractDetails(int ContractId, string Status)
        {
            if (_db.ContractDetails.Any(p => p.Id == ContractId))
            {
                var data = _db.ContractDetails.Where(p => p.Id == ContractId).ToList().Select(s => new ContractDetailsViewModel
                {
                    CompanyName = s.User.CompanyName,
                    State = _db.States.Where(a => a.Id == s.User.State).Select(t => t.Name).FirstOrDefault(),
                    City = s.User.City,
                    CreatedDate = s.CreatedDate,
                    DateString = s.CreatedDate.Value.ToString("MM/dd/yyyy"),
                    BuilderId = s.BuilderId,
                    ContractId = ContractId,
                    FirstName = s.User.FirstName,
                    LastName = s.User.LastName,
                    PhoneNumber = s.User.PhoneNumber,
                    AvgPriceUnit = s.User.AveragePriceUnit,
                    AnnualNoOfHomes = s.User.AnnualNumberOfHomes,
                    AnnualHomes = Convert.ToInt32((s.User.AnnualNumberOfHomes)),
                    PercentPlumbingUpgrades = s.User.PercentageOfPlumbingUpgrades,
                    WebsiteURL = s.User.WebsiteURL,
                    InstallationPlumb = s.User.InstallationPlumbers,
                    PlumbManufacturePartners = s.User.PlumbingManufacturingPartners,
                    PlumbWholesalePartners = s.User.PlumbingWholesalePartners,
                    ProductCategories = (from p in _db.UserProdCategories
                                         join q in _db.ProductCategories on p.ProductCategoryId equals q.ProductCategoryId
                                         where p.UserId == s.BuilderId
                                         select q.ProductName).ToList(),
                    //ContractStatusString = Enum.GetName(typeof(ContractStatus), s.ContractStatus != 2 ? s.ContractStatus : 1),
                    ContractStatus = s.ContractStatus.ToString(),
                    //ApproveDeniedString = Enum.GetName(typeof(ContractStatus), s.ContractStatus),
                    ReasonDenied = s.ReasonDenied,
                    AgreementString = s.FilePath
                }).FirstOrDefault();
                ViewBag.Status = Status;
                return View(data);
            }
            return View();
        }

        [HttpPost]
        public ActionResult ContractDetails(ContractDetailsViewModel obj)
        {
            try
            {
                var contract = _db.ContractDetails.Find(obj.ContractId);
                if (contract != null)
                {
                    if (obj.SignedAgreement != null)
                    {
                        HttpPostedFileBase file = obj.SignedAgreement;
                        var filePath = SaveFile(file, "~/Signed Contracts/", contract.Id.ToString());
                        contract.FilePath = filePath;
                        contract.UploadStatus = "Signed";
                    }

                    BuilderStatusChanx builderStatusChanx = new BuilderStatusChanx();
                    contract.ContractStatus = obj.ContractStatus == "2" ? 2 : 3; // (int)((BuilderStatus)Enum.Parse(typeof(BuilderStatus), obj.ContractStatusString));
                    contract.ModifiedDate = DateTime.Now;
                    contract.ReasonDenied = obj.ReasonDenied;
                    var note = "";
                    if (Request.Cookies["Role"].Value.ToString() == "2")
                    {
                        contract.ModifiedBy = contract.ContractStatus == 2 ? 2 : 1;
                        note = "Contract Status By Rep ";
                    }

                    else if (Request.Cookies["Role"].Value.ToString() == "3")
                    {
                        contract.ModifiedBy = contract.ContractStatus == 2 ? 3 : 2;
                        note = "Contract Status By Manager ";
                    }

                    else
                    {
                        contract.ModifiedBy = contract.ContractStatus == 2 ? 4 : 3;
                        note = "Contract Status By Finance ";
                        contract.ExpirationDate = DateTime.Now.AddYears(1);
                    }
                    _db.SaveChanges();

                    builderStatusChanx.BuilderStatusId = contract.ContractStatus;
                    builderStatusChanx.Note = note + obj.ContractStatus == "2" ? "Approved" : "Denied";
                    builderStatusChanx.CreatedDate = DateTime.Now;
                    builderStatusChanx.ReasonDenied = obj.ReasonDenied;
                    _db.BuilderStatusChanges.Add(builderStatusChanx);
                    _db.SaveChanges();
                    return RedirectToAction("ContractsGrid");
                }

            }
            catch (Exception ex)
            {

            }
            return View();
        }

        private string SaveFile(HttpPostedFileBase httpPostedFile, string location, string cNumber)
        {
            var fileName = cNumber + "_" + Path.GetFileNameWithoutExtension(httpPostedFile.FileName) + "_" +
                           DateTime.Now.Ticks.ToString();
            var fileExt = Path.GetExtension(httpPostedFile.FileName);
            var path = Server.MapPath(Path.Combine(location, fileName + fileExt));
            httpPostedFile.SaveAs(path);
            return fileName + fileExt;
        }

        public ActionResult DeniedContracts()
        {
            if (Request.Cookies["Role"].Value == "2")
            {
                var loginid = Convert.ToInt32(Request.Cookies["LoginId"].Value);
                if (_db.Users.Any(s => s.RepId == loginid))
                {
                    //var builderids = _db.RepBuilders.Where(s => s.RepId == loginid).Select(p => p.BuilderId).ToList();
                    var builderids = _db.Users.Where(s => s.RepId == loginid).Select(p => p.Id).ToList();
                    if (_db.ContractDetails.Any(s => builderids.Contains(s.BuilderId) && s.ContractStatus == 3))
                    {
                        var deniedlist = _db.ContractDetails.Where(s => builderids.Contains(s.BuilderId) && s.ContractStatus == 3).ToList().Select(p => new ContractDetailsViewModel
                        {
                            CompanyName = p.User.CompanyName,
                            City = p.User.City,
                            State = _db.States.Where(s => s.Id == p.User.State).Select(t => t.Name).FirstOrDefault(),
                            ModifiedDate = p.ModifiedDate,
                            ContractStatus = "Denied",
                            ReasonDenied = p.ReasonDenied,
                            ContractId = p.Id,
                            BuilderId = p.BuilderId
                        });
                        ViewBag.Status = "Denied";
                        return View("ContractsGrid", deniedlist);
                    }
                }
            }
            else
            {
                if (_db.ContractDetails.Any(s => s.ContractStatus == 3))
                {
                    var deniedlist = _db.ContractDetails.Where(s => s.ContractStatus == 3).ToList().Select(p => new ContractDetailsViewModel
                    {
                        CompanyName = p.User.CompanyName,
                        City = p.User.City,
                        State = _db.States.Where(s => s.Id == p.User.State).Select(t => t.Name).FirstOrDefault(),
                        ModifiedDate = p.ModifiedDate,
                        ContractStatus = "Denied",
                        ReasonDenied = p.ReasonDenied,
                        ContractId = p.Id,
                        BuilderId = p.BuilderId
                    });
                    ViewBag.Status = "Denied";
                    return View("ContractsGrid", deniedlist);
                }
            }

            return View("ContractsGrid");
        }

        public ActionResult ApprovedContracts()
        {
            if (Request.Cookies["Role"].Value == "2")
            {
                var loginid = Convert.ToInt32(Request.Cookies["LoginId"].Value);
                if (_db.Users.Any(s => s.RepId == loginid))
                {
                    var builderids = _db.Users.Where(s => s.RepId == loginid).Select(p => p.Id).ToList();
                    if (_db.ContractDetails.Any(s => builderids.Contains(s.BuilderId) && s.ContractStatus == 2 && s.ModifiedBy == 4))
                    {
                        var appprlist = _db.ContractDetails.Where(s => builderids.Contains(s.BuilderId) && s.ContractStatus == 2 && s.ModifiedBy == 4).ToList().Select(p => new ContractDetailsViewModel
                        {
                            CompanyName = p.User.CompanyName,
                            City = p.User.City,
                            State = _db.States.Where(s => s.Id == p.User.State).Select(t => t.Name).FirstOrDefault(),
                            ModifiedDate = p.ModifiedDate,
                            ContractId = p.Id,
                            BuilderId = p.BuilderId
                        });
                        ViewBag.Status = "Approved";
                        return View("ContractsGrid", appprlist);
                    }
                }
            }
            else
            {
                if (_db.ContractDetails.Any(s => s.ContractStatus == 2 && s.ModifiedBy == 4))
                {
                    var appprlist = _db.ContractDetails.Where(s => s.ContractStatus == 2 && s.ModifiedBy == 4).ToList().Select(p => new ContractDetailsViewModel
                    {
                        CompanyName = p.User.CompanyName,
                        City = p.User.City,
                        State = _db.States.Where(s => s.Id == p.User.State).Select(t => t.Name).FirstOrDefault(),
                        ModifiedDate = p.ModifiedDate,
                        ContractId = p.Id,
                        BuilderId = p.BuilderId
                    });
                    ViewBag.Status = "Approved";
                    return View("ContractsGrid", appprlist);
                }
            }


            return View("ContractsGrid");
        }

        public ActionResult UnsignedContracts()
        {
            if (_db.ContractDetails.Any(s => s.IsActive == true && s.ContractStatus == 1 && s.UploadStatus == "Unsigned"))
            {
                var list = _db.ContractDetails.Where(s => s.IsActive == true && s.ContractStatus == 1 && s.UploadStatus == "Unsigned").ToList().Select(p => new ContractDetailsViewModel
                {
                    CompanyName = p.User.CompanyName,
                    City = p.User.City,
                    State = _db.States.Where(s => s.Id == p.User.State).Select(t => t.Name).FirstOrDefault(),
                    CreatedDate = p.CreatedDate,
                    ContractId = p.Id,
                    BuilderId = p.BuilderId
                });
                ViewBag.Status = "Unsigned";
                return View("ContractsGrid", list);
            }
            return View("ContractsGrid");
        }

        public ActionResult ExpiringContracts(int period)
        {
            var currentdate = DateTime.Now;
            var futuredate = period == 90 ? DateTime.Now.AddMonths(3) : DateTime.Now.AddMonths(6);
            ViewBag.Status = "Expiry";
            if (Request.Cookies["Role"].Value == "2")
            {
                var loginid = Convert.ToInt32(Request.Cookies["LoginId"].Value);
                if (_db.Users.Any(s => s.RepId == loginid))
                {
                    var builderids = _db.Users.Where(s => s.RepId == loginid).Select(p => p.Id).ToList();
                    if (_db.ContractDetails.Any(s => builderids.Contains(s.BuilderId) && s.IsActive == true && s.ContractStatus == 2 && s.ExpirationDate >= currentdate && s.ExpirationDate <= futuredate))
                    {
                        var data = _db.ContractDetails.Where(s => builderids.Contains(s.BuilderId) && s.IsActive == true && s.ContractStatus == 2 && s.ExpirationDate >= currentdate && s.ExpirationDate <= futuredate).ToList().Select(s => new ContractDetailsViewModel
                        {
                            ContractId = s.Id,
                            BuilderId = s.BuilderId,
                            CompanyName = s.User.CompanyName,
                            City = s.User.City,
                            State = _db.States.Where(p => p.Id == s.User.State).Select(t => t.Name).FirstOrDefault(),
                            ModifiedDate = s.ModifiedDate,
                            ExpirationDate = s.ExpirationDate,

                        });
                        return View("ContractsGrid", data);
                    }
                }
            }
            else
            {
                if (_db.ContractDetails.Any(s => s.IsActive == true && s.ContractStatus == 2 && s.ExpirationDate >= currentdate && s.ExpirationDate <= futuredate))
                {
                    var data = _db.ContractDetails.Where(s => s.IsActive == true && s.ContractStatus == 2 && s.ExpirationDate >= currentdate && s.ExpirationDate <= futuredate).ToList().Select(s => new ContractDetailsViewModel
                    {
                        ContractId = s.Id,
                        BuilderId = s.BuilderId,
                        CompanyName = s.User.CompanyName,
                        City = s.User.City,
                        State = _db.States.Where(p => p.Id == s.User.State).Select(t => t.Name).FirstOrDefault(),
                        ModifiedDate = s.ModifiedDate,
                        ExpirationDate = s.ExpirationDate,

                    });
                    return View("ContractsGrid", data);
                }
            }

            return View("ContractsGrid");
        }

        public ActionResult AddRebateByRep()
        {
            List<object> list = new List<object>();
            List<StateViewModel> states = _db.States.Where(x => (bool)x.IsActive).Select(s => new StateViewModel { Id = s.Id, Name = s.Name }).ToList();
            ViewBag.States = new SelectList(states.OrderBy(m => m.Name).ToList(), "Id", "Name");
            var loginid = Convert.ToInt32(Request.Cookies["LoginId"].Value);
            if (_db.Users.Any(s => s.RepId == loginid))
            {
                var builderids = _db.Users.Where(s => s.RepId == loginid).Select(p => p.Id).ToList();
                //if (_db.ContractDetails.Any(s => builderids.Contains(s.BuilderId) && s.UploadStatus == "Signed" && s.ModifiedBy == 4 && s.ContractStatus == 2)  ||
                //    (_db.Users.Any(p => builderids.Contains(p.Id) && p.Type == 1 && p.ModifiedBy == 3 && p.BuilderStatusId == 2)))
                //{

                //    var rbuilders = _db.Users.Where(p => builderids.Contains(p.Id) && p.Type == 1 && p.ModifiedBy == 3 && p.BuilderStatusId == 2).Select(o => new { o.CompanyName, o.Id }).Distinct().ToList();
                //    var cb = _db.ContractDetails.Where(s => builderids.Contains(s.BuilderId) && s.UploadStatus == "Signed" && s.ModifiedBy == 4 && s.ContractStatus == 2).Select(o => new { o.BuilderId }).Distinct().ToList();
                //    foreach(var c in cb)
                //    {
                //        var cbuilders = _db.Users.Where(s => s.Id.Equals(c.BuilderId)).Select(a => new { a.Id, a.CompanyName }).First();
                //        list.Add(cbuilders);
                //    }

                //   // var abc = (((from c in cbuilders select new { c.BuilderId, c.CompanyName }).ToList()).Union(from r in rbuilders select new { r.Id, repcompanyname=r.CompanyName  })).ToList();
                //    //var builders = _db.Users.Where(t => builderids.Contains(t.Id)).Select(o => new { o.CompanyName, o.Id }).Distinct().ToList();
                //}

                //if (_db.Users.Any(p => builderids.Contains(p.Id) && p.Type == 1 && p.ModifiedBy == 3 && p.BuilderStatusId == 2))
                //{
                //    rbuilders = _db.Users.Where(p => builderids.Contains(p.Id) && p.Type == 1 && p.ModifiedBy == 3 && p.BuilderStatusId == 2).Select(o => new { o.CompanyName, o.Id }).Distinct().ToList();
                //}
                if (_db.Users.Any(t => builderids.Contains(t.Id)))
                {
                    var builders = _db.Users.Where(t => builderids.Contains(t.Id)).Select(o => new { o.CompanyName, o.Id }).Distinct().ToList();
                    ViewBag.builders = new SelectList(builders, "Id", "CompanyName");
                }

            }
            return View();
        }

        public JsonResult GetCitiesByStateId(int id)
        {
            var loginid = Convert.ToInt32(Request.Cookies["LoginId"].Value);
            if (_db.Users.Any(s => s.RepId == loginid))
            {
                var builderids = _db.Users.Where(s => s.RepId == loginid).Select(p => p.Id).ToList();
                if (_db.Users.Any(t => builderids.Contains(t.Id)))
                {
                    var cities = _db.Users.Where(t => builderids.Contains(t.Id) && t.State == id).Select(o => o.City).Distinct().ToList();
                    return Json(cities, JsonRequestBehavior.AllowGet);
                }
            }
            return Json(1);
        }

        public JsonResult GetBuildersFromCity(int stateid, string city)
        {
            var loginid = Convert.ToInt32(Request.Cookies["LoginId"].Value);
            if (_db.Users.Any(s => s.RepId == loginid))
            {
                var builderids = _db.Users.Where(s => s.RepId == loginid).Select(p => p.Id).ToList();
                if (_db.Users.Any(t => builderids.Contains(t.Id)))
                {
                    var buildername = _db.Users.Where(t => builderids.Contains(t.Id) && t.State == stateid && t.City == city).Select(o => new { o.CompanyName, o.Id }).Distinct().ToList();
                    return Json(
                new SelectList(buildername.Select(s => new BuilderDetailsViewModel { BuilderId = s.Id, CompanyName = s.CompanyName }), "BuilderId",
                    "CompanyName"), JsonRequestBehavior.AllowGet);
                }
            }
            return Json(1);
        }

        [HttpPost]
        public ActionResult AddRebateByRep(AddRebateViewModel obj)
        {
            List<StateViewModel> states = _db.States.Where(x => (bool)x.IsActive).Select(s => new StateViewModel { Id = s.Id, Name = s.Name }).ToList();
            ViewBag.States = new SelectList(states.OrderBy(m => m.Name).ToList(), "Id", "Name");
            var loginid = Convert.ToInt32(Request.Cookies["LoginId"].Value);
            if (_db.Users.Any(s => s.RepId == loginid))
            {
                var builderids = _db.Users.Where(s => s.RepId == loginid).Select(p => p.Id).ToList();
                if (_db.Users.Any(t => builderids.Contains(t.Id)))
                {
                    var builders = _db.Users.Where(t => builderids.Contains(t.Id)).Select(o => new { o.CompanyName, o.Id }).Distinct().ToList();
                    ViewBag.builders = new SelectList(builders, "Id", "CompanyName");
                }
            }
            if (ModelState.IsValid)
            {
                if (_db.ContractDetails.Any(s => s.BuilderId == obj.BuilderId && s.UploadStatus == "Signed" && s.ModifiedBy == 4 && s.ContractStatus == 2) || _db.Users.Any(p => p.Type == 1 && p.ModifiedBy == 3 && p.Id == obj.BuilderId && p.BuilderStatusId == 2))
                {
                    //redirect to rebate method
                    if (_db.Users.Where(s => s.Id == obj.BuilderId).Select(p => p.Type).First() == 1)
                        return RedirectToAction("RebateForRewards", "Builder");
                    else return RedirectToAction("RebateForSelect", "Builder");
                }
                else
                {
                    ModelState.AddModelError("BuilderId", "Builder is not approved yet");
                    return View("AddRebateByRep");
                }
            }
            else return View();
        }

        public ActionResult EditContracts()
        {
            RebateList obj = new RebateList();
            var loginid = Convert.ToInt32(Request.Cookies["LoginId"].Value);
            if (_db.Users.Any(s => s.RepId == loginid))
            {
                var builderids = _db.Users.Where(s => s.RepId == loginid).Select(p => p.Id).ToList();
                if (_db.ContractDetails.Any(s => builderids.Contains(s.BuilderId) && s.UploadStatus == "Signed" && s.ModifiedBy == 4 && s.ContractStatus == 2))
                {
                    var appprlist = _db.ContractDetails.Where(s => builderids.Contains(s.BuilderId) && s.UploadStatus == "Signed" && s.ModifiedBy == 4 && s.ContractStatus == 2).ToList().Select(p => new ContractDetailsViewModel
                    {
                        CompanyName = p.User.CompanyName,
                        City = p.User.City,
                        State = _db.States.Where(s => s.Id == p.User.State).Select(t => t.Name).FirstOrDefault(),
                        ModifiedDate = p.ModifiedDate,
                        ContractId = p.Id,
                        BuilderId = p.BuilderId,

                    });
                    obj.contractslist = appprlist.ToList();
                }

                if (_db.Users.Any(p => builderids.Contains(p.Id) && p.Type == 1 && p.ModifiedBy == 3 && p.BuilderStatusId == 2))
                {
                    var rewardslist = _db.Users.Where(p => builderids.Contains(p.Id) && p.Type == 1 && p.ModifiedBy == 3 && p.BuilderStatusId == 2).ToList().Select(p => new BuilderDetailsViewModel
                    {
                        CompanyName = p.CompanyName,
                        City = p.City,
                        State = _db.States.Where(s => s.Id == p.State).Select(t => t.Name).FirstOrDefault(),
                        ModifiedDate = p.ModifiedDate,
                        BuilderId = p.Id,
                        RebateType = "Rewards"
                    });
                    obj.builderslist = rewardslist.ToList();
                }

            }
            ViewBag.Status = "Edit";
            return View(obj);

        }

        public ActionResult ContractTemplates(int BuilderId)
        {
            BuilderDetailsViewModel obj = new BuilderDetailsViewModel();
            if (BuilderId > 0)
            {
                var builder = _db.Users.Find(BuilderId);
                obj.CompanyName = builder.CompanyName;
                obj.ModelHomeCoOp = builder.ModelHomeCoOp == "Yes" ? "Yes" : "No";
                obj.Value = builder.ModelHomeCoOp == "Yes" ? _db.ModelCoOpAmounts.Where(s => s.Id == builder.Value).Select(p => p.Amount).First() : "--";
                obj.TermAgreement = builder.TermAgreement == "Other" ? builder.ContactLengthYears : builder.TermAgreement;
                obj.FirstName = builder.FirstName;
                obj.LastName = builder.LastName;
                obj.PhoneNumber = builder.PhoneNumber;
                obj.Address = builder.CompanyAddress1;
                obj.City = builder.City;
                obj.State = _db.States.Where(p => p.Id == builder.State).Select(t => t.Name).FirstOrDefault();
                obj.Zip = builder.Zip;
                obj.Email = builder.Email;
                obj.BuilderId = builder.Id;
                obj.PerHouseRebate = builder.PerHouseRebate;
                obj.MasterBathroom = (from p in _db.UserRoomProductSKUs
                                      join q in _db.Products on p.ProductSkuId equals q.Id
                                      join r in _db.Manufacturers on q.Manufacturer equals r.id
                                      where p.UserId == BuilderId && p.RoomId == 1
                                      select q.SKU + "_" + r.mfg_name).ToList();
                obj.sBathroom1 = (from p in _db.UserRoomProductSKUs
                                  join q in _db.Products on p.ProductSkuId equals q.Id
                                  join r in _db.Manufacturers on q.Manufacturer equals r.id
                                  where p.UserId == BuilderId && p.RoomId == 2
                                  select q.SKU + "_" + r.mfg_name).ToList();
                obj.sBathroom2 = (from p in _db.UserRoomProductSKUs
                                  join q in _db.Products on p.ProductSkuId equals q.Id
                                  join r in _db.Manufacturers on q.Manufacturer equals r.id
                                  where p.UserId == BuilderId && p.RoomId == 3
                                  select q.SKU + "_" + r.mfg_name).ToList();
                obj.sBathroom3 = (from p in _db.UserRoomProductSKUs
                                  join q in _db.Products on p.ProductSkuId equals q.Id
                                  join r in _db.Manufacturers on q.Manufacturer equals r.id
                                  where p.UserId == BuilderId && p.RoomId == 4
                                  select q.SKU + "_" + r.mfg_name).ToList();
                obj.sKitchen1 = (from p in _db.UserRoomProductSKUs
                                 join q in _db.Products on p.ProductSkuId equals q.Id
                                 join r in _db.Manufacturers on q.Manufacturer equals r.id
                                 where p.UserId == BuilderId && p.RoomId == 5
                                 select q.SKU + "_" + r.mfg_name).ToList();
                obj.sKitchen2 = (from p in _db.UserRoomProductSKUs
                                 join q in _db.Products on p.ProductSkuId equals q.Id
                                 join r in _db.Manufacturers on q.Manufacturer equals r.id
                                 where p.UserId == BuilderId && p.RoomId == 6
                                 select q.SKU + "_" + r.mfg_name).ToList();
                obj.sLaundryroom = (from p in _db.UserRoomProductSKUs
                                    join q in _db.Products on p.ProductSkuId equals q.Id
                                    join r in _db.Manufacturers on q.Manufacturer equals r.id
                                    where p.UserId == BuilderId && p.RoomId == 7
                                    select q.SKU + "_" + r.mfg_name).ToList();
                obj.sOther = (from p in _db.UserRoomProductSKUs
                              join q in _db.Products on p.ProductSkuId equals q.Id
                              join r in _db.Manufacturers on q.Manufacturer equals r.id
                              where p.UserId == BuilderId && p.RoomId == 8
                              select q.SKU + "_" + r.mfg_name).ToList();
            }
            return View(obj);
        }

        //[HttpPost]
        //public ActionResult ContractTemplates(BuilderDetailsViewModel contractTemplateViewModel)
        //{
        //    var buildername = _db.Users.Where(s => s.Id == contractTemplateViewModel.BuilderId).Select(p => new { p.FirstName, p.LastName }).FirstOrDefault();
        //    var repid = _db.RepBuilders.Where(s => s.BuilderId == contractTemplateViewModel.BuilderId).Select(p => p.RepId).FirstOrDefault();
        //    var repname = _db.Users.Where(s => s.Id == repid).Select(p => new { p.FirstName, p.LastName }).FirstOrDefault();

        //    var document = new Document(PageSize.A4, 50, 50, 25, 25);
        //    var output = new MemoryStream();
        //    var writer = PdfWriter.GetInstance(document, output);
        //    document.Open();
        //    var titleFont = FontFactory.GetFont("Arial", 18, Font.BOLD);
        //    var subTitleFont = FontFactory.GetFont("Arial", 14, Font.BOLD);
        //    var boldTableFont = FontFactory.GetFont("Arial", 12, Font.BOLD);
        //    var endingMessageFont = FontFactory.GetFont("Arial", 10, Font.ITALIC);
        //    var bodyFont = FontFactory.GetFont("Arial", 12, Font.NORMAL);
        //    document.Add(new Paragraph("Lixil Contract", titleFont));
        //    document.Add(new Paragraph("Congratulations and welcome to the Lixil Builder Rebate Program. Below are the contract details.", bodyFont));
        //    document.Add(Chunk.NEWLINE);

        //    document.Add(new Paragraph("Builder Information", subTitleFont));
        //    var bulderinfo = new PdfPTable(5);
        //    bulderinfo.HorizontalAlignment = 0;
        //    bulderinfo.SpacingBefore = 10;
        //    bulderinfo.SpacingAfter = 10;
        //    bulderinfo.DefaultCell.Border = 0;
        //    //bulderinfo.SetWidths(new int[] { 1, 1, 1, 1, 1 });

        //    bulderinfo.AddCell(new Phrase("Builder Name:", boldTableFont));
        //    bulderinfo.AddCell(contractTemplateViewModel.FirstName+ " "+contractTemplateViewModel.LastName);
        //    bulderinfo.AddCell(new Phrase("Company:", boldTableFont));
        //    bulderinfo.AddCell(contractTemplateViewModel.CompanyName);
        //    bulderinfo.AddCell(new Phrase("Phone Number:", boldTableFont));
        //    bulderinfo.AddCell(contractTemplateViewModel.PhoneNumber);
        //    bulderinfo.AddCell(new Phrase("City:", boldTableFont));
        //    bulderinfo.AddCell(contractTemplateViewModel.City);
        //    bulderinfo.AddCell(new Phrase("Registration Date:", boldTableFont));
        //    bulderinfo.AddCell(contractTemplateViewModel.DateString);

        //    document.Add(bulderinfo);

        //    document.Add(new Paragraph("Contract Information", subTitleFont));
        //    var contractinfo = new PdfPTable(3);
        //    contractinfo.HorizontalAlignment = 0;
        //    contractinfo.SpacingBefore = 10;
        //    contractinfo.SpacingAfter = 35;
        //    contractinfo.DefaultCell.Border = 0;

        //    contractinfo.AddCell(new Phrase("Template Column1", boldTableFont));
        //    contractinfo.AddCell(new Phrase("Template Column2", boldTableFont));
        //    contractinfo.AddCell(new Phrase("Template Column3", boldTableFont));
        //    contractinfo.AddCell(contractTemplateViewModel.TemplateColumn1);
        //    contractinfo.AddCell(contractTemplateViewModel.TemplateColumn2);
        //    contractinfo.AddCell(contractTemplateViewModel.TemplateColumn3);
        //    document.Add(contractinfo);

        //    var endingMessage = new Paragraph("Thank you for your business! If you have any questions about your contract, please contact us at 800-555-5454.", endingMessageFont);
        //    endingMessage.Alignment = Element.ALIGN_CENTER;
        //    document.Add(endingMessage);

        //    var logo = Image.GetInstance(Server.MapPath("~/Images/lixil_logo.jpg"));
        //    logo.SetAbsolutePosition(440, 800);
        //    document.Add(logo);
        //    writer.CloseStream = false;
        //    document.Close();
        //    byte[] bytes = output.ToArray();
        //    System.IO.File.WriteAllBytes(Server.MapPath("~/PdfContracts/Contract-"+contractTemplateViewModel.BuilderId+".pdf"), bytes);

        //    Response.ContentType = "application/pdf";
        //    var path = string.Format("attachment;filename=Contract-{0}.pdf", contractTemplateViewModel.BuilderId);
        //    var filename = string.Format("Contract-{0}.pdf", contractTemplateViewModel.BuilderId);
        //    Response.AddHeader("Content-Disposition", path);
        //    Response.BinaryWrite(output.ToArray());
        //    output.Position = 0;
        //    SendMailBuilderSelect(buildername.FirstName + " " + buildername.LastName, output, filename);
        //    SendMailRepSelectFinance(buildername.FirstName + " " + buildername.LastName);
        //    SendMailManagerSelectFinance(buildername.FirstName + " " + buildername.LastName, repname.FirstName);

        //    ContractDetail contract = new ContractDetail();
        //    contract.BuilderId = contractTemplateViewModel.BuilderId;
        //    contract.CreatedDate = DateTime.Now;
        //    contract.ModifiedDate = DateTime.Now;
        //    contract.ModifiedBy = 1;
        //    contract.IsActive = true;
        //    contract.ContractStatus = 1;
        //    contract.UploadStatus = "Unsigned";
        //    _db.ContractDetails.Add(contract);
        //    _db.SaveChanges();
        //    return RedirectToAction("BuildersGrid");
        //}

        public ActionResult DownloadPDF(int builderId)
        {
            if (builderId > 0)
            {
                ContractDetail contract = new ContractDetail();
                contract.BuilderId = builderId;
                contract.CreatedDate = DateTime.Now;
                contract.ModifiedDate = DateTime.Now;
                contract.ModifiedBy = 1; //1
                contract.IsActive = true;
                contract.ContractStatus = 1; //1
                contract.UploadStatus = "Unsigned"; //Unsigned
                _db.ContractDetails.Add(contract);
                _db.SaveChanges();

                WebClient wc = new WebClient();
                string urL = ConfigurationManager.AppSettings["URL"].ToString();
                urL += "/Users/ContractTemplates?BuilderId=" + builderId + "";
                string htmlText = wc.DownloadString(urL);
                HtmlDocument doc = new HtmlDocument();
                doc.LoadHtml(htmlText);
                string agreement = doc.GetElementbyId("dvAgrmnt").InnerHtml;
                Response.Clear();
                Response.ContentType = "application/pdf";
                Response.AddHeader("content-disposition", "attachment;filename=" + "Contract-" + builderId + ".pdf");
                Response.Cache.SetCacheability(HttpCacheability.NoCache);
                Response.BinaryWrite(GetPDF(agreement, builderId));
                Response.End();

            }
            
            return RedirectToAction("BuildersGrid", new { roleid = Request.Cookies["Role"].Value.ToString() });
        }

        public byte[] GetPDF(string pHTML, int builderId)
        {
            var buildername = _db.Users.Where(s => s.Id == builderId).Select(p => new { p.FirstName, p.LastName }).FirstOrDefault();
            var repid = _db.Users.Find(builderId).RepId;
            var repname = _db.Users.Where(s => s.Id == repid).Select(p => new { p.FirstName, p.LastName, p.Email }).FirstOrDefault();
            byte[] bPDF = null;

            MemoryStream ms = new MemoryStream();
            TextReader txtReader = new StringReader(pHTML);

            // 1: create object of a itextsharp document class  
            Document doc = new Document(PageSize.A4, 25, 25, 25, 25);

            // 2: we create a itextsharp pdfwriter that listens to the document and directs a XML-stream to a file  
            PdfWriter oPdfWriter = PdfWriter.GetInstance(doc, ms);

            // 3: we create a worker parse the document  
            HTMLWorker htmlWorker = new HTMLWorker(doc);

            // 4: we open document and start the worker on the document  
            doc.Open();
            htmlWorker.StartDocument();


            // 5: parse the html into the document  
            htmlWorker.Parse(txtReader);

            // 6: close the document and the worker  
            htmlWorker.EndDocument();
            htmlWorker.Close();

            var filename = string.Format("Contract-{0}.pdf", builderId);
            var logo = Image.GetInstance(Server.MapPath("~/Images/lixil_logo.jpg"));
            logo.SetAbsolutePosition(440, 800);
            doc.Add(logo);
            oPdfWriter.CloseStream = false;
            byte[] bytes = ms.ToArray();
            System.IO.File.WriteAllBytes(Server.MapPath("~/PdfContracts/Contract-" + builderId + ".pdf"), bytes);

            doc.Close();


            bPDF = ms.ToArray();
            ms.Position = 0;
            var builder = _db.Users.Where(s => s.Id == builderId).First();
            mailService.SendMailBuilderSelect(builder.FirstName + " " + builder.LastName, ms, filename, builder.Email);
            mailService.SendMailRepSelectFinance(builder.FirstName + " " + builder.LastName, repname.Email);
            mailService.SendMailManagerSelectFinance(builder.FirstName + " " + builder.LastName, repname.FirstName);

            return bPDF;
        }

        public ActionResult ShowPdf(string fileName)
        {
            string filePath = "~/Signed Contracts/" + fileName;
            return File(filePath, "application/pdf");
        }

        public void DelayWeekMails()
        {
            DateTime dbeg = DateTime.Today.AddDays(-7);
            DateTime dend = dbeg.AddDays(1).AddSeconds(-1);
            var repdelay = _db.Users.Where(s => s.CreatedDate >= dbeg && s.CreatedDate <= dend && s.RoleId == 5 && s.ModifiedBy == 1).ToList();
            foreach (var builder in repdelay)
            {
                if (_db.RepBuilders.Any(s => s.BuilderId == builder.Id))
                {
                    var repid = _db.RepBuilders.Where(s => s.BuilderId == builder.Id && s.IsActive == true).Select(p => p.RepId).First();
                    var repname = _db.Users.Find(repid).FirstName;
                    var repemail = _db.Users.Find(repid).Email;
                    mailService.SendMailtoRepDelay(builder.FirstName + " " + builder.LastName, repname, repemail);
                }
            }
            var mandelay = _db.Users.Where(s => s.ModifiedDate >= dbeg && s.ModifiedDate <= dend && s.ModifiedBy == 2 && s.BuilderStatusId == 2).ToList();
            foreach (var builder in mandelay)
            {
                if (_db.RepBuilders.Any(s => s.BuilderId == builder.Id))
                {
                    var repid = _db.RepBuilders.Where(s => s.BuilderId == builder.Id && s.IsActive == true).Select(p => p.RepId).First();
                    var repname = _db.Users.Find(repid).FirstName;
                    mailService.SendMailtoManagerDelay(builder.FirstName + " " + builder.LastName, repname, "testmanagerlixil@mailinator.com");
                    mailService.SendMailtoManagerDelay(builder.FirstName + " " + builder.LastName, repname, "testmanagerlixil@mailinator.com");
                }
            }
            var findelay = _db.Users.Where(s => s.ModifiedDate >= dbeg && s.ModifiedDate <= dend && s.ModifiedBy == 3 && s.BuilderStatusId == 2 && s.Type == 2).ToList();
            var cbuilderdelay = _db.ContractDetails.Where(s => s.CreatedDate >= dbeg && s.CreatedDate <= dend && s.ContractStatus == 1 && s.ModifiedBy == 1 && s.UploadStatus == "Unsigned").ToList();
            foreach (var contract in cbuilderdelay)
            {
                var builderid = _db.ContractDetails.Where(s => s.BuilderId == contract.BuilderId && s.IsActive == true).Select(p => p.BuilderId).FirstOrDefault();
                var builder = _db.Users.Where(s => s.Id == builderid).Select(p => new { p.FirstName, p.LastName, p.Email }).First();
                mailService.SendMailtoBuilderSignedDelay(builder.FirstName + " " + builder.LastName, builder.Email);
            }


        }


    }

}
