﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.HtmlControls;
using AutoMapper;
using LixilRebate.App_Start;
using LixilRebate.Models;
using LixilRebate.ViewModels;
using LixilRebate.Constants;
using System.Collections;
using System.Text;
using System.Security.Cryptography;

namespace LixilRebate.Controllers
{
    public class BuilderController : Controller
    {
        private readonly DBContext _db = new DBContext();
        int _builderId = 0;
        // GET: Builder
        public ActionResult Index()
        {
            if (Request.Cookies["UserId"] != null)
            {
                _builderId = Convert.ToInt32(Request.Cookies["UserId"].Value);
                StatusProgressViewModel progressDetails = GetBuilder(_builderId);
                return View(progressDetails);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        public StatusProgressViewModel GetBuilder(int builderId)
        {
            //var config = new AutoMapperConfig().Configure();
            //IMapper iMapper = config.CreateMapper();

            var user = new User();
            user = _db.Users.Where(a => a.Id == builderId && a.RoleId == 5).FirstOrDefault();
            var contractDetails = new ContractDetail();

            if (_db.ContractDetails.Any(x => x.BuilderId == builderId))
            {
                contractDetails = _db.ContractDetails.Where(x => x.BuilderId == builderId).First();
            }
            var builder = new StatusProgressViewModel()
            {
                BuilderStatusId = (int)user.BuilderStatusId,
                RegistrationSubmitted = (DateTime)user.CreatedDate,
                RegistrationModified = user.ModifiedDate,
                ContractSubmitted = contractDetails.CreatedDate,
                ContractModified = contractDetails.ModifiedDate,
                ContractModifiedBy = contractDetails.ModifiedBy,
                RegistrationModifiedBy = user.ModifiedBy,
                Type = user.Type,
                IsContractDateExpired = false
            };
            if (contractDetails.ExpirationDate != null)
            {
                if (Convert.ToDateTime(DateTime.Now.ToString("MM/dd/yyyy")) > Convert.ToDateTime(contractDetails.ExpirationDate.Value.ToString("MM/dd/yyyy")))
                {
                    builder.IsContractDateExpired = true;
                }
            }
            if (contractDetails.ContractStatus != null)
            {
                builder.ContractStatusId = contractDetails.ContractStatus;
            }
            else
            {
                builder.ContractStatusId = 1;
            }
            return builder;
        }

        [HttpGet]
        public ActionResult RebateForRewards(string invoiceDate = null, string invoiceNumber = null, bool secondRequest = false)
        {
            var products = _db.Products.ToList();



            List<ProductViewModel> productViews = new List<ProductViewModel>();

            foreach (var product in products)
            {
                ProductViewModel productViewModel = new ProductViewModel();
                productViewModel.SKU = product.SKU;
                string mfgName = _db.Manufacturers.Where(x => x.id == product.Manufacturer).Select(a => a.mfg_name).FirstOrDefault();
                productViewModel.SKU = product.SKU + " - " + mfgName;
                productViewModel.Id = product.Id;
                productViews.Add(productViewModel);
            }

            ViewBag.Products = new SelectList(productViews, "Id", "SKU");

            //var manufacturers = _db.Manufacturers.ToList();
            //ViewBag.Manufacturers = new SelectList(manufacturers,"id","mfg_name");

            RebateRewardsViewModel rewardsViewModel = new RebateRewardsViewModel();

            if (invoiceDate != null && invoiceNumber != null)
            {
                rewardsViewModel.DistributorInvoiceDate = invoiceDate;
                rewardsViewModel.DistributorInvoiceNumber = invoiceNumber;
               // rewardsViewModel.secondRequest = secondRequest;
                
            }

            rewardsViewModel.secondRequest = secondRequest;
            return View(rewardsViewModel);
        }

        //[HttpPost]
        //public JsonResult GetProducts(int prefix)
        //{
        //    var products = _db.Products.ToList();
        //    var similarProducts = (from product in products
        //                           where product.ProductNumber.StartsWith(prefix.ToString())
        //                           select new { product.ProductNumber });
        //    return Json(similarProducts, JsonRequestBehavior.AllowGet);
        //}

        [HttpPost]
        public ActionResult RebateForRewards(RebateRewardsViewModel rebateRewards)
        {
            try
            {
                var user = new User();
                _builderId = Convert.ToInt32(Request.Cookies["UserId"].Value);
                user = _db.Users.Where(a => a.Id == _builderId && a.RoleId == 5).FirstOrDefault();


                if (user != null)
                {
                    RebateDetail rebateDetails = new RebateDetail();
                    var RNumber = "";
                    //if (rebateRewards.secondRequest == true)
                    //{
                    //    rebateRewards.RebateId = _db.RebateDetails.Max(x => x.Id);
                    //}
                    if (rebateRewards.secondRequest == false)
                    {
                        RNumber = GetUniqueKey(12);
                        //bool isRebateExist = _db.RebateDetails.Any(x => x.BuilderId == user.Id && x.Type == user.Type);


                        //if (isRebateExist)
                        //{
                        //    rebateDetails.Id = _db.RebateDetails.Where(x => x.BuilderId == user.Id && x.Type == user.Type).Select(x => x.Id).First();
                        //}
                        //else
                        //{
                        rebateDetails.BuilderId = user.Id;
                        rebateDetails.Type = (int)user.Type;
                        rebateDetails.CreatedDate = DateTime.Now;
                        rebateDetails.ModifiedDate = DateTime.Now;
                        rebateDetails.IsActive = true;
                        _db.RebateDetails.Add(rebateDetails);
                        _db.SaveChanges();
                        //}

                        RebateReward rebateReward = new RebateReward()
                        {
                            BuilderId = user.Id,
                            RebateId = rebateDetails.Id,
                            DistributorInvoiceDate = rebateRewards.DistributorInvoiceDate,
                            DistributorInvoiceNumber = rebateRewards.DistributorInvoiceNumber,
                            RecordNumber = RNumber,
                            Status = 2,
                            CreatedDate = DateTime.Now,
                            ModifiedDate = DateTime.Now,
                            IsActive = false
                        };

                        _db.RebateRewards.Add(rebateReward);
                        _db.SaveChanges();

                        _db.Entry(rebateReward).State = System.Data.Entity.EntityState.Detached;
                    }

                    rebateRewards.RebateId = _db.RebateDetails.Max(x => x.Id);

                    //string text = rebateRewards.ProductNumber;
                    //string[] parts = text.Split('-');
                    //string productNumber = parts[0];
                    //string Mfg = parts[1];

                    int productId = Convert.ToInt32(rebateRewards.ProductNumber);
                    var product = _db.Products.Where(x => x.Id == productId).First();

                    //int mfg_name = Convert.ToInt32(rebateRewards.MfgName);
                    InvoiceProduct invoiceProduct = new InvoiceProduct()
                    {
                        InvoiceNumber = rebateRewards.DistributorInvoiceNumber,
                        BuilderId = _builderId,
                        ProductNumber = product.SKU,
                        Quantity = rebateRewards.Quantity,
                        Manufacturer = product.Manufacturer,
                        CreatedDate = DateTime.Now,
                        ModifiedDate = DateTime.Now,
                        IsActive = true
                    };


                    //var product = _db.Products.Where(x => x.SKU == rebateRewards.ProductNumber && x.Manufacturer == mfg_name).First();
                    var manfacturer = product.Manufacturer;
                    var price = product.ListPrice;
                    var percentage = _db.Manufacturers.Where(x => x.id == manfacturer).Select(a => a.Percentage).First();
                    var quantity = rebateRewards.Quantity;



                    invoiceProduct.DollarEstimated = ((price * quantity) * (percentage / 100)).ToString();
                    //invoiceProduct.DollarEstimated = "$" + invoiceProduct.DollarEstimated;

                    _db.InvoiceProducts.Add(invoiceProduct);
                    _db.SaveChanges();

                    var dollarEstimated = 0.0;
                    var products = _db.InvoiceProducts.Where(x => x.InvoiceNumber == rebateRewards.DistributorInvoiceNumber && x.BuilderId == _builderId).ToList();

                    if (products.Count > 0)
                    {
                        foreach (var product1 in products)
                        {
                            dollarEstimated = dollarEstimated + Convert.ToDouble(product1.DollarEstimated);
                        }
                        rebateRewards.DollarEstimated = dollarEstimated.ToString();

                        RebateReward reward = new RebateReward();
                        reward.Id = _db.RebateRewards.Where(x => x.RebateId == rebateRewards.RebateId).Select(a => a.Id).FirstOrDefault();
                        reward.DollarEstimated = "$" + rebateRewards.DollarEstimated;
                        _db.RebateRewards.Attach(reward);
                        _db.Entry(reward).Property(x => x.DollarEstimated).IsModified = true;
                        _db.Configuration.ValidateOnSaveEnabled = false;
                        _db.SaveChanges();

                    }


                    //string path = Server.MapPath("~/Invoice Files/");
                    //if (!Directory.Exists(path))
                    //{
                    //    Directory.CreateDirectory(path);
                    //}
                    if (rebateRewards.InvoiceFile != null)
                    {
                        foreach (HttpPostedFileBase file in rebateRewards.InvoiceFile)
                        {
                            if (file != null)
                            {
                                //string fileName = Path.GetFileName(file.FileName);
                                //string filePath = path + fileName;

                                var filePath = SaveFile(file, "~/Invoice Files/", RNumber);

                                RebateInvoice rebateInvoice = new RebateInvoice()
                                {
                                    RebateId = (int)rebateRewards.RebateId,
                                    InvoicePath = filePath,
                                    InvoiceStatus = 1,
                                    InvoiceType = (int)user.Type,
                                    CreatedDate = DateTime.Now,
                                    ModifiedDate = DateTime.Now,
                                    IsActive = true
                                };
                                _db.RebateInvoices.Add(rebateInvoice);
                                _db.SaveChanges();
                            }

                        }


                    }

                }

            }

            catch (Exception e)
            {

            }


            if (rebateRewards.IsAnotherProduct.ToLower() != "yes")
            {
                return RedirectToAction("RebateRedemption");
            }
            else
            {
                return RedirectToAction("RebateForRewards", new
                {
                    invoiceDate = rebateRewards.DistributorInvoiceDate
                                                                 ,
                    invoiceNumber = rebateRewards.DistributorInvoiceNumber,
                    secondRequest = true
                });
            }

        }

        [HttpGet]
        public ActionResult RebateForSelect()
        {
            return View();
        }

        [HttpPost]
        public ActionResult RebateForSelect(RebateSelectViewModel rebateSelectViewModel)
        {
            var user = new User();
            _builderId = Convert.ToInt32(Request.Cookies["UserId"].Value);
            user = _db.Users.Where(a => a.Id == _builderId && a.RoleId == 5).FirstOrDefault();

            if (user != null)
            {
                //bool isRebateExist = _db.RebateDetails.Any(x => x.BuilderId == user.Id && x.Type == user.Type);
                RebateDetail rebateDetails = new RebateDetail();

                //if (isRebateExist)
                //{
                //    rebateDetails.Id = _db.RebateDetails.Where(x => x.BuilderId == user.Id && x.Type == user.Type).Select(x => x.Id).First();
                //}
                //else
                //{
                    rebateDetails.BuilderId = user.Id;
                    rebateDetails.Type = (int)user.Type;
                    rebateDetails.CreatedDate = DateTime.Now;
                    rebateDetails.ModifiedDate = DateTime.Now;
                    rebateDetails.IsActive = true;
                    _db.RebateDetails.Add(rebateDetails);
                //}
                var RNumber = GetUniqueKey(12);
                RebateSelect rebateSelect = new RebateSelect()
                {
                    BuilderId = user.Id,
                    RebateId = rebateDetails.Id,
                    SubDivisionName = rebateSelectViewModel.SubDivisionName,
                    Lot_ = rebateSelectViewModel.Lot_,
                    PerHomeRebate = rebateSelectViewModel.PerHomeRebate,
                    ClosingStatementDate = rebateSelectViewModel.ClosingStatementDate,
                    RecordNumber = RNumber,
                    CreatedDate = DateTime.Now,
                    ModifiedDate = DateTime.Now,
                    IsActive = false
                };

                rebateSelect.Lot_ = rebateSelectViewModel.Lot_;
                rebateSelect.PerHomeRebate = rebateSelectViewModel.PerHomeRebate;
                int lot = Convert.ToInt32(rebateSelectViewModel.Lot_);
                int perHomeRebate = Convert.ToInt32(rebateSelectViewModel.PerHomeRebate);
                rebateSelect.DollarEstimated = lot * perHomeRebate;


                _db.RebateSelects.Add(rebateSelect);

                //string path = Server.MapPath("~/Invoice Files/");
                //if (!Directory.Exists(path))
                //{
                //    Directory.CreateDirectory(path);
                //}
                if (rebateSelectViewModel.InvoiceFile != null)
                {
                    foreach (HttpPostedFileBase file in rebateSelectViewModel.InvoiceFile)
                    {
                        if (file != null)
                        {
                            //string fileName = Path.GetFileName(file.FileName);
                            //string filePath = path + fileName;
                            var filePath = SaveFile(file, "~/Invoice Files/", RNumber);

                            RebateInvoice rebateInvoice = new RebateInvoice()
                            {
                                RebateId = rebateDetails.Id,
                                InvoicePath = filePath,
                                InvoiceStatus = 1,
                                InvoiceType = (int)user.Type,
                                CreatedDate = DateTime.Now,
                                ModifiedDate = DateTime.Now,
                                IsActive = true
                            };
                            _db.RebateInvoices.Add(rebateInvoice);
                        }

                    }

                }

                _db.SaveChanges();
            }

            return RedirectToAction("RebateRedemption");
        }

        [HttpGet]
        public ActionResult RebateRedemption()
        {
            var states = _db.States.ToList();
            ViewBag.States = new SelectList(states, "Id", "Name");
            return View();
        }

        [HttpPost]
        public ActionResult RebateRedemption(RebateRedemptionViewModel redemptionViewModel)
        {

            var user = new User();
            _builderId = Convert.ToInt32(Request.Cookies["UserId"].Value);
            user = _db.Users.Where(a => a.Id == _builderId && a.RoleId == 5).FirstOrDefault();
            if (user != null)
            {
                bool isRebateExist = _db.RebateDetails.Any(x => x.BuilderId == user.Id && x.Type == user.Type);

                if (isRebateExist)
                {

                    int rebateId = _db.RebateDetails.Where(x => x.BuilderId == user.Id && x.Type == user.Type).OrderByDescending(a => a.Id).Select(x => x.Id).First();

                    RebateRedemption rebateRedemption = new RebateRedemption()
                    {
                        BuilderId = user.Id,
                        RebateId = rebateId,
                        Name = redemptionViewModel.Name,
                        Address1 = redemptionViewModel.Address1,
                        Address2 = redemptionViewModel.Address2,
                        City = redemptionViewModel.City,
                        State = redemptionViewModel.State,
                        Zip = redemptionViewModel.Zip,
                        //ReceiveRebateOption = redemptionViewModel.ReceiveRebateOption,
                        IsAuthorized = redemptionViewModel.IsAutorized,
                        CreatedDate = DateTime.Now,
                        ModifiedDate = DateTime.Now,
                        IsActive = true
                    };
                    _db.RebateRedemptions.Add(rebateRedemption);
                    _db.SaveChanges();

                    if (user.Type == 1)
                    {
                        var rebateReward = _db.RebateRewards.Where(x => x.RebateId == rebateId).FirstOrDefault();
                        _db.Entry(rebateReward).State = System.Data.Entity.EntityState.Detached;

                        RebateReward rebate = new RebateReward()
                        {
                            Id = rebateReward.Id,
                            IsActive = true
                        };
                        _db.RebateRewards.Attach(rebate);
                        _db.Entry(rebate).Property(x => x.IsActive).IsModified = true;
                        _db.Configuration.ValidateOnSaveEnabled = false;
                        _db.SaveChanges();

                        return RedirectToAction("RebateStatusForRewards");
                    }
                    else if (user.Type == 2)
                    {
                        var rebateSelect = _db.RebateSelects.Where(x => x.RebateId == rebateId).FirstOrDefault();
                        _db.Entry(rebateSelect).State = System.Data.Entity.EntityState.Detached;

                        RebateSelect select = new RebateSelect()
                        {
                            Id = rebateSelect.Id,
                            IsActive = true
                        };
                        _db.RebateSelects.Attach(select);
                        _db.Entry(select).Property(x => x.IsActive).IsModified = true;
                        _db.Configuration.ValidateOnSaveEnabled = false;
                        _db.SaveChanges();
                        return RedirectToAction("RebateStatusForSelect");
                    }

                   
                }

            }
            return RedirectToAction("Index");

        }

        [HttpGet]
        public ActionResult SendMailToSupportTeam()
        {
            _builderId = Convert.ToInt32(Request.Cookies["UserId"].Value);
            var builder = _db.Users.Where(a => a.Id == _builderId && a.RoleId == 5).FirstOrDefault();
            UserViewModel userViewModel = new UserViewModel()
            {
                FirstName = builder.FirstName,
                LastName = builder.LastName,
                Email = builder.Email,
                PhoneNumber = builder.PhoneNumber
            };
            return View(userViewModel);
        }

        [HttpPost]
        public ActionResult SendMailToSupportTeam(UserViewModel userViewModel)
        {
            _builderId = Convert.ToInt32(Request.Cookies["UserId"].Value);
            var builder = _db.Users.Where(a => a.Id == _builderId && a.RoleId == 5).FirstOrDefault();
            string subject = "Possible Eligible Product Missing";
            string body = "<strong>" + builder.FirstName + " " + builder.LastName + "</strong>" + " with Builder name tried to enter missing product" +
                          " number and the system does not have this product as earning a rebate. Please confirm with Lixil that this product " +
                          "should not be added and update "+"<strong>"+builder.FirstName +" "+builder.LastName+"</strong>"+" with your findings"+".<br><br>";
            body += "Builder Name : " + builder.FirstName + " " + builder.LastName + ".<br>";
            body += "Email Address : " + builder.Email + ".<br>";
            body += "Phone Number : " + builder.PhoneNumber + ".<br>";
            body += "How would you like us to contact you : " + userViewModel.Contact + ".<br>";
            body += "Message : " + userViewModel.Description+".<br><br>";
            body += "Lixil Builder Rebate" + ".<br>";
            body += "877-239-9878";
            MailHelper mailHelper = new MailHelper();
            var status = mailHelper.CommonMail(builder.Email, subject, body);
            return RedirectToAction("Index");
        }

        public JsonResult InvoiceNumberExists(string DistributorInvoiceNumber)
        {
            _builderId = Convert.ToInt32(Request.Cookies["UserId"].Value);
            return Json(!_db.RebateRewards.Any(x => x.DistributorInvoiceNumber == DistributorInvoiceNumber && x.BuilderId == _builderId));
        }

        public JsonResult ProductExists(string ProductNumber, string DistributorInvoiceNumber)
        {
            _builderId = Convert.ToInt32(Request.Cookies["UserId"].Value);
            int productId = Convert.ToInt32(ProductNumber);
            string productNum = _db.Products.Where(x => x.Id == productId).Select(x => x.SKU).FirstOrDefault();
            var r = _db.InvoiceProducts.Any(x => x.InvoiceNumber == DistributorInvoiceNumber &&
                                                            x.ProductNumber == productNum && x.BuilderId == _builderId);
            return Json(!r, JsonRequestBehavior.AllowGet);
        }

        public ActionResult RebateStatusForRewards()
        {
            _builderId = Convert.ToInt32(Request.Cookies["UserId"].Value);
            //var builderRebateDtls = _db.RebateDetails.Where(x => x.BuilderId == _builderId && x.Type == 1).FirstOrDefault();
            var builderInvoices = _db.RebateRewards.Where(x => x.BuilderId == _builderId && x.IsActive == true).ToList();
            List<RebateRewardsViewModel> rebates = new List<RebateRewardsViewModel>();

            foreach (var invoice in builderInvoices)
            {
                //var dollarEstimated = 0.0;
                RebateRewardsViewModel rebateRewards = new RebateRewardsViewModel()
                {
                    OrderNumber = invoice.RecordNumber,
                    DistributorInvoiceNumber = invoice.DistributorInvoiceNumber,
                    //Status = _db.InvoiceStatus.Where(x => x.Id == invoice.Status).Select(a => a.Name).FirstOrDefault(),
                    DollarEstimated = invoice.DollarEstimated,
                    CreatedDate = invoice.CreatedDate
                };
                var rebateDetails = _db.RebateDetails.Where(x => x.Id == invoice.RebateId).FirstOrDefault();
                if (rebateDetails.status == 3 && rebateDetails.ModifiedBy == 1)
                {
                    rebateRewards.Status = "Approved";
                }
                else if(rebateDetails.status == 4 && rebateDetails.ModifiedBy == 1)
                {
                    rebateRewards.Status = "Denied";
                }
                else
                {
                    rebateRewards.Status = "Pending";
                }
                rebateRewards.DollarApproved = (rebateDetails.ApprovedAmount != null) ? rebateDetails.ApprovedAmount : "$0.00";
                rebateRewards.ApprovedOrDeniedDate = rebateDetails.ApprovedDeniedDate;
               
                //rebateRewards.StatusDate = (rebateDetails.ApprovedDeniedDate != null) ? rebateDetails.ApprovedDeniedDate.ToString() : "-";
                //var products = _db.InvoiceProducts.Where(x => x.InvoiceNumber == invoice.DistributorInvoiceNumber && x.BuilderId == _builderId).ToList();
                //foreach (var product in products)
                //{
                //    dollarEstimated = dollarEstimated + Convert.ToDouble(product.DollarEstimated);
                //}
                //rebateRewards.DollarEstimated = dollarEstimated.ToString();
                //rebateRewards.DollarApproved = invoice.DollarApproved;
                rebates.Add(rebateRewards);
            }
            return View(rebates);
        }

        public ActionResult InvoiceInformation(string orderNumber)
        {
            _builderId = Convert.ToInt32(Request.Cookies["UserId"].Value);
            //var builderInvoices = _db.RebateRewards.Where(x => x.DistributorInvoiceNumber == invoiceNumber && x.BuilderId == _builderId).FirstOrDefault();
            var builderInvoices = _db.RebateRewards.Where(x => x.RecordNumber == orderNumber).FirstOrDefault();
            var products = _db.InvoiceProducts.Where(x => x.InvoiceNumber == builderInvoices.DistributorInvoiceNumber && x.BuilderId == _builderId).ToList();

            //string status = _db.InvoiceStatus.Where(x => x.Id == builderInvoices.Status).Select(a => a.Name).FirstOrDefault();
            var rebateDetails = _db.RebateDetails.Where(x => x.Id == builderInvoices.RebateId).FirstOrDefault();
            ViewBag.InvoiceNumber = builderInvoices.DistributorInvoiceNumber;
            ViewBag.SubmittedDate = builderInvoices.CreatedDate.Value.ToString("MM/dd/yyyy");
            if (rebateDetails.status == 3 && rebateDetails.ModifiedBy == 1)
            {
                ViewBag.Status = "Approved";
            }
            else if (rebateDetails.status == 4 && rebateDetails.ModifiedBy == 1)
            {
                ViewBag.Status = "Denied";
            }
            else
            {
                ViewBag.Status = "Pending";
            }            

            List<InvoiceProductViewModel> invoiceProducts = new List<InvoiceProductViewModel>();
            ViewBag.Reason = rebateDetails.ReasonIfDenied;

            foreach (var product in products)
            {
                InvoiceProductViewModel productViewModel = new InvoiceProductViewModel()
                {
                    InvoiceDate = builderInvoices.DistributorInvoiceDate,
                    ProductNumber = product.ProductNumber,
                    Quantity = product.Quantity
                };
                var mfgName = _db.Manufacturers.Where(x => x.id == product.Manufacturer).Select(a => a.mfg_name).FirstOrDefault();
                productViewModel.Manufacturer = mfgName;

                invoiceProducts.Add(productViewModel);
            }

            return View(invoiceProducts);
        }

        public string GetUniqueKey(int size)
        {
            const string valid = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
            StringBuilder res = new StringBuilder();
            using (RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider())
            {
                byte[] uintBuffer = new byte[sizeof(uint)];

                while (size-- > 0)
                {
                    rng.GetBytes(uintBuffer);
                    uint num = BitConverter.ToUInt32(uintBuffer, 0);
                    res.Append(valid[(int)(num % (uint)valid.Length)]);
                }
            }

            return res.ToString();
        }

        public ActionResult RebateStatusForSelect()
        {
            _builderId = Convert.ToInt32(Request.Cookies["UserId"].Value);
            var rebates = _db.RebateSelects.Where(x => x.BuilderId == _builderId && x.IsActive == true).ToList();

            var rebateSelects = new List<RebateSelectViewModel>();
            foreach (var rebate in rebates)
            {
                RebateSelectViewModel rebateSelectViewModel = new RebateSelectViewModel()
                {
                    RebateId = rebate.RebateId,
                    OrderNumber = rebate.RecordNumber,
                    CreatedDate = rebate.CreatedDate,
                    DollarEstimated = rebate.DollarEstimated
                };
                var rebateDetails = _db.RebateDetails.Where(x => x.Id == rebate.RebateId).FirstOrDefault();
                //rebateSelectViewModel.Status = _db.InvoiceStatus.Where(x => x.Id == rebateDetails.status).Select(a => a.Name).FirstOrDefault();
                if (rebateDetails.status == 3 && rebateDetails.ModifiedBy == 1)
                {
                    rebateSelectViewModel.Status = "Approved";
                }
                else if (rebateDetails.status == 4 && rebateDetails.ModifiedBy == 1)
                {
                    rebateSelectViewModel.Status = "Denied";
                }
                else
                {
                    rebateSelectViewModel.Status = "Pending";
                }
                //rebateSelectViewModel.ModifiedBy = rebateDetails.ModifiedBy;
                rebateSelectViewModel.ApprovedorDeniedDate = rebateDetails.ApprovedDeniedDate;
                //rebateSelectViewModel.StatusDate = (rebateDetails.ApprovedDeniedDate != null) ? rebateDetails.ApprovedDeniedDate.ToString() : "-";
                rebateSelectViewModel.ApprovedAmount = (rebateDetails.ApprovedAmount != null) ? rebateDetails.ApprovedAmount : "$0.00";
                rebateSelects.Add(rebateSelectViewModel);
            }

            return View(rebateSelects);
        }

        public ActionResult SelectInvoiceInformation(string orderNumber)
        {
            _builderId = Convert.ToInt32(Request.Cookies["UserId"].Value);
            var rebate = _db.RebateSelects.Where(x => x.RecordNumber == orderNumber).FirstOrDefault();

            var rebateSelect = new RebateSelectViewModel()
            {
                OrderNumber = rebate.RecordNumber,
                SubDivisionName = rebate.SubDivisionName,
                Lot_ = rebate.Lot_,
                PerHomeRebate = rebate.PerHomeRebate,
                ClosingStatementDate = rebate.ClosingStatementDate
            };

            var rebateDetails = _db.RebateDetails.Where(x => x.Id == rebate.RebateId).FirstOrDefault();
            rebateSelect.ApprovedorDeniedDate = rebateDetails.ApprovedDeniedDate;
            //rebateSelect.StatusDate = (rebateDetails.ApprovedDeniedDate != null) ? rebateDetails.ApprovedDeniedDate.ToString() : "-";
            rebateSelect.ApprovedAmount = (rebateDetails.ApprovedAmount != null) ? rebateDetails.ApprovedAmount : "$0.00";
            rebateSelect.ReasonIfDenied = rebateDetails.ReasonIfDenied;
            //rebateSelect.Status = _db.InvoiceStatus.Where(x => x.Id == rebateDetails.status).Select(a => a.Name).FirstOrDefault();
            if(rebateDetails.status == 3 && rebateDetails.ModifiedBy == 1)
            {
                rebateSelect.Status = "Approved";
            }
            else if (rebateDetails.status == 4 && rebateDetails.ModifiedBy == 1)
            {
                rebateSelect.Status = "Denied";
            }
            else
            {
                rebateSelect.Status = "Pending";
            }
            return View(rebateSelect);
        }
        private string SaveFile(HttpPostedFileBase httpPostedFile, string location, string rNumber)
        {
            var fileName = rNumber + "_" + Path.GetFileNameWithoutExtension(httpPostedFile.FileName) + "_" +
                           DateTime.Now.Ticks.ToString();
            var fileExt = Path.GetExtension(httpPostedFile.FileName);
            var path = Server.MapPath(Path.Combine(location, fileName + fileExt));
            httpPostedFile.SaveAs(path);
            return fileName + fileExt;
        }
    }
}