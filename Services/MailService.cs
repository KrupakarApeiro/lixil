﻿using LixilRebate.Constants;
using LixilRebate.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;

namespace LixilRebate.Services
{
    public class MailService
    {
        string urL = ConfigurationManager.AppSettings["URL"].ToString();
        private readonly DBContext _db = new DBContext();
        public void SendRepBuilderMail(string Email, string repFirstName, string builderFullName)
        {
            try
            {
               
               
                var completeUrl = urL + "/Home";
                string subject = "Lixil Builder Rebate – Action Required";


                string body = "Great news Lixil "+ repFirstName+"!  "+ builderFullName + "&nbsp;has just submitted a request to earn a Lixil builder rebate.<br><br>";

                body += "Please log into&nbsp;<a href=" + completeUrl + ">Lixil Builder Rebate</a> and approve/deny the request.<br><br>";
                body += "If you have any questions, please contact Lixil Builder Rebate headquarters – 877-239-9878.<br><br>";
                body += "Happy Selling!<br><br>";
                body += "Lixil Builder Rebate<br>";
                body += "877-239-9878";


                var status = MailHelper.SendMailUser(Email, subject, body, 0, null, null);
            }
            catch { }
        }

        public void SendBuilderMail(string Email, string builderFullName, string repDetails)
        {
            try
            {
             
                var completeUrl = urL+"/Home";
                string subject = "<b>Lixil Builder Rebate – Registration Received</b>";
                string body = "Hello&nbsp;" + builderFullName + ".<br><br>";

                body += "Thank you for your interest in becoming part of the Lixil builder rebate program.  This email is to confirm we have received your request.<br><br>";
                body += "We will update the status of your request via <a href=" + completeUrl + ">Lixil Builder Rebate</a>.  We will also email you via your email address.<br><br>";
                if(repDetails!=null && repDetails != "")
                {
                    body += "If you would like more information please contact:" + "<br>" + repDetails + ".<br>";

                }
                else
                {
                    body += " Please contact Lixil.<br>";
                }
                body += "Lixil Builder Rebate<br>";
                body += "877-239-9878";
                var status = MailHelper.SendMailUser(Email, subject, body, 0, null, null);
            }
            catch { }
        }

        public void SendResetPasswordLink(string Email, string confirmEmail)
        {
            var completeUrl = urL + confirmEmail;
            string subject = "Reset Password link.";

            string body = "";
            body += "Thank you for contacting Lixil Builder Rebate.<br/>";
            body += "This email is in response to your request for password reset.  Please click the link below to reset your password.<br>";
            body += "<a href=" + completeUrl + ">Reset Password</a><br>";
            body += "Thank you,<br>";
             MailHelper.SendMailUser(Email, subject, body, 0, null, null);
        }
        public void SendAdminBuilderMail( string builderFullName, string zipCode)
        {
       
            try
            {


                var completeUrl = urL + "/Home";
                string subject = "Lixil Builder Rebate – Action Required";


                string body = "Hi Lee,<br><br>";

                body += builderFullName + " has registered with zip code "+ zipCode + " .  "+ zipCode + " does not have a rep attached to it.  Please contact Lixil and ask who should we assign the rep to.<br><br>";
                body += "Thank you!<br><br>";
                body += "Lixil Builder Rebate<br>";
                body += "877-239-9878";


                var status = MailHelper.SendMailAdmin(subject, body);
            }
            catch { }
        }

        public void ContactUs(string Id,string Name, string Email, string Message)
        {
            try
            {
                var completeUrl = urL + "/Home";
                string subject = "";
                if (Id != null || Id != "")
                {
                    subject = "Message from guest user";
                }
                else
                {
                    subject = "Message from " + Name;
                }


                string body = "Hi Lee,<br><br>";
                body += Message + "<br>";
                body += "Thank you!<br>";
                body += Name + "<br>";
                body += "Lixil Builder Rebate<br>";
                body += "877-239-9878";


                var status = MailHelper.SendMailAdmin(subject, body);
            }
            catch { }
        }

        public void SendBuilderDeniedMail(string BuilderName, string Reason, string Lixilname, string LixilEmail, string Builderemail)
        {
            string subject = "Lixil Builder Rebate - Request Denied";
            string body = "Hello " + BuilderName + ".<br><br>";
            body += "Thank you for your interest in becoming part of the Lixil Builder Rebate Program. Lixil regrets to inform you that the request has been denied for the following reason: " + Reason + ".<br><br>";
            body += "If you would like more information please contact: .<br>";
            body += Lixilname + " via " + LixilEmail + ".<br><br>";
            body += "Thank you for your interest in the Lixil Builder Rebate Program .<br><br>";
            body += "Lixil Builder Rebate .<br>";
            body += "877-239-9878";

            var status = MailHelper.SendMailUser(Builderemail, subject, body, 0, null, null);
        }

        public void SendMailManagerFinance(string BuilderName, string LixilName, string manfin)
        {
            string subject = "Lixil Builder Rebate - Action Required";
            string body = "Hello .<br><br>";
            string urL = ConfigurationManager.AppSettings["URL"].ToString();
            body += "Lixil Builder " + LixilName + " has just approved " + BuilderName + " for a Lixil builder rebate .<br><br>";
            body += "Please log into <a href=" + urL + ">Lixil Builder Rebate</a> and approve/deny the request. <br><br>";
            body += "If you have any questions, please contact Lixil Builder Rebate headquarters – 877-239-9878 .<br><br>";

            body += "Lixil Builder Rebate .<br>";
            body += "877-239-9878";

            if (manfin == "manager")
            {
                var status = MailHelper.SendMailUser("testmanagerlixil@mailinator.com", subject, body, 0, null, null);
                var status2 = MailHelper.SendMailUser("Jonlixil@mailinator.com", subject, body, 0, null, null);
            }

            else
            {
                var status = MailHelper.SendMailUser("financelixil@mailinator.com", subject, body, 0, null, null);
            }

        }

        public void SendDeniedMailRep(string RepName, string loginname, string loginemail, string buildername, string ReasonDenied, string lixil, string repemail)
        {
            string subject = "Lixil Builder Rebate - Request Denied";
            string body = "Hello " + RepName + " .<br><br>";
            body += "The Lixil builder rebate " + lixil + " " + loginname + " has denied the request to add " + buildername + " to the Lixil builder rebate program.  The " + lixil + " denied the builder for the following reason " + ReasonDenied + " .<br><br>";
            body += "If you believe this decision should be reviewed, please contact Lixil builder rebate " + lixil + " " + loginname + ". <br><br>";
            body += "Lixil Builder Rebate .<br>";
            body += "877-239-9878";

            var status = MailHelper.SendMailUser(repemail, subject, body, 0, null, null);
        }


        public void SendMailBuilderRewards(string BuilderName, string BuilderEmail)
        {
            string subject = "Lixil Builder Rebate - Request Approved";
            string body = "Hello " + BuilderName + " .<br><br>";
            string urL = ConfigurationManager.AppSettings["URL"].ToString();
            body += "Congratulations " + BuilderName + ", and welcome to the Lixil Builder Rebate program!.<br><br>";
            body += "" + BuilderName + " has been approved for the Lixil Builder Rebate program.  Please visit <a href=" + urL + ">Lixil Builder Rebate</a>  and begin entering your distributor invoices into the system to earn your Lixil builder rebate <br><br>";
            body += "If you have any questions, please contact Lixil Builder Rebate headquarters – 877-239-9878 .<br><br>";
            body += "Welcome to the Lixil Builder Rebate! .<br><br>";
            body += "Lixil Builder Rebate .<br>";
            body += "877-239-9878";

            var status = MailHelper.SendMailUser(BuilderEmail, subject, body, 0, null, null);
        }

        public void SendMailRepRewards(string buildername, string Repemail)
        {
            string subject = "Lixil Builder Rebate - Request Approved";
            string body = "Here's a quick update! .<br><br>";
            body += "Lixil manager has approved " + buildername + " for the Rewards rebate program.  " + buildername + " has received an email and can begin entering distributor invoices into the system to earn their Lixil builder rebate .<br><br>";
            body += "Please congratulate " + buildername + " and confirm they are comfortable with the process. <br><br>";
            body += "If you have any questions, please contact Lixil Builder Rebate headquarters – 877-239-9878. <br><br>";
            body += "Happy Selling! <br><br>";
            body += "Lixil Builder Rebate .<br>";
            body += "877-239-9878";

            var status = MailHelper.SendMailUser(Repemail, subject, body, 0, null, null);
        }

        public void SendMailRepSelectManager(string buildername, string repemail)
        {
            string subject = "Lixil Builder Rebate - Next Step in Process";
            string body = "Here's a quick update! .<br><br>";
            body += "Lixil manager has approved " + buildername + " for the Select rebate program. This request has now been forwarded to the finance team for final approval. If approved, contracts will be sent to the builder.<br><br>";
            body += "Happy Selling! <br><br>";
            body += "Lixil Builder Rebate .<br>";
            body += "877-239-9878";

            var status = MailHelper.SendMailUser(repemail, subject, body, 0, null, null);
        }

        public void SendMailBuilderSelect(string buildername, MemoryStream memory, string filename, string builderemail)
        {
            string subject = "Lixil Builder Rebate - Request Approved, Final Step";
            string body = "Congratulations " + buildername + ", and welcome to the Lixil Builder Rebate program! .<br><br>";
            body += "" + buildername + " has been approved for the Lixil Builder Rebate program. Attached are the contracts for you to complete and sign so " + buildername + " may begin entering the required data points to receive your Lixil rebate.<br><br>";
            body += "If you have any questions, please contact Lixil Builder Rebate headquarters – 877-239-9878. <br><br>";
            body += "We look forward to receiving your signed documents within the next 2 business days .<br><br>";
            body += "Lixil Builder Rebate .<br>";
            body += "877-239-9878";

            var status = MailHelper.SendMailUser(builderemail, subject, body, 1, memory, filename);
        }

        public void SendMailRepSelectFinance(string buildername, string repemail)
        {
            string subject = "Lixil Builder Rebate - Request Approved, Final Step";
            string body = "Great news! .<br><br>";
            body += "Lixil finance has approved " + buildername + " for the Select rebate program.  The system has generated the contracts and emailed them to " + buildername + " .<br><br>";
            body += "Please contact the builder and confirm they have received the contracts. <br><br>";
            body += "Happy Selling! <br><br>";
            body += "Lixil Builder Rebate .<br>";
            body += "877-239-9878";

            var status = MailHelper.SendMailUser(repemail, subject, body, 0, null, null);
        }

        public void SendMailManagerSelectFinance(string buildername, string rep)
        {
            string subject = "Lixil Builder Rebate - Request Approved, Final Step";
            string body = "Great news! .<br><br>";
            body += "Lixil finance has approved " + buildername + " for the Select rebate program.  The system has generated the contracts and emailed them to " + buildername + " .<br><br>";
            body += "We have sent an email to Lixil builder rep " + rep + " requesting the rep to contact the builder and confirm they have received the contracts. <br><br>";
            body += "Lixil Builder Rebate .<br>";
            body += "877-239-9878";

            var status = MailHelper.SendMailUser("testmanagerlixil@mailinator.com", subject, body, 0, null, null);
            var status1 = MailHelper.SendMailUser("Jonlixil@mailinator.com", subject, body, 0, null, null);
        }

        public void SendMailtoRepDelay(string BuilderName, string Repname, string RepEmail)
        {
            string urL = ConfigurationManager.AppSettings["URL"].ToString();
            string subject = "Lixil Builder Rebate – FINAL NOTICE for Approval/Denial";
            string body = "It’s been 7 days Lixil " + Repname + "!  " + BuilderName + " submitted a request to earn a Lixil builder rebate a week ago and we are showing you have not approved or denied the request. <br><br>";
            body += "Please log into <a href=" + urL + ">Lixil Builder Rebate</a> Now and approve/deny the request. <br><br>";
            body += "If you have any questions, please contact Lixil Builder Rebate headquarters – 877-239-9878 .<br><br>";
            body += "This is your final notification! <br><br>";
            body += "Lixil Builder Rebate .<br>";
            body += "877-239-9878";

            var status = MailHelper.SendMailUser(RepEmail, subject, body, 0, null, null);
        }

        public void SendMailtoManagerDelay(string BuilderName, string Repname, string manemail)
        {
            string urL = ConfigurationManager.AppSettings["URL"].ToString();
            string subject = "Lixil Builder Rebate – FINAL NOTICE for Approval/Denial";
            string body = "It’s been 7 days Brady and Jon.  Lixil rep " + Repname + " approved " + BuilderName + " for a Lixil builder rebate a week ago and we are showing you have not approved or denied the request. <br><br>";
            body += "Please log into <a href=" + urL + ">Lixil Builder Rebate</a> Now and approve/deny the request. <br><br>";
            body += "If you have any questions, please contact Lixil Builder Rebate headquarters – 877-239-9878 .<br><br>";
            body += "This is your final notification! <br><br>";
            body += "Lixil Builder Rebate .<br>";
            body += "877-239-9878";

            var status = MailHelper.SendMailUser("testmanagerlixil@mailinator.com", subject, body, 0, null, null);
        }


        public void SendMailtoBuilderSignedDelay(string BuilderName, string BuilderEmail)
        {
            string subject = "Lixil Builder Rebate – Missing Contract, Approval Pending";
            string body = "Hello " + BuilderName + " <br><br>";
            body += "We are concerned that it has been 7 days and we have not received your signed contract for the Lixil Builder Rebate program. <br><br>";
            body += "Please complete and sign the contracts within the next 2 business days so you can begin receiving your rebate. <br><br>";
            body += "If you have any questions, please contact Lixil Builder Rebate headquarters – 877-239-9878 .<br><br>";
            body += "We look forward to receiving your signed documents within the next 2 business days. <br><br>";
            body += "Lixil Builder Rebate .<br>";
            body += "877-239-9878";

            var status = MailHelper.SendMailUser(BuilderEmail, subject, body, 0, null, null);
        }

        public void SendMailtoRepContractExp180Days()
        {
            var currentdate = DateTime.Now;
            var futuredate = DateTime.Now.AddMonths(6);
            if (_db.ContractDetails.Any(s => s.IsActive == true && s.ContractStatus == 2 && s.ExpirationDate >= currentdate && s.ExpirationDate <= futuredate))
            {
                var lstcontracts = _db.ContractDetails.Where(s => s.IsActive == true && s.ContractStatus == 2 && s.ExpirationDate >= currentdate && s.ExpirationDate <= futuredate).ToList();
                foreach (var contract in lstcontracts)
                {
                    if (_db.RepBuilders.Any(s => s.BuilderId == contract.BuilderId))
                    {
                        var repid = _db.RepBuilders.Where(s => s.BuilderId == contract.BuilderId).Select(p => p.RepId).First();
                        var repname = _db.Users.Find(repid).FirstName;
                        var repemail = _db.Users.Find(repid).Email;
                        var builder = _db.Users.Where(s => s.Id == contract.BuilderId).Select(p => new { p.FirstName, p.LastName }).First();
                        string urL = ConfigurationManager.AppSettings["URL"].ToString();
                        string subject = "Lixil Builder Rebate – You have agreements expiring in 180 days";
                        string body = "ATTENTION Lixil Rep" + repname + "! <br><br>";
                        body += "" + builder.FirstName + " " + builder.LastName + " has a builder rebate contract set to expire within approximately 180 days.  Please work with the builder to renew the contract. <br><br>";
                        body += "To complete the process, please visit <a href=" + urL + ">Lixil Builder Rebate</a> today. <br><br>";
                        body += "If you have any questions, please contact Lixil Builder Rebate headquarters – 877-239-9878 .<br><br>";
                        body += "Happy Selling! <br><br>";
                        body += "Lixil Builder Rebate .<br>";
                        body += "877-239-9878";

                        var status = MailHelper.SendMailUser(repemail, subject, body, 0, null, null);
                    }
                }

            }

        }

        public void SendMailtoRepContractExp90Days()
        {
            var currentdate = DateTime.Now;
            var futuredate = DateTime.Now.AddMonths(3);
            if (_db.ContractDetails.Any(s => s.IsActive == true && s.ContractStatus == 2 && s.ExpirationDate >= currentdate && s.ExpirationDate <= futuredate))
            {
                var lstcontracts = _db.ContractDetails.Where(s => s.IsActive == true && s.ContractStatus == 2 && s.ExpirationDate >= currentdate && s.ExpirationDate <= futuredate).ToList();
                foreach (var contract in lstcontracts)
                {
                    if (_db.RepBuilders.Any(s => s.BuilderId == contract.BuilderId))
                    {
                        var repid = _db.RepBuilders.Where(s => s.BuilderId == contract.BuilderId).Select(p => p.RepId).First();
                        var repname = _db.Users.Find(repid).FirstName;
                        var repemail = _db.Users.Find(repid).Email;
                        var builder = _db.Users.Where(s => s.Id == contract.BuilderId).Select(p => new { p.FirstName, p.LastName }).First();
                        string urL = ConfigurationManager.AppSettings["URL"].ToString();
                        string subject = "Lixil Builder Rebate – You have agreements expiring in 90 days";
                        string body = "ATTENTION Lixil Rep" + repname + "! <br><br>";
                        body += "" + builder.FirstName + " " + builder.LastName + " has a builder rebate contract set to expire within approximately 90 days.  Please work with the builder to renew the contract. <br><br>";
                        body += "To complete the process, please visit <a href=" + urL + ">Lixil Builder Rebate</a> today. <br><br>";
                        body += "If you have any questions, please contact Lixil Builder Rebate headquarters – 877-239-9878 .<br><br>";
                        body += "Happy Selling! <br><br>";
                        body += "Lixil Builder Rebate .<br>";
                        body += "877-239-9878";

                        var status = MailHelper.SendMailUser(repemail, subject, body, 0, null, null);
                    }
                }

            }

        }
    }
}