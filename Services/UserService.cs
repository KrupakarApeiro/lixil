﻿using LixilRebate.Constants;
using LixilRebate.Models;
using LixilRebate.ViewModels;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;

namespace LixilRebate.Services
{
    public class UserService
    {

        public bool UserExists(string email)
        {
            using (DBContext _db = new DBContext())
            {
                return _db.Users.Any(a =>
                         a.Email == email && a.IsActive==true);
            }
        }
        public User VerifyUser(LoginViewModel loginViewModel)
        {
            PasswordHash passwordHash = new PasswordHash();
            var cryptoPassword = passwordHash.EncryptString(loginViewModel.Password);
            using (DBContext _db = new DBContext())
            {

                if (_db.Users.Any(a =>
                         a.Email == loginViewModel.Email && a.Password == cryptoPassword))

                {
                    var user = _db.Users.Single(a => a.Email == loginViewModel.Email);
                    return user;

                }

            }
            return null;
        }
        public List<SelectListItem> prodCat()
        {
            using (DBContext _db = new DBContext())
            {
                List<SelectListItem> pd = _db.ProductCategories.Select(s => new SelectListItem()
                {
                    Text = s.ProductName,
                    Value = s.ProductCategoryId.ToString(),
                    Selected = false
                }).ToList();
                return pd;
            }

        }

        public List<SelectListItem> ManufacturingPartners()
        {
            using (DBContext _db = new DBContext())
            {
                List<SelectListItem> pd = _db.PlumbingManufacturingPartners.Select(s => new SelectListItem()
                {
                    Text = s.Name,
                    Value = s.Id.ToString(),
                    Selected = false
                }).ToList();
                return pd;
            }

        }
        public string RegisterUser(UserViewModel userViewModel)
        {
            using (DBContext _db = new DBContext())
            {
                MailService mailService = new MailService();
                string repDetails = "";
                bool userExists = _db.Users.Any(x => x.Email == userViewModel.Email);
                if (!userExists)
                {
                    PasswordHash passwordHash = new PasswordHash();
                    User user = new User()
                    {
                        FirstName = userViewModel.FirstName,
                        LastName = userViewModel.LastName,
                        Email = userViewModel.Email,
                        Password = passwordHash.EncryptString(userViewModel.Password),
                        PhoneNumber = userViewModel.PhoneNumber,
                        CompanyName = userViewModel.CompanyName,
                        CompanyAddress1 = userViewModel.CompanyAddress1,
                        CompanyAddress2 = userViewModel.CompanyAddress2,
                        State = userViewModel.State,
                        City = userViewModel.City,
                        Zip = userViewModel.Zip,
                        AnnualNumberOfHomes = userViewModel.AnnualNumberOfHomes,
                        AveragePriceUnit = userViewModel.AveragePriceUnit,
                        WebsiteURL = userViewModel.WebsiteURL,
                        PercentageOfPlumbingUpgrades = userViewModel.PercentageOfPlumbingUpgrades,
                        //PlumbingManufacturingPartners = userViewModel.CurrentPlumbingManufacturingPartners,
                        InstallationPlumbers = userViewModel.InstallationPlumbers,
                        //PlumbingWholesalePartners = userViewModel.PlumbingWholesalePartners,
                        OpportunityBoxNotes = userViewModel.DescribeOpportunity,
                        CreatedDate = DateTime.Now,
                        IsActive = true,
                        RoleId = 5,
                        BuilderStatusId = 1,
                        ContractStatusId = 1,
                        ModifiedBy = 1,
                        Identifier = Guid.NewGuid().ToString()

                    };
                    _ = _db.Users.Add(user);
                    _ = _db.SaveChanges();
                    userViewModel.ProductName = prodCat();
                    if (userViewModel.ProductCategoryId != null)
                    {
                        List<SelectListItem> selectedItems = userViewModel.ProductName.Where(p => userViewModel.ProductCategoryId.Contains(int.Parse(p.Value))).ToList();
                        foreach (var selectedItem in selectedItems)
                        {
                            int prodId = int.Parse(selectedItem.Value);
                            bool exists = _db.UserProdCategories.Any(x => x.UserId == user.Id && x.ProductCategoryId == prodId);
                            if (!exists)
                            {
                                UserProdCategory userProdCategory = new UserProdCategory()
                                {
                                    UserId = user.Id,
                                    ProductCategoryId = prodId
                                };
                                _ = _db.UserProdCategories.Add(userProdCategory);
                                _ = _db.SaveChanges();

                            }
                        }
                    }
                    userViewModel.PlumbingManufacturingPartners = ManufacturingPartners();
                    if(userViewModel.ManufacturingPartnerId != null)
                    {
                        List<SelectListItem> selectedItems = userViewModel.PlumbingManufacturingPartners.Where(p => userViewModel.ManufacturingPartnerId.Contains(int.Parse(p.Value))).ToList();
                        foreach (var selectedItem in selectedItems)
                        {
                            int manfId = int.Parse(selectedItem.Value);
                            bool exists = _db.UserPlumbingManufacturers.Any(x => x.UserId == user.Id && x.PlumbingManufactureId == manfId);
                            if (!exists)
                            {
                                UserPlumbingManufacturer userPlumMan = new UserPlumbingManufacturer()
                                {
                                    UserId = user.Id,
                                    PlumbingManufactureId = manfId
                                };
                                _ = _db.UserPlumbingManufacturers.Add(userPlumMan);
                                _ = _db.SaveChanges();

                            }
                        }
                    }

                    if(userViewModel.PlumbWholePartState != null && userViewModel.PlumbWholePartCity != null && userViewModel.PlumbWholePartner != null)
                    {
                        UserPlumbingWholesalePartner userPlumbingWholesalePartner = new UserPlumbingWholesalePartner()
                        {
                            PStateId = userViewModel.PlumbWholePartState,
                            PCityId = userViewModel.PlumbWholePartCity,
                            PWholesalePartner = userViewModel.PlumbWholePartner,
                            UserId = user.Id
                        };
                        _db.UserPlumbingWholesalePartners.Add(userPlumbingWholesalePartner);
                        _db.SaveChanges();
                    }

                    if(userViewModel.WholesalePartners != null)
                    {
                        string[] splits = userViewModel.WholesalePartners.Split('|');
                        foreach (var w in splits)
                        {
                            var wholesale = w.Split(',');
                            if(wholesale[0] != "")
                            {
                                var wpartner = wholesale[2];
                                bool exists = _db.UserPlumbingWholesalePartners.Any(x => x.UserId == user.Id && x.PWholesalePartner.ToString() == wpartner);
                                if (!exists)
                                {
                                    UserPlumbingWholesalePartner userPlumbingWholesalePartner = new UserPlumbingWholesalePartner()
                                    {
                                        PStateId = Convert.ToInt32(wholesale[0]),
                                        PCityId = Convert.ToInt32(wholesale[1]),
                                        PWholesalePartner = Convert.ToInt32(wholesale[2]),
                                        UserId = user.Id
                                    };
                                    _db.UserPlumbingWholesalePartners.Add(userPlumbingWholesalePartner);
                                    _db.SaveChanges();
                                }
                            }
                        }
                    }
                    

                    var builderFullName = user.FirstName + " " + user.LastName;

                    var reps = _db.RepZipCodes.Where(x => x.ZipCode == user.Zip)
                                                                    .Select(s => new { s.RepId })
                                                                    .ToList();

                    if (reps.Count > 0)
                    {
                        foreach (var rep in reps)
                        {
                            var repBuilder = _db.Users.Where(x => x.Id == (int)rep.RepId)
                                                 .Select(s => new { s.Email, s.FirstName, s.LastName })
                                                 .FirstOrDefault();
                            RepBuilder repB = new RepBuilder()
                            {
                                RepId = (int)rep.RepId,
                                BuilderId = user.Id,
                                CreatedDate = DateTime.Now,
                                IsActive = true
                            };
                            _db.RepBuilders.Add(repB);
                            _db.SaveChanges();
                            var repFullName = repBuilder.FirstName + " " + repBuilder.LastName;
                            repDetails += repFullName + "&nbsp;via&nbsp;" + repBuilder.Email + "<br>";
                            try
                            {
                                mailService.SendRepBuilderMail(repBuilder.Email, repBuilder.FirstName, builderFullName);
                            }
                            catch { }
                        }


                    }
                    else
                    {
                        mailService.SendAdminBuilderMail(builderFullName, user.Zip);
                    }
                    try
                    {
                        mailService.SendBuilderMail(user.Email, builderFullName, repDetails);
                    }
                    catch { }

                    return "Success";
                }
                return "Failure";
            }

        }

        public string ResetPassword(ResetPasswordViewModel resetPassword)
        {
            using (DBContext _db = new DBContext())
            {
                PasswordHash passwordHash = new PasswordHash();

                if (resetPassword.Identifier != null)
                {
                    User user = _db.Users.Where(x => x.Identifier.Equals(resetPassword.Identifier)).FirstOrDefault();
                    if (user != null)
                    {
                        user.Password = passwordHash.EncryptString(resetPassword.Password);
                        _db.SaveChanges();

                        return "Success";
                    }

                    else
                    {
                        return "NotFound";
                    }
                }

                return "Failure";
            }
        }


        public string ChangePassword(ChangePasswordViewModel changePasswordViewModel)
        {
            using (DBContext _db = new DBContext())
            {
                PasswordHash passwordHash = new PasswordHash();
                var encryptPass = passwordHash.EncryptString(changePasswordViewModel.CurrentPassword);
                User user = _db.Users.Where(x => x.Id == changePasswordViewModel.userId).FirstOrDefault();
                if (user != null && user.Password == encryptPass)
                {

                    var newEncryptPass = passwordHash.EncryptString(changePasswordViewModel.NewPassword);
                    if (newEncryptPass == user.Password)
                    {
                        return "SamePassword";
                    }
                    user.Password = newEncryptPass;
                    _db.SaveChanges();
                    return "Success";
                }
                return "Failure";
            }

        }



        public string ResetPasswordlink(string Email, string confirmEmail)
        {
            try
            {
                MailService mailService = new MailService();
                using (DBContext _db = new DBContext())
                {
                    var user = _db.Users.Where(x => x.Email.Equals(Email)).FirstOrDefault();
                    mailService.SendResetPasswordLink(Email, confirmEmail);
                    return "Email send successfully.";
                }

            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }



        public string ContactUs(ContactUsViewModel contactUsVM)
        {
            using (DBContext _db = new DBContext())
            {

                ContactU contactUs = new ContactU()
                {

                    Name = contactUsVM.Name,
                    Email = contactUsVM.Email,
                    Message = contactUsVM.Message

                };
                if (contactUsVM.UserId != "")
                    contactUs.UserId = Convert.ToInt32(contactUsVM.UserId);
                _db.ContactUs.Add(contactUs);
                _db.SaveChanges();
                MailService mailService = new MailService();
                mailService.ContactUs(contactUsVM.UserId, contactUs.Name, contactUs.Email, contactUs.Message);
                return "Success";
            }

        }

        public UserEditViewModel GetUserDetails(string userId)
        {
            using (DBContext _db = new DBContext())
            {

                int Id = Convert.ToInt32(userId);
                var user = _db.Users.Single(x => x.Id.Equals(Id));

                UserEditViewModel userVM = new UserEditViewModel()
                {
                    Id = user.Id,
                    CompanyName = user.CompanyName,
                    CompanyAddress1 = user.CompanyAddress1,
                    CompanyAddress2 = user.CompanyAddress2,
                    State = user.State,
                    City = user.City,
                    Zip = user.Zip,
                    FirstName = user.FirstName,
                    LastName = user.LastName,
                    PhoneNumber = user.PhoneNumber,
                    Email = user.Email
                };
                return userVM;

            }

        }
        public string PostUserDetails(UserEditViewModel userEdit)
        {
            using(DBContext _db = new DBContext())
            {
                User user = _db.Users.Where(x => x.Id.Equals(userEdit.Id)).SingleOrDefault();
                if (user != null)
                {
                    user.CompanyName = userEdit.CompanyName;
                    user.CompanyAddress1 = userEdit.CompanyAddress1;
                    user.CompanyAddress2 = userEdit.CompanyAddress2;
                    user.State = userEdit.State;
                    user.City = userEdit.City;
                    user.Zip = userEdit.Zip;
                    user.FirstName = userEdit.FirstName;
                    user.LastName = userEdit.LastName;
                    user.PhoneNumber = userEdit.PhoneNumber;
                    user.Email = userEdit.Email;
                  
                    _db.SaveChanges();
                    return "Success";
                }
                return "Failure";
            }
           
          

        }
    }
}
