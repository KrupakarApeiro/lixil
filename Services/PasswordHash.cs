﻿using LixilRebate.Constants;
using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Web.Mvc;

namespace LixilRebate.Services
{
    public class PasswordHash
    {
        #region password

  
        public string EncryptString(string passwordText)
        {
            var passwordTextBytes = Encoding.UTF8.GetBytes(passwordText);
            var crypto = SymmetricAlgorithm.Create();
            var ms = new MemoryStream();
            var keyBytes = Encoding.ASCII.GetBytes(PasswordConstants.Key);
            var vectorBytes = Encoding.ASCII.GetBytes(PasswordConstants.Vector);
            var cs = new CryptoStream(ms, crypto.CreateEncryptor(keyBytes, vectorBytes), CryptoStreamMode.Write);
            cs.Write(passwordTextBytes, 0, passwordTextBytes.Length);
            cs.Close();
            return Convert.ToBase64String(ms.ToArray());
        }

       
        public string DecryptString(string cryptoPassword)
        {
            if (cryptoPassword != null)
            {
                var encryptedTextBytes = Convert.FromBase64String(cryptoPassword);
                var ms = new MemoryStream();
                var crypto = SymmetricAlgorithm.Create();

                var keyBytes = Encoding.ASCII.GetBytes(PasswordConstants.Key);
                var vectorBytes = Encoding.ASCII.GetBytes(PasswordConstants.Vector);
                var cs = new CryptoStream(ms, crypto.CreateDecryptor(keyBytes, vectorBytes), CryptoStreamMode.Write);
                cs.Write(encryptedTextBytes, 0, encryptedTextBytes.Length);
                cs.Close();
                return Encoding.UTF8.GetString(ms.ToArray());
            }
            return null;
        }

        #endregion
    }
}