﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LixilRebate.Attributes
{
	[AttributeUsage(AttributeTargets.Field | AttributeTargets.Property, AllowMultiple = false, Inherited = true)]
	public class FileSizeAttribute : ValidationAttribute, IClientValidatable
	{
		public int? MaxBytes { get; set; }

		public FileSizeAttribute(int maxBytes)
			: base("Please upload only supported file(s).")
		{
			MaxBytes = maxBytes;
			var MB = (MaxBytes / 1000000);
			if (MaxBytes.HasValue)
			{
				ErrorMessage = "Please upload file(s) with total size less than " + MB + " MB.";
			}
		}

		public override bool IsValid(object value)
		{
			if (value is IEnumerable<HttpPostedFileBase> files)
			{
				var bytes = 0;
				foreach (HttpPostedFileBase file in files)
				{
					if (file != null)
					{
						bytes += file.ContentLength;
					}
				}

				if (MaxBytes.HasValue)
				{
					return (bytes < MaxBytes.Value);
				}
			}

			return true;
		}

		IEnumerable<ModelClientValidationRule> IClientValidatable.GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
		{
			var rule = new ModelClientValidationRule
			{
				ValidationType = "filesize",
				ErrorMessage = FormatErrorMessage(metadata.DisplayName)
			};
			rule.ValidationParameters["maxbytes"] = MaxBytes;
			yield return rule;
		}
	}


	[AttributeUsage(AttributeTargets.Field | AttributeTargets.Property, AllowMultiple = false, Inherited = true)]
	public class FileCountAttribute : ValidationAttribute, IClientValidatable
	{
		public int? Count { get; set; }

		public FileCountAttribute(int count)
			: base("Please upload a supported file.")
		{
			Count = count;
			if (Count.HasValue)
			{
				ErrorMessage = "A maximum of " + Count + " files are allowed.";
			}
		}

		public override bool IsValid(object value)
		{
			if (value is IEnumerable<HttpPostedFileBase> files)
			{
				if (Count.HasValue)
				{
					return (files.Count() <= Count.Value);
				}
			}

			return true;
		}

		IEnumerable<ModelClientValidationRule> IClientValidatable.GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
		{
			var rule = new ModelClientValidationRule
			{
				ValidationType = "filecount",
				ErrorMessage = FormatErrorMessage(metadata.DisplayName)
			};
			rule.ValidationParameters["count"] = Count;
			yield return rule;
		}
	}

	[AttributeUsage(AttributeTargets.Field | AttributeTargets.Property, AllowMultiple = false, Inherited = true)]
	public class EnforceTrueAttribute : ValidationAttribute, IClientValidatable
	{
		public override bool IsValid(object value)
		{
			if (value == null) return false;
			if (value.GetType() != typeof(bool)) throw new InvalidOperationException("can only be used on boolean properties.");
			return (bool)value == true;
		}

		public override string FormatErrorMessage(string name)
		{
			return "The " + name + " field must be checked in order to continue.";
		}

		public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
		{
			yield return new ModelClientValidationRule
			{
				ErrorMessage = String.IsNullOrEmpty(ErrorMessage) ? FormatErrorMessage(metadata.DisplayName) : ErrorMessage,
				ValidationType = "enforcetrue"
			};
		}
	}

	[AttributeUsage(AttributeTargets.Field | AttributeTargets.Property, AllowMultiple = false, Inherited = true)]
	public class FileTypeAttribute : ValidationAttribute, IClientValidatable
	{
		private const string DefaultErrorMessage = "Only the following file types are allowed: {0}";
		private IEnumerable<string> ValidTypes { get; set; }

		public FileTypeAttribute(string validTypes)
		{
			ValidTypes = validTypes.Split(',').Select(s => s.Trim().ToLower());
			ErrorMessage = string.Format(DefaultErrorMessage, string.Join(" or ", ValidTypes));
		}

		protected override ValidationResult IsValid(object value, ValidationContext validationContext)
		{
			if (value is IEnumerable<HttpPostedFileBase> files)
			{
				foreach (HttpPostedFileBase file in files)
				{
					if (file != null && !ValidTypes.Any(e => file.FileName.ToLower().EndsWith(e)))
					{
						return new ValidationResult(ErrorMessageString);
					}
				}
			}
			return ValidationResult.Success;
		}

		public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
		{
			var rule = new ModelClientValidationRule
			{
				ValidationType = "filetype",
				ErrorMessage = ErrorMessageString
			};
			rule.ValidationParameters.Add("validtypes", string.Join(",", ValidTypes).ToLower());
			yield return rule;
		}
	}
}