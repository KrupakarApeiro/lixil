﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LixilRebate.ViewModels
{
    public class PlumbWholeSalePartnerViewModel
    {
        public int Id { get; set; }

        //[Required(ErrorMessage = "Please select Plumbing Wholesale Partner.")]
        public string WholesalePartnerName { get; set; }
        //[Required(ErrorMessage = "Please select Plumbing Wholesale Partner State.")]
        public Nullable<int> State { get; set; }
        //[Required(ErrorMessage = "Please select Plumbing Wholesale Partner City.")]
        public Nullable<int> City { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public Nullable<bool> IsActive { get; set; }
    }
}