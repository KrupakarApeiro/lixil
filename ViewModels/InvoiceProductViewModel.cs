﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LixilRebate.ViewModels
{
    public class InvoiceProductViewModel
    {
        public int id { get; set; }
        public string InvoiceDate { get; set; }
        public string ProductNumber { get; set; }
        public int? Quantity { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public bool? IsActive { get; set; }
        public string DollarEstimated { get; set; }
        public string InvoiceNumber { get; set; }
        public int? BuilderId { get; set; }

        public string Manufacturer { get; set; }
    }
}