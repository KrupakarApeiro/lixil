﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using LixilRebate.Attributes;

namespace LixilRebate.ViewModels
{
    public class RebateSelectViewModel
    {
        public int Id { get; set; }
        public int BuilderId { get; set; }

        [Display(Name = "Subdivison Name")]
        [Required(ErrorMessage = "Subdivison Name is required")]
        public string SubDivisionName { get; set; }

        [Display(Name = "Lot #")]
        [Required(ErrorMessage = "Lot # is required")]
        public string Lot_ { get; set; }

        [Display(Name = "Per Home Rebate")]
        [Required(ErrorMessage = "Per Home Rebate is required")]
        public string PerHomeRebate { get; set; }

        [Display(Name = "Closing Statement Date")]
        [Required(ErrorMessage = "Closing Statement Date is required")]
        public string ClosingStatementDate { get; set; }

        //[RegularExpression(@"([a-zA-Z0-9\s_\\.\-:])+(.png|.PNG|.pdf|.PDF|.xls|.xlsx|.csv|.jpeg|.JPEG)$", ErrorMessage = "Only pdf,xls,xlsx,png,jpeg files are allowed.")]
        [FileType("JPG,JPEG,PNG,PDF,XLS,XLSX,CSV")]
        [Required(ErrorMessage = "Upload Invoice is Required")]
        [FileSize(15728000)]
        [FileCount(6)]
        public List<HttpPostedFileBase> InvoiceFile { get; set; }
        public string Status { get; set; }
        public int? StatusId { get; set; }
        public string Comments { get; set; }
        public DateTime? ApprovedorDeniedDate { get; set; }
        public string BuilderEmail { get; set; }
        public string BuilderPhNo { get; set; }
        public string BuilderName { get; set; }
        public string ReasonIfDenied { get; set; }
        public decimal? DollarEstimated { get; set; }
        public string ApprovedAmount { get; set; }
        public string StatusDate { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public int? ModifiedBy { get; set; }
        public bool? IsActive { get; set; }
        public decimal? Value { get; set; }
        public int? RebateId { get; set; }
        public string Type { get; set; }
        public string OrderNumber { get; set; }
    }
}