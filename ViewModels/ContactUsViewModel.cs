﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace LixilRebate.ViewModels
{
    public class ContactUsViewModel
    {
        public string UserId { get; set; }
        [Required(ErrorMessage ="Please enter your name.")]
        public string Name { get; set; }
        [Required(ErrorMessage = "Please enter your email.")]
        [RegularExpression(@"^[a-zA-Z0-9_\.-]+@([a-zA-Z0-9-]+\.)+[a-zA-Z]{2,6}$", ErrorMessage = "Please enter a valid Email.")]

        public string Email { get; set; }
        [Required(ErrorMessage = "Please enter message.")]
        public string Message { get; set; }
    }
}