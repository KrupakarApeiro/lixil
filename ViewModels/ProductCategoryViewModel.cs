﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LixilRebate.ViewModels
{
    public class ProductCategoryViewModel
    {
        public int[] ProductCategoryId { get; set; }
        public List<SelectListItem> ProductName { get; set; }
    }
}