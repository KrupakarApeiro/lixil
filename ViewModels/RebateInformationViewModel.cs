﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LixilRebate.ViewModels
{
    public class RebateInformationViewModel
    {
        public List<RebateRewardsViewModel> RebateRewards { get; set; }
        public List<RebateSelectViewModel> RebateSelects { get; set; }
    }
}