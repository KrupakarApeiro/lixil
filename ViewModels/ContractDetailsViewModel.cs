﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using LixilRebate.Attributes;

namespace LixilRebate.ViewModels
{
    public class ContractDetailsViewModel
    {
        public int ContractId { get; set; }
        public int BuilderId { get; set; }
        [Display(Name = "Company Name")]
        public string CompanyName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        [Display(Name = "Phone Number")]
        public string PhoneNumber { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        [Display(Name = "Created Date")]
        public DateTime? CreatedDate { get; set; }
        [Display(Name = "Contract Status")]
        [Required(ErrorMessage = "Please select contract status")]
        public string ContractStatus { get; set; }
        
        [Display(Name = "Denied Reason")]
        public string ReasonDenied { get; set; }
        [Display(Name = "Modified Date")]
        public DateTime? ModifiedDate { get; set; }

        public DateTime? ExpirationDate { get; set; }
        public string ApproveDeniedString { get; set; }
        public string DateString { get; set; }

        [Display(Name = "Annual # of Homes")]
        public decimal? AnnualNoOfHomes { get; set; }
        [Display(Name = "Average Price Unit")]
        public decimal? AvgPriceUnit { get; set; }
        [Display(Name = "Website URL")]
        public string WebsiteURL { get; set; }
        [Display(Name = "Percentage Plumbing Upgrades")]
        public int? PercentPlumbingUpgrades { get; set; }
        [Display(Name = "Plumbing Manufacturing Partners")]
        public string PlumbManufacturePartners { get; set; }
        [Display(Name = "Installation Plumbers")]
        public string InstallationPlumb { get; set; }
        [Display(Name = "Plumbing WholeSale Partners")]
        public string PlumbWholesalePartners { get; set; }
        [Display(Name = "Product Categories")]
        public List<string> ProductCategories { get; set; }
        [Display(Name = "Opportunity Box Notes")]
        public string OpportunityBoxNotes { get; set; }
        public int AnnualHomes { get; set; }

        [FileType("PDF")]
        [FileSize(15728000)]
        public HttpPostedFileBase SignedAgreement { get; set; }

        public string AgreementString { get; set; }
    }

    public class RebateList
    {
        public List<ContractDetailsViewModel> contractslist { get; set; }
        public List<BuilderDetailsViewModel> builderslist { get; set; }
    }


}