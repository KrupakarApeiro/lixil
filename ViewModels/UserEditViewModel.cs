﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace LixilRebate.ViewModels
{
    public class UserEditViewModel
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Please enter Company Name.")]
        public string CompanyName { get; set; }
        [Required(ErrorMessage = "Please enter Company Address.")]
        public string CompanyAddress1 { get; set; }
        [Required(ErrorMessage = "Please enter Company Address2.")]
        public string CompanyAddress2 { get; set; }


        public string Zip { get; set; }
        [Required(ErrorMessage = "Please enter city.")]

        public string City { get; set; }
        [Required(ErrorMessage = "Please select state.")]

        public Nullable<int> State { get; set; }
        [Required(ErrorMessage = "Please enter First Name.")]
        public string FirstName { get; set; }
        [Required(ErrorMessage = "Please enter Last Name.")]
        public string LastName { get; set; }
        [Required(ErrorMessage = "Please enter Phone number.")]
        //[RegularExpression(@"^(\d{10})$", ErrorMessage = "Phone number contains 10 digits only.")]

        public string PhoneNumber { get; set; }
        public string Email { get; set; }

    }
}