﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LixilRebate.ViewModels
{
    public class AmountViewModel
    {
        public int AmountId { get; set; }
        public string AmountName { get; set; }
    }
}