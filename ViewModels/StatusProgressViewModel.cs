﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LixilRebate.ViewModels
{
    public class StatusProgressViewModel
    {
        public int BuilderStatusId { get; set; }
        public int? ContractStatusId { get; set; }
        public DateTime RegistrationSubmitted { get; set; }
        public DateTime? RegistrationModified { get; set; }
        public int? Type { get; set; }

        public DateTime? ContractSubmitted { get; set; }
        public DateTime? ContractModified { get; set; }
        public int? ContractModifiedBy { get; set; }

        public int? RegistrationModifiedBy { get; set; }

        public bool IsContractDateExpired { get; set; }
    }
}