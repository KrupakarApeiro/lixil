﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LixilRebate.ViewModels
{
    public class ProductViewModel
    {
        public int Id { get; set; }
        public string ProductName { get; set; }
        public string ProductNumber { get; set; }
        public Nullable<int> ProductCategoryId { get; set; }
        public string SKU { get; set; }
        public string ModelNumber { get; set; }
        public Nullable<int> Manufacturer { get; set; }
        public Nullable<decimal> ListPrice { get; set; }
    }
}