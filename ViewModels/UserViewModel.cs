﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace LixilRebate.ViewModels
{
    public class UserViewModel
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Please enter Company Name.")]
        public string CompanyName { get; set; }
        [Required(ErrorMessage = "Please enter Company Address.")]
        public string CompanyAddress1 { get; set; }
        public string CompanyAddress2 { get; set; }

        [Required(ErrorMessage = "Please enter zip.")]
        [RegularExpression(@"^(\d{5})$", ErrorMessage = "Zip contains 5 digits only.")]
        public string Zip { get; set; }
        [Required(ErrorMessage = "Please enter city.")]

        public string City { get; set; }
        [Required(ErrorMessage = "Please select state.")]

        public Nullable<int> State { get; set; }
       

        public string StateName { get; set; }
        [Required(ErrorMessage = "Please enter First Name.")]
        public string FirstName { get; set; }
        [Required(ErrorMessage = "Please enter Last Name.")]
        public string LastName { get; set; }
        [Required(ErrorMessage = "Please enter Phone number.")]
        //[RegularExpression(@"^(\d{10})$", ErrorMessage = "Phone number contains 10 digits only.")]
        //[RegularExpression(@"/\(?([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})/", ErrorMessage = "Not a valid phone number")]
        [Remote("PhoneNumberExists", "Home", HttpMethod = "POST", ErrorMessage = "Phone Number already registered.")]
        public string PhoneNumber { get; set; }
        [Required(ErrorMessage = "Please enter Email.")]
        [RegularExpression(@"^[a-zA-Z0-9_\.-]+@([a-zA-Z0-9-]+\.)+[a-zA-Z]{2,6}$", ErrorMessage = "Please enter a valid Email.")]
        [Remote("EmailExists", "Home", HttpMethod = "POST", ErrorMessage = "Email already registered.")]
        public string Email { get; set; }
        [Required(ErrorMessage = "Please enter Password.")]
        [RegularExpression(@"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[^\da-zA-Z]).{6,15}$", ErrorMessage = "Password must be atleast 6 characters and should contain one lowercase, uppercase, special character, number (eg:Examlpe@1).")]
        public string Password { get; set; }
        [Required(ErrorMessage = "Please enter Confirm Password.")]
        [System.ComponentModel.DataAnnotations.Compare("Password", ErrorMessage = "Password does not match.")]
        public string ConfirmPassword { get; set; }
        [Required(ErrorMessage = "Annual Number Of Homes is required.")]
        // [RegularExpression(@"^(\d{10})$", ErrorMessage = "Annual Number Of Homes must be a number.")]

        public Nullable<decimal> AnnualNumberOfHomes { get; set; }
        [Required(ErrorMessage = "Average Home Sales Price is required.")]
        //[RegularExpression(@"^(\d{10})$", ErrorMessage = "Average Price Unit must be a number.")]


        public Nullable<decimal> AveragePriceUnit { get; set; }
        [Required(ErrorMessage = "Website URL is required.")]
        [RegularExpression(@"^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$", ErrorMessage = "Please enter a valid URL.")]
        //[RegularExpression(@"[a-z0-9]+(.[a-z]+)$", ErrorMessage = "Please enter a valid website URL.")]
        public string WebsiteURL { get; set; }
        [Required(ErrorMessage = "Percentage Of Plumbing Upgrades is required.")]
        //[RegularExpression(@"^(\d{10})$", ErrorMessage = "Percentage Of Plumbing Upgrades must be a number.")]

        public Nullable<int> PercentageOfPlumbingUpgrades { get; set; }

        public string CurrentPlumbingManufacturingPartners { get; set; }
        [Required(ErrorMessage = "Installation Plumbers is required.")]

        public string InstallationPlumbers { get; set; }
       

        public string PlumbingWholesalePartners { get; set; }
        [Required(ErrorMessage = "Describe Opportunity is required.")]


        public string DescribeOpportunity { get; set; }

        public Nullable<int> BuilderStatusId { get; set; }
        public Nullable<int> ContractStatusId { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<int> RoleId { get; set; }
        public string RoleName { get; set; }
        public Nullable<int> Type { get; set; }



        [Required(ErrorMessage = "Please select product category")]
        public int[] ProductCategoryId { get; set; }
        public List<SelectListItem> ProductName { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public string Contact { get; set; }

        [Required(ErrorMessage = "This field is required")]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }

        [Required(ErrorMessage = "Please select Plumbing Manufacturing Partner")]
        public int[] ManufacturingPartnerId { get; set; }
        public List<SelectListItem> PlumbingManufacturingPartners { get; set; }

        [Required(ErrorMessage = "Please select Plumbing Wholesale Partner State.")]
        public Nullable<int> PlumbWholePartState { get; set; }

        [Required(ErrorMessage = "Please select Plumbing Wholesale Partner City.")]
        public Nullable<int> PlumbWholePartCity { get; set; }

        [Required(ErrorMessage = "Please select Plumbing Wholesale Partner.")]
        public Nullable<int> PlumbWholePartner { get; set; }

        public string WholesalePartners { get; set; }

     
    }

    public class ValidatePasswordClass
    {
        static bool ValidatePassword(string password)
        {
            const int MIN_LENGTH = 8;
            const int MAX_LENGTH = 15;

            if (password == null) throw new ArgumentNullException();

            bool meetsLengthRequirements = password.Length >= MIN_LENGTH && password.Length <= MAX_LENGTH;
            bool hasUpperCaseLetter = false;
            bool hasLowerCaseLetter = false;
            bool hasDecimalDigit = false;

            if (meetsLengthRequirements)
            {
                foreach (char c in password)
                {
                    if (char.IsUpper(c)) hasUpperCaseLetter = true;
                    else if (char.IsLower(c)) hasLowerCaseLetter = true;
                    else if (char.IsDigit(c)) hasDecimalDigit = true;
                }
            }

            bool isValid = meetsLengthRequirements
                        && hasUpperCaseLetter
                        && hasLowerCaseLetter
                        && hasDecimalDigit
                        ;
            return isValid;

        }
    }


}