﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LixilRebate.ViewModels
{
    public class InvoicePathViewModel
    {
        public string InvoiceType { get; set; }
        public string InvoicePath { get; set; }
    }
}