﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LixilRebate.ViewModels
{
    public class ContractTemplateViewModel
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string Template { get; set; }
        [Display(Name = "Builder Id")]
        public int BuilderId { get; set; }
        [Display(Name = "Company Name")]
        public string CompanyName { get; set; }
        public string State { get; set; }
        public string City { get; set; }
        [Display(Name = "Registration Date")]
        public DateTime? RegDate { get; set; }
        [Display(Name = "First Name")]
        public string FirstName { get; set; }
        [Display(Name = "Last Name")]
        public string LastName { get; set; }
        [Display(Name = "Phone Number")]
        public string PhoneNumber { get; set; }
        public string DateString { get; set; }
        public string TemplateColumn1 { get; set; }
        public string TemplateColumn2 { get; set; }
        public string TemplateColumn3 { get; set; }
        public string TemplateColumn4 { get; set; }
        public string TemplateColumn5 { get; set; }
        public string TemplateColumn6 { get; set; }
        public string TemplateColumn7 { get; set; }
        public string TemplateColumn8 { get; set; }
        public string TemplateColumn9 { get; set; }

    }
}