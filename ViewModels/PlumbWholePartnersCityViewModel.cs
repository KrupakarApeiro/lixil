﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LixilRebate.ViewModels
{
    public class PlumbWholePartnersCityViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public Nullable<int> StateId { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public Nullable<bool> IsActive { get; set; }
    }
}