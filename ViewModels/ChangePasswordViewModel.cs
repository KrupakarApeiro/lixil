﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace LixilRebate.ViewModels
{
    public class ChangePasswordViewModel
    {
		public int userId { get; set; }
		[Required(ErrorMessage = "Please enter Current Password.")]
		[DataType(DataType.Password)]
		public string CurrentPassword { get; set; }
        [Required(ErrorMessage = "Please enter New Password.")]
		[DataType(DataType.Password)]
		[StringLength(20, MinimumLength = 6, ErrorMessage = "Password must be at least 6 and at most 20 characters long.")]
		[RegularExpression(@"(^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{4,}$)", ErrorMessage = "Password must contain atleast one lowercase, uppercase, special character, number.")]
		public string NewPassword { get; set; }

		[Required(ErrorMessage = "Please enter Confirm Password.")]
		[DataType(DataType.Password)]
		[Compare("NewPassword", ErrorMessage = "Password does not match.")]

		public string ConfirmPassword { get; set; }
	}
}