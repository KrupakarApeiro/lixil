﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LixilRebate.ViewModels
{
    public class ZipCodeViewModel
    {
        public int Id { get; set; }
        public string Zipcode { get; set; }
    }
}