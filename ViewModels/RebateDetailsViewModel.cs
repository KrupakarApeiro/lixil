﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LixilRebate.ViewModels
{
    public class RebateDetailsViewModel
    {
        public int Id { get; set; }
        public int BuilderId { get; set; }
        public int Type { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public DateTime? ApprovedDeniedDate { get; set; }
        public string ReasonIfDenied { get; set; }
        public int? ModifiedBy { get; set; }
        public decimal? ApprovedAmount { get; set; }
        public string Comments { get; set; }
        public bool? IsActive { get; set; }
        public int? status { get; set; }
       
    }
}