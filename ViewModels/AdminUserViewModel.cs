﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Xunit;
using CompareAttribute = System.ComponentModel.DataAnnotations.CompareAttribute;

namespace LixilRebate.ViewModels
{
    public class UserViewModel
    {
        public int Id { get; set; }
        [Required]
        public string FirstName { get; set; }
        public string LastName { get; set; }

        [Required]
        public string Password { get; set; }
        [Required(ErrorMessage = "Please enter Confirm Password.")]
        [Compare("Password", ErrorMessage = "Password does not match.")]
        public string ConfirmPassword { get; set; }
        [Required(ErrorMessage = "Please enter Email Id.")]
        [RegularExpression(@"^[a-zA-Z0-9_\.-]+@([a-zA-Z0-9-]+\.)+[a-zA-Z]{2,6}$", ErrorMessage = "Please enter a valid EmailId.")]
        [Remote("EmailExists", "Home", HttpMethod = "POST", ErrorMessage = "EmailId already registered.")]
        public string EmailId { get; set; }
        [Remote("ZipCodeExists", "Home", HttpMethod = "POST", ErrorMessage = "ZipCode already assigned.")]
        public int[] ZipCode { get; set; }
        public List<SelectListItem> ZipcodesList { get; set; }

        public int RoleId { get; set; }
    }
    
}