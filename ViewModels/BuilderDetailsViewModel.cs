﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LixilRebate.ViewModels
{
    public class BuilderDetailsViewModel
    {
        [Display(Name = "Builder Id")]
        public int BuilderId { get; set; }
        [Display(Name = "Company Name")]
        public string CompanyName { get; set; }
        public string Address { get; set; }
        public string Zip { get; set; }
        public string State { get; set; }
        public string Email { get; set; }
        public string City { get; set; }
        [Display(Name = "Registration Date")]
        public DateTime? RegDate { get; set; }
        [Display(Name = "First Name")]
        public string FirstName { get; set; }
        [Display(Name = "Last Name")]
        public string LastName { get; set; }
        [Display(Name = "Phone Number")]
        public string PhoneNumber { get; set; }
        [Display(Name = "Annual # of Homes")]
        public decimal? AnnualNoOfHomes { get; set; }
        [Display(Name = "Average Home Sale Price")]
        public decimal? AvgPriceUnit { get; set; }
        [Display(Name = "Website URL")]
        public string WebsiteURL { get; set; }
        [Display(Name = "Percentage Plumbing Upgrades")]
        public int? PercentPlumbingUpgrades { get; set; }
        [Display(Name = "Plumbing Manufacturing Partners")]
        public List<string> PlumbManufacturePartners { get; set; }
        [Display(Name = "Installation Plumbers")]
        public string InstallationPlumb { get; set; }
        [Display(Name = "Plumbing Wholesale Partners")]
        public List<string> PlumbWholesalePartners { get; set; }
        [Display(Name = "Product Categories")]
        public List<string> ProductCategories { get; set; }
        [Display(Name = "Opportunity Box Notes")]
        public string OpportunityBoxNotes { get; set; }
        [Display(Name = "Program Type")]
        [Required(ErrorMessage = "Please Select Program Type")]
        public string RebateType { get; set; }
        [Display(Name = "Model Home Co-Op")]
        [Required(ErrorMessage = "Model Home Co - Op is required")]
        public string ModelHomeCoOp { get; set; }
        [Display(Name = "Per House Rebate")]
        [Required(ErrorMessage = "Please Enter Per House Rebate")]
        public string PerHouseRebate { get; set; }
        [Display(Name = "Term of Agreement")]
        [Required(ErrorMessage = "Term of Agreement is required")]
        public string TermAgreement { get; set; }
        [Display(Name = "Model Home CO-OP Value")]
        [Required(ErrorMessage = "Please Select Model Home CO-OP Value")]
        public string Value { get; set; }
        [Display(Name = "Builder Status")]
        [Required(ErrorMessage = "Please Select Builder Status")]
        public string BuilderStatus { get; set; }

        [Display(Name = "Denied Reason")]
        [Required(ErrorMessage = "Enter the Reason")]
        public string ReasonDenied { get; set; }
        [Display(Name = "Other Amount")]
        [Required(ErrorMessage = "Please Enter Amount")]
        public string OtherAmount { get; set; }

        public int AmountId { get; set; }
        [Display(Name = "Last Approval")]
        public string LastApproval { get; set; }
        [Display(Name = "Modified Date")]
        public DateTime? ModifiedDate { get; set; }

        public string Amount { get; set; }
        public string DateString { get; set; }

        public int AnnualHomes { get; set; }
        public int? Edit { get; set; }
        [Required(ErrorMessage = "Please Enter Contract Length in years")]
        public string ContractLengthYears { get; set; }
        [Required(ErrorMessage = "Please Select Product SKUs")]
        public int[] ProductNumber { get; set; }
        public int[] Bathroom1 { get; set; }
        public int[] Bathroom2 { get; set; }
        public int[] Bathroom3 { get; set; }
        public int[] Kitchen1 { get; set; }
        public int[] Kitchen2 { get; set; }
        public int[] LaundryRoom { get; set; }
        public int[] Other { get; set; }
        public List<SelectListItem> Products { get; set; }
        [Required(ErrorMessage = "Please Select Room")]
        public string Room { get; set; }

        public List<string> MasterBathroom { get; set; }
        public List<string> sBathroom1 { get; set; }
        public List<string> sBathroom2 { get; set; }
        public List<string> sBathroom3 { get; set; }
        public List<string> sKitchen1 { get; set; }
        public List<string> sKitchen2 { get; set; }
        public List<string> sLaundryroom { get; set; }
        public List<string> sOther { get; set; }
        public string Roomskus { get; set; }

        public string RoomProductSKUs { get; set; }
    }

}