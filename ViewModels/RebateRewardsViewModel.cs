﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;
using Microsoft.Practices.EnterpriseLibrary.Validation;
using ExpressiveAnnotations.Attributes;
using LixilRebate.Attributes;
using System.Web.Mvc;

namespace LixilRebate.ViewModels
{
    public class RebateRewardsViewModel
    {
        public int Id { get; set; }
        public int BuilderId { get; set; }

        [Required(ErrorMessage = "Distributor Invoice Date is required")]
        [Display(Name = "Distributor Invoice Date")]        
        public string DistributorInvoiceDate { get; set; }

        [Display(Name = "Distributor Invoice Number")]
        [Required(ErrorMessage = "Distributor Invoice Number is required")]
        [Remote("InvoiceNumberExists","Builder",HttpMethod = "POST", ErrorMessage = "Invoice Number already exists.")]
        public string DistributorInvoiceNumber { get; set; }

        [Display(Name = "Manufacturer")]
        [Required(ErrorMessage = "Please select Manufacturer")]
        public string MfgName { get; set; }

        [Display(Name = "Lixil Product Purchased")]
        [Required(ErrorMessage = "Product Number is required")]
        [Remote("ProductExists", "Builder", AdditionalFields = "DistributorInvoiceNumber", HttpMethod = "POST", ErrorMessage = "Product Number already exists.")]
        public string ProductNumber { get; set; }

        public bool secondRequest { get; set; }

        [Required(ErrorMessage = "Quantity is required")]
        public int Quantity { get; set; }

        [Required(ErrorMessage = "Please select one option")]
        public string IsAnotherProduct { get; set; }       
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public int? ModifiedBy { get; set; }
        public bool? IsActive { get; set; }
        public decimal? Value { get; set; }
        public int? RebateId { get; set; }
        public int? StatusId { get; set; }
        public string Type { get; set; }

        //[RegularExpression(@"([a-zA-Z0-9\s_\\.\-:])+(.png|.PNG|.pdf|.PDF|.xls|.xlsx|.csv|.jpeg|.JPEG)$", ErrorMessage = "Only pdf,xls,xlsx,png,jpeg files are allowed.")]
        [FileType("JPG,JPEG,PNG,PDF,XLS,XLSX,CSV")]
        [Required(ErrorMessage = "Upload Invoice is Required")]
        [FileSize(15728000)]
        [FileCount(6)]
        public List<HttpPostedFileBase> InvoiceFile { get; set; }
        public string OrderNumber { get; set; }
        public string Status { get; set; }
        public DateTime? ApprovedOrDeniedDate { get; set; }

        public string StatusDate { get; set; }

        public string BuilderEmail { get; set; }

        public string BuilderPhNo { get; set; }

        public string BuilderName { get; set; }
        public string ReasonIfDenied { get; set; }
        public string DollarEstimated { get; set; }
        public string DollarApproved { get; set; }
        public string Comments { get; set; }
        public virtual RebateDetailsViewModel RebateDetails { get; set; }

    }
}