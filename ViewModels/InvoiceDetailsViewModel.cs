﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LixilRebate.ViewModels;

namespace LixilRebate.ViewModels
{
    public class InvoiceDetailsViewModel
    {
        public RebateRewardsViewModel RebateRewardsViewModel { get; set; }
        public RebateSelectViewModel RebateSelectViewModel { get; set; }
        public RebateRedemptionViewModel RebateRedemptionViewModel { get; set; }
        public List<InvoiceProductViewModel> InvoiceProductViewModels { get; set; }
        public List<InvoicePathViewModel> InvoicePathViewModels { get; set; }
        public UserViewModel UserViewModel { get; set; }
    }
}