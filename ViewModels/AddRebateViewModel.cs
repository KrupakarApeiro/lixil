﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace LixilRebate.ViewModels
{
    public class AddRebateViewModel
    {
        [Display(Name = "Builder")]
        [Required(ErrorMessage = "Please Select Builder")]
        public int BuilderId { get; set; }
      
    }
}