﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using LixilRebate.Attributes;

namespace LixilRebate.ViewModels
{
    public class RebateRedemptionViewModel
    {
        public int id { get; set; }

        [Display(Name = "Name funds should be sent to ")]
        [Required(ErrorMessage = "Name is required")]
        public string Name { get; set; }

        [Display(Name = "Address")]
        [Required(ErrorMessage = "Address is required")]
        public string Address1 { get; set; }

        [Display(Name = "Address 2")]
        public string Address2 { get; set; }

        [Required(ErrorMessage = "City is required")]
        public string City { get; set; }

        [Required(ErrorMessage = "State is required")]
        public int? State { get; set; }
        public string StateName { get; set; }

        [Required(ErrorMessage = "Zip is required")]
        [RegularExpression(@"^(\d{5})$", ErrorMessage = "Zip is not valid.")]
        public string Zip { get; set; }

        [EnforceTrue(ErrorMessage = "This field is required")]

        //[CustomValidation(typeof(BoolValidation), "ValidateBool")]
        public bool IsAutorized { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public int? ModifiedBy { get; set; }
        public bool? IsActive { get; set; }
        public int? RebateId { get; set; }

        [Display(Name = "Receive Rebate Option")]
        [Required(ErrorMessage = "Receive Rebate Option is required")]
        public string ReceiveRebateOption { get; set; }
        public int? BuilderId { get; set; }        

        
    }
}