﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using LixilRebate.ViewModels;
using System.IO;
using System.Configuration;

namespace LixilRebate.Constants
{
	public class MailHelper
    {

		static string host = "smtp.gmail.com";
		static int port = 587;
		static string email_to = "apeirouser@gmail.com";
		static string senderEmail = "apeirouser@gmail.com";
		static string senderPassword = "Dev.net@inc";
		static bool enableSsl = true;

		public string CommonMail(string from, string subject, string body)
		{
			try
			{
				var smtpClient = new SmtpClient()
				{
					Host = host,
					Port = port,
					EnableSsl = enableSsl,

					DeliveryMethod = SmtpDeliveryMethod.Network,
					UseDefaultCredentials = false,
					Credentials = new NetworkCredential(senderEmail, senderPassword)
				};
				var mailMessage = new MailMessage(from, email_to, subject, body)
				{
					IsBodyHtml = true
				};
				
				smtpClient.Send(mailMessage);
				return "Sent";
			}
			catch(Exception ex)
			{
				return ex.Message;
			}
		}
		public static string SendMailUser(string to, string subject, string body, int? attch, MemoryStream output, string filename)
		{
			try
			{
				SmtpClient smtpClient = new SmtpClient();
				MailMessage mailMessage = new MailMessage();
				mailMessage.To.Add(new MailAddress(to));
				mailMessage.Subject = subject;
				mailMessage.Body = body;
				mailMessage.IsBodyHtml = true;
				smtpClient.EnableSsl = true;
				if (attch == 1)
					mailMessage.Attachments.Add(new Attachment(output, filename));
				smtpClient.Send(mailMessage);
				return "sent";
			}
			catch (Exception ex) { return ex.Message; }
		}


		public static string SendMailAdmin(string subject, string body)
		{
			try
			{
				string cc = ConfigurationManager.AppSettings["AdminMail"].ToString();
				string builderManagerEmail = ConfigurationManager.AppSettings["BuilderManager"].ToString();
				string LixilSystemMail = ConfigurationManager.AppSettings["LixilSystemEmail"].ToString();
				List<string> emailList = new List<string>();
				emailList.Add(builderManagerEmail);
				emailList.Add(LixilSystemMail);
				
				var smtpClient = new SmtpClient()
				{
					Host = host,
					Port = port,
					EnableSsl = enableSsl,

					DeliveryMethod = SmtpDeliveryMethod.Network,
					UseDefaultCredentials = false,
					Credentials = new NetworkCredential(senderEmail, senderPassword)
				};
				var mailMessage = new MailMessage();
				mailMessage.From =new MailAddress(email_to);
				foreach(var e in emailList)
                {
					mailMessage.To.Add(e);
				}
				mailMessage.Subject = subject;
				mailMessage.Body = body;
				mailMessage.CC.Add(cc);
				mailMessage.IsBodyHtml = true;
				smtpClient.Send(mailMessage);
				return "Sent";
			}
			catch (Exception ex)
			{
				return ex.Message;
			}
		}
	}
}