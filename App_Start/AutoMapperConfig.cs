﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LixilRebate.Models;
using LixilRebate.ViewModels;

namespace LixilRebate.App_Start
{
    public class AutoMapperConfig
    {
        public MapperConfiguration Configure()
        {
            var config = new MapperConfiguration(cfg =>
            {
                //way one 
                //cfg.CreateMap<User, UserViewModel>();
                //way two
                cfg.AddProfile<AuthorMappingProfile>();
            }
           );

            return config;
        }

        public class AuthorMappingProfile : Profile
        {
            public AuthorMappingProfile()
            {
                CreateMap<User, UserViewModel>().ReverseMap();
                CreateMap<RebateReward, RebateRewardsViewModel>().ReverseMap();
            }
        }

    }
}