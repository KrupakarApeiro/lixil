﻿$.validator.unobtrusive.adapters.add('filetype', ['validtypes'], function (options) {
	options.rules['filetype'] = { validtypes: options.params.validtypes.split(',') };
	options.messages['filetype'] = options.message;
});

$.validator.addMethod("filetype", function (value, element, param) {
	for (var i = 0; i < element.files.length; i++) {
		var extension = getFileExtension(element.files[i].name);
		if ($.inArray(extension, param.validtypes) === -1) {
			return false;
		}
	}
	return true;
});

function getFileExtension(fileName) {
	if (/[.]/.exec(fileName)) {
		return /[^.]+$/.exec(fileName)[0].toLowerCase();
	}
	return null;
}

jQuery.validator.unobtrusive.adapters.add('filesize', ['maxbytes'], function (options) {
	// Set up test parameters
	var params = {
		maxbytes: options.params.maxbytes
	};

	// Match parameters to the method to execute
	options.rules['filesize'] = params;
	if (options.message) {
		// If there is a message, set it for the rule
		options.messages['filesize'] = options.message;
	}
});

jQuery.validator.addMethod("filesize", function (value, element, param) {
	if (value === "") {
		// no file supplied
		return true;
	}

	var maxBytes = parseInt(param.maxbytes);
	var filesize = 0;
	// use HTML5 File API to check selected file size
	// https://developer.mozilla.org/en-US/docs/Using_files_from_web_applications
	// http://caniuse.com/#feat=fileapi
	for (var i = 0; i < element.files.length; i++) {
		if (element.files[i] !== undefined && element.files[i].size !== undefined) {
			filesize += parseInt(element.files[i].size);
		}
	}
	console.log(filesize);
	if (filesize > 0)
		return filesize <= maxBytes;
	// if the browser doesn't support the HTML5 file API, just return true
	// since returning false would prevent submitting the form
	return true;
});


jQuery.validator.unobtrusive.adapters.add('filecount', ['count'], function (options) {
	// Set up test parameters
	var params = {
		count: options.params.count
	};

	// Match parameters to the method to execute
	options.rules['filecount'] = params;
	if (options.message) {
		// If there is a message, set it for the rule
		options.messages['filecount'] = options.message;
	}
});

jQuery.validator.addMethod("filecount", function (value, element, param) {
	if (value === "") {
		// no file supplied
		return true;
	}
	return element.files.length <= parseInt(param.count);
});

jQuery.validator.addMethod("enforcetrue", function (value, element, param) {
	return element.checked;
});
jQuery.validator.unobtrusive.adapters.addBool("enforcetrue");

