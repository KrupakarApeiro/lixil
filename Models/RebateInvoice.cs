//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace LixilRebate.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class RebateInvoice
    {
        public int Id { get; set; }
        public int RebateId { get; set; }
        public string InvoicePath { get; set; }
        public int InvoiceStatus { get; set; }
        public int InvoiceType { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public Nullable<bool> IsActive { get; set; }
    }
}
