//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace LixilRebate.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class RebateReward
    {
        public int Id { get; set; }
        public int BuilderId { get; set; }
        public string DistributorInvoiceDate { get; set; }
        public string DistributorInvoiceNumber { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<decimal> Value { get; set; }
        public Nullable<int> RebateId { get; set; }
        public string RecordNumber { get; set; }
        public Nullable<int> Status { get; set; }
        public Nullable<System.DateTime> ApprovedOrDeniedDate { get; set; }
        public string ReasonIfDenied { get; set; }
        public string DollarEstimated { get; set; }
        public string DollarApproved { get; set; }
    
        public virtual RebateDetail RebateDetail { get; set; }
    }
}
